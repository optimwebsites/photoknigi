<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>##__page_title##</title>
  <meta content="##__description##" name="description" />
  <meta content="##__keywords##" name="keywords" />
  <meta class="viewport" name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">

  <!-- Favicon -->
 <!-- <link rel="shortcut icon" href="img/favicon.ico">-->
  
  	##__header_css##
	##__header_js##
	<link href="/_th/assets/galleries/royalslider/royalslider.css" rel="stylesheet">
	<link href="/_th/assets/galleries/royalslider/skins/minimal-white/rs-minimal-white.css" rel="stylesheet">
	<link href="/_th/assets/galleries/royalslider/royalslider-custom.css" rel="stylesheet">


  <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	<link rel='stylesheet' href="/_th/pgsv/css/ie/ie8.css">
  <![endif]-->
  ##__headerscript##
</head>
<body  id_page="##id_page##">
<!-- Rating@Mail.ru counter -->
<script type="text/javascript">
	var _tmr = window._tmr || (window._tmr = []);
	_tmr.push({id: "2812500", type: "pageView", start: (new Date()).getTime()});
	(function (d, w, id) {
		if (d.getElementById(id)) return;
		var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
		ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
		var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
		if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
	})(document, window, "topmailru-code");
</script><noscript><div style="position:absolute;left:-10000px;">
		<img src="//top-fwz1.mail.ru/counter?id=2812500;js=na" style="border:0;" height="1" width="1" alt="Рейтинг@Mail.ru" />
	</div></noscript>
<!-- //Rating@Mail.ru counter -->
<style>
	#slider-with-blocks-1 {
		width: 100%;
		min-height:500px;
	}

	.rsMinW, .rsMinW .rsOverflow, .rsMinW .rsSlide, .rsMinW .rsVideoFrameHolder, .rsMinW .rsThumbs {
		background: none;
	}

	.rsContent {
		font-size: 24px;
		line-height: 32px;
		float: left;
	}
	.bContainer {
		position: relative;
	}

	.rsContent p {
		font-size:16px;
	}
	.rsABlock {
		position: relative;
		display: block;
		left: auto;
		top: auto;
	}

	ul.textlist {
		margin:0;
		padding:0px 40px;
	}
	ul.textlist li {
		font-size:16px;
		line-height:24px;
		margin:0;
		padding-bottom:4px;
	}

	.slide1 .bContainer { text-align:center; }
	.slide1 img {border-radius: 80px; width:150px; height:150px; margin-top:40px; }
	.slide1 .h2 {text-align: center !important;}

	.slide2 {}
	.slide2 .bContainer {}
	.slide2 {}
	.slide2 {

	}
	.slide3 {}
	.slide3 .bContainer {}
	.slide3 span {

	}
	.slide4 .bContainer {

	}

	.photoCopy a {
		color: #FFF;
	}

	.benefits {
		color: #fff;
	}

	.terms {
		text-align: center;
	}
	.terms .h2 {
		text-align:center !important;
	}

	.img-mobile {
		display:none;
	}
	.img-desktop {
		display:block;
	}

	.contacts-info {
		display:none;
	}


	@media screen and (min-width: 0px) and (max-width: 980px) {
		.royalSlider,
		.rsContent {
			font-size: 22px;
			line-height: 28px;
			min-height:500px;
		}

		#header_right {
			display:none;
		}

		.fa-long-arrow-right {
			display:none;
		}
		.img-mobile {
			display:block;
		}
		.img-desktop {
			display:none;
		}
		.contacts-info {
			display:block;
		}
		.contacts-email {
			display:none;
		}

		.header-info {
			display:none;
		}

	}



	@media screen and (min-width: 0px) and (max-width: 770px) {
		.slide1 img, .slide2 img, .slide3 img, .slide4 img {
			display:none;
		}

		.royalSlider {
			min-height:500px;
		}

		#footer .phone {
			float:none;
			font-size:18px;
			text-align:center;
			width: 100%;
		}

		#footer_5 {
			display:none;
		}

	}
	@media screen and (min-width: 0px) and (max-width: 500px) {
		.royalSlider,
		.rsContent {
			font-size: 18px;
			line-height: 26px;
			min-height:500px;
		}

	}

</style>
<div class="page-box">
<div class="page-box-content">
	<div class="full-width-box header-box">
<header class="header">
  <div class="header-wrapper">
	<div class="container">
	  <div class="row">
		<div class="col-xs-6 col-md-2 logo-box template_block" id="header_logo" data-title="Логотип">
		  ##header_logo##
		</div><!-- .logo-box -->
		
		<div class="col-md-6  template_block" id="header_right" data-title="Шапка. Центр">
			##header_right##
		</div>
		  <div class="col-xs-6 col-md-2 template_block" id="header_right_right" data-title="Шапка. Блок справа">
			  ##header_right_right##
		  </div>
		  <div class="col-md-2 template_block" id="header_right_right" data-title="Шапка. Блок справа">
			  <div class="header-info">
				  <div style="font-size:13px;">Без выходных!</div>
				  <div class="contacts-phone" style="font-size:13px;">
					  <span class="text-phone">Доставка по России </span>
					  <span class="text-warning">340 р.</span>
				  </div>
			  </div>
		  </div>
	  </div>

	</div>
  </div><!-- .header-wrapper -->
  
</header>
		</div><!-- .header -->
	  
<section id="main"  class="main-content template_block" data-title="Основной контент">
	##main##

</section>
	<div class="full-width-box sample-book order"  id="orderbook">
		<div class="container ">
			<div class="row">
				<div class="col-md-3">
					&nbsp;<br/>
				</div>
				<div class="col-sm-12 col-md-6 template_block" id="order_book" data-title="форма заказа">
					##order_book##
				</div>
				<div class="col-md-3">
					&nbsp;<br/>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>



</div></div>

<footer id="footer">
  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-6 template_block" id="footer_5" data-title="Подвал 5">
		   ##footer_5##
		</div>

        <div class="col-xs-12 col-md-6  template_block" id="footer_8" data-title="Подвал 8">
		   ##footer_8##         
        </div>
      </div>
    </div>
  </div><!-- .footer-bottom -->
</footer>
<div class="clearfix"></div>
	##__footer_js##
</body>
</html>