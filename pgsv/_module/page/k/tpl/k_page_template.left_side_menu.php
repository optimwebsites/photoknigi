<?php
$phone = new Phone(Yii::app()->params['cms']['phone']);
?><!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>##__page_title##</title>
    <meta content="##__description##" name="description" />
    <meta content="##__keywords##" name="keywords" />
    <meta class="viewport" name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Favicon -->
    <!--<link rel="shortcut icon" href="img/favicon.ico"> -->

    ##__header_css##
    ##__header_js##

    ##__headerscript##
</head>
<body id_page="##id_page##">
<!-- Rating@Mail.ru counter -->
<script type="text/javascript">
    var _tmr = window._tmr || (window._tmr = []);
    _tmr.push({id: "2812500", type: "pageView", start: (new Date()).getTime()});
    (function (d, w, id) {
        if (d.getElementById(id)) return;
        var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
        ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
        var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
        if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
    })(document, window, "topmailru-code");
</script><noscript><div style="position:absolute;left:-10000px;">
        <img src="//top-fwz1.mail.ru/counter?id=2812500;js=na" style="border:0;" height="1" width="1" alt="Рейтинг@Mail.ru" />
    </div></noscript>
<!-- //Rating@Mail.ru counter -->
<div class="page-box">
    <div class="page-box-content">
        <div id="top-box">
            <div class="top-box-wrapper" style="background-color: #616466">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6 col-lg-4 hidden-sm hidden-xs">
                            <div class="top-info "><span class="big">Лучшие фотокниги</span> <br/>для самых ценных моментов</div>
                        </div>
                        <div class="col-xs-12 col-md-2 col-lg-5">
                            <div class="top-info text-right">
                                <a href="tel:<?=$phone->getNumber()?>" style="font-size:150%"><?=$phone->prettyFormat()?></a> <br/><span class="badge badge-warning">Без выходных</span>

                            </div>
                        </div>
                        <div class="hidden-xs col-md-4 hidden-sm col-lg-3">
                            <div class="top-info text-right">
                                <i class="fa fa-envelope-o"></i>
                                Email: <a href="maito:<?=Yii::app()->site->getEmail();?>"><?=Yii::app()->site->getEmail();?></a><br/>
                                <div class="text-center social-icons">
                                    <a href="https://www.facebook.com/people/Funfotobook-Ru/100010914794325"><i class="fa fa-facebook"></i></a>
                                    <a href="https://vk.com/club109395735"><i class="fa fa-vk"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>


        <header class="header">
            <div class="header-wrapper">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-2 col-lg-3 logo-box template_block"  id="header_logo" data-title="Логотип">
                            ##header_logo##

                            <div class="logo">
                                <h5 class="title text-danger"><?=Yii::app()->site->getName();?></h5>
                            </div>
                        </div>
                        <!-- .logo-box -->

                        <div class="col-xs-6 col-md-10 col-lg-9 template_block"  id="header_center" data-title="Шапка. Центр" >
                            <br> ##header_center##
                        </div>
                    </div>
                </div>

                <!--.row -->
            </div>
            <!-- .header-wrapper -->
        </header>

        <!-- .header -->
        <div class="full-width-box">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-12 template_block" id="head_gallery" data-title="Галерея на главной">
                        ##head_gallery##
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-9 template_block"  id="content" data-title="Контент">
                       ##content##
                </div>
                <div class="col-sm-3 template_block"  id="content_left" data-title="Меню слева">
                    ##content_left##
                </div>
            </div>
        </div>

        </div>
        <!-- .page-box-content -->
    </div>
    <!-- .page-box -->

    <footer id="footer">
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="hidden-xs hidden-sm col-md-4 template_block text-light" id="footer_1" data-title="Подвал 1">
                        ##footer_1##

                        <p><b>Создавайте фотокниги вместе с <?=Yii::app()->site->getName();?></b></p>
                        <p>5 лет на рынке, более 10000 довольных клиентов</p>

                    </div>
                    <div class="col-xs-12 col-sm-12 col-md-3 template_block bottom-padding-mini text-light" id="footer_2" data-title="Подвал 2">
                        ##footer_2##
                        <p class="text-center">
                            <br/><b><a href="mailto:<?=Yii::app()->site->getEmail();?>" class="text-light"><?=Yii::app()->site->getEmail();?></a></b>
                        </p>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-3 template_block bottom-padding-mini text-light" id="footer_2" data-title="Подвал 2">
                        ##footer_2##
                        <p class="text-center">
                            <a href="tel:<?=$phone->getNumber()?>" class="text-light big"><?=$phone->prettyFormat()?></a><br/><br/><span class="badge badge-warning">Без выходных</span>

                        </p>
                    </div>

                    <div class="col-xs-12 col-sm-12 col-md-2 template_block" id="footer_3" data-title="Подвал 3">
                        ##footer_3##
                        <a href="#" class="up">
                            <span class="glyphicon glyphicon-arrow-up"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <!-- .footer-bottom -->
    </footer>
    <div class="clearfix"></div>
<?php
$callback_widget = new w_callback_button();
echo $callback_widget->getHtml();

?>

    ##__footer_js##

    <script src="/_site/js/bootstrap.min.js"></script>
    <script src="/_site/js/jquery.touchwipe.min.js"></script>
    <script src="/_site/js/jquery.appear.js"></script>
    <script src="/_site/js/jquery.royalslider.min.js"></script>
    <script src="/_site/js/raphael.min.js"></script>
    <script src="/_site/js/layerslider/greensock.js"></script>
    <script src="/_site/js/layerslider/layerslider.transitions.js"></script>
    <script src="/_site/js/layerslider/layerslider.kreaturamedia.jquery.js"></script>
    <script src="/_site/js/revolution/jquery.themepunch.tools.min.js"></script>
    <script src="/_site/js/revolution/jquery.themepunch.revolution.min.js"></script>
    <script src="/_site/js/main.js"></script>
    <script src="/_th/assets/jquery-block-ui/jqueryblockui.js"></script>
    <script src="/_th/assets/blockui.js"></script>


    <script type="text/javascript">

        $(".scroll-to").click(function () {
            var hr = $(this).attr("href");
            var o = $(hr).offset();

            $('html, body').animate({
                scrollTop: o.top - 100
            }, 1000);
        });

    </script>

</body>
</html>