<?php

return array(
	'layouts'=>array(
		'landing'=>array('title'=>'Landing page','is_main'=>0),
		'landing2'=>array('title'=>'Landing page v2','is_main'=>0),
		'left_side_menu'=>array('title'=>'Left Side Menu','is_main'=>0),
	),
	'footer' => [
		'js' => [
			999 => ["_js/site.js", "dynamic"]
		]
	],
	'header'=>[
		"css"=>[
			['/_th/assets/bootstrap/3.2.0/bootstrap.min.css', null, "bootstrap.css"],
			['/_site/css/bootstrap.css',"dynamic"],
			['/_site/css/font-awesome.min.css',"dynamic"],
			['/_site/css/jslider.css',"dynamic"],
			['/_site/css/settings.css',"dynamic"],
			['/_site/css/animate.css',"dynamic"],
			['/_site/css/morris.css',"dynamic"],
			['/_site/css/ladda.min.css',"dynamic"],
			['/_site/css/style.css',"dynamic"],
			['/_site/css/ie/ie.css',"dynamic"],
			['/_site/_css/style.css',"dynamic"],
		],
		"js"=>array(
			array('/_th/assets/jquery/jquery-1.11.min.js', null, "jquery.js"),
			array('/_site/js/jquery-2.1.3.min.js',"dynamic"),
			array('/_th/assets/optimweb/base/_js/u.js',"dynamic")
		)
	],

);
