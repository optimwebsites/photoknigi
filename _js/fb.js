jQuery(document).ready(function($) {
	// Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
	// it's recommended to disable them when using autoHeight module
	/*$('#slider-with-blocks-1').royalSlider({
		arrowsNav             : true,
		loop                  : true,
		keyboardNavEnabled    : true,
		controlsInside        : true,
		imageAlignCenter		: true,
		imageScaleMode        : 'fill',
		arrowsNavAutoHide     : false,
		autoScaleSlider       : true,
		autoScaleSliderWidth  : 1920,
		autoScaleSliderHeight : 800,
		controlNavigation     : 'bullets',
		thumbsFitInViewport   : false,
		navigateByClick       : true,
		startSlideId          : 0,
		autoPlay              : {
			enabled: false,
			pauseOnHover: true
		},
		transitionType        :'move',
		globalCaption         : false,
		deeplinking           : {
			enabled : true,
			change: true,
			prefix : 'g-slide-'
		},
		imgWidth              : 1920,
		imgHeight             : 800,
		delay: 5000
	});*/

/*	if ($(window).scrollTop() > 0) {
		$("#main_content").css({marginTop:"0%"});
	} else {
		setTimeout(function() {
			$("#main_content").animate({marginTop:"0%"}, 2000);
		}, 500);
	}
*/
	setTimeout(function() {
		$("#main_header").animate({opacity: 1}, 1000);
	}, 300)


	$(".scroll-to").click(function() {
		scrollToElement($(this).data("target"), 1000);
	});

	var sliders = $('.tp-banner');
	//Revolution Slider Start
	if (sliders.length && $.fn.revolution) {

		sliders.each(function () {
			var revolutionSlider = $(this);
			revolutionSlider.revolution({
				delay          : 6000,
				startwidth     : 1200,
				startheight    : $(this).data('height'),
				hideThumbs     : 10,
				navigationType : 'none',
				onHoverStop    : 'off',
				hideTimerBar      : 'on'
			});
		});
	}
	//Revolution Slider End

	$(".igallery").poptrox({
		usePopupCaption:true,
		usePopupNav:true,
		popupBlankCaptionText: "",
		popupCloserBackgroundColor:"#ff341d"
	});

	$(".igallery-item").hover(function() {
		var txt = $(this).find("img").data('title');
		var h = $(this).find(".igallery-hover");
		if (h.length == 0) {
			$(this).append('<div class="igallery-hover"></div>');
			var h = $(this).find(".igallery-hover");
		}
		h.html(txt);
		h.show('fast');
	}, function() {
		var h = $(this).find(".igallery-hover");
		h.hide('fast');
	});


	var reachedGoals = {clickOnOrderButton:false, startPhoneEnter:false, orderCallback:false};

	$("[name='m_callback[phone]']").click(function () {
		$(document).trigger("reachGoal", {name: "startPhoneEnter"});
	});

	$("button.form-submit").click(function () {
		$(document).trigger("reachGoal", {name: "orderCallback"});
	});

	$(".scroll-to").click(function () {
		$(document).trigger("reachGoal", {name: "clickOnOrderButton"});
	});

	$(document).on("reachGoal", function (e, data) {
		if (window['yaCounter30888196'] && !reachedGoals[data.name]) {
			yaCounter30888196.reachGoal(data.name);
		}
		reachedGoals[data.name] = true;
	});

	$("#openEditor").click(function() {
		/*new ffbEditor({
			"container": "ffbEditor",
			"template": 20,
			"format": 20,
		});*/
	});

	$(document).ready(function() {

		var body = $(document.body);

		var scrollPages = ["page-print", "page-vipusk", "page-zakaz", "page-wedding", "page-dmb"];
		for (var i=0; i < scrollPages.length; i++) {
			if ($(body).hasClass(scrollPages[i])) {
				scrollOnLoad($("." + scrollPages[i] + " .main-content"));
			}
		}
	});
});

function scrollOnLoad(el) {
	if ($(document.body).scrollTop() > 50) {
		$("#head-list li.hide").removeClass('hide');
		$(el).css({marginTop:0, opacity:1});
		return;
	}

	var interval = 400;
	var start = 0;
	$("#head-list li.hide").each(function(){
		console.log(this);
		start+=interval;
		$(this).css({marginTop:"-50px", opacity:0});
		$(this).removeClass('hide');
		var _this = this;
		setTimeout(function() {$(_this).animate({marginTop:0, opacity:1}, 600);}, start);
	});

	setTimeout(function () {
		$(el).animate({marginTop:0, opacity:1}, 1000);
	}, 1500);
}


$(window).scroll(function(e) {
	var sc = $(this).scrollTop();
	if (sc < 180) {
		$("#header").css("background", "rgba(0,0,0, "+ (0.2 + 0.65 * sc / 180) +")");
	} else {
		$("#header").css("background", "rgba(0,0,0, 0.85 ");
	}
});
