
/**
 * Виджет
 */
function ffbEditor(options) {
	this.init(options);
}

/**
 * инициализация объекта
 * @param options
 */
ffbEditor.prototype.init = function (options) {

	this.container = null;
	this.options = {
		pid: null,
		container: null,
		url: "/_c/photobook/createPhotobook/s/?order=60",
		width: "100%",
		format: null,
		template: null
	};

	this.extend(options);

	if (this.createContainer() === false) {
		return false;
	}

	var iframe = document.createElement('iframe');
	this.iframe = iframe;
	iframe.setAttribute('src', this.options.url);
	iframe.setAttribute('width', this.options.width);
	iframe.setAttribute('frameborder', 0);
	iframe.setAttribute('scrolling', 'no');

	this.container.appendChild(this.iframe);

	var width = $(this.container).width();
	var height = "800";
	this.iframe.style.height = height + "px";
	this.iframe.style.width = width + "px";

};

/**
 * Переопределение свойств объекта
 * @param options
 */
ffbEditor.prototype.extend = function (options) {
	var n;
	for (n in options) {
		options.hasOwnProperty(n) && (this.options[n] = options[n]);
	}

	if (this.options.id == null) {
		this.options.id = this.widget + "_" + Math.round(Math.random()*10000);
	}
};

/**
 * создание контейнера
 *
 * @return {boolean}
 */
ffbEditor.prototype.createContainer = function () {
	var options = this.options;
	this.container = document.getElementById(options.container);
	return true;
};
