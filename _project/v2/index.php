<!DOCTYPE html>
<html lang="en">
<head>
	<title>##__page_title##</title>
	<meta content="##__description##" name="description" />
	<meta content="##__keywords##" name="keywords" />
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- Bootstrap core CSS -->
	<link href="/_th/assets/bootstrap/3.2.0/bootstrap.min.css" rel="stylesheet">
	<link href='https://fonts.googleapis.com/css?family=Roboto:100,400,300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	<link href="/_th/assets/galleries/royalslider/royalslider.css" rel="stylesheet">
	<link href="/_th/assets/galleries/royalslider/skins/default/rs-default.css" rel="stylesheet">
</head>

<body>
<style>
	html {
		font-size:14px;
	}
	body {
		font-family: Roboto, Arial, Sans-Serif;
		font-weight: 300;
		font-size:14px;
		color: #333	;
	}

	/* typography */

	h1, h2,h3, h4,.h1,.h2,.h3,.h4 {
		font-weight:100;
		color: #FF341D;
	}

	h1, .h1 {
		font-size:3rem;
		color:#fff;
		text-shadow: 1px 1px 1px rgba(0,0,0,0.5);
	}

	h2, .h2 {
		font-size:2.5rem;
		color: #555;
		margin-bottom:2rem;
	}

	h3, .h3 {
		font-size:1.5rem;
	}


	a, a:hover {
		color:#FF341D;
		text-decoration: underline;
	}

	/* base */

	.btn-danger {
		color: #fff;
		background-color: #ff4c31;
		border-color: #f7472a;
	}


	.btn-danger:hover {
		color: #fff;
		background-color: #ff694b;
		border-color: #f7472a;
	}

	.form-field {
		padding: 0.7rem;
		border-radius:4px;
		border:1px solid #88a876;
		width:100%;
	}

	/*header */

	header {
		top:0;
		display:block;
		position:fixed;
		height:75px;
		z-index:9999;
		background-color: rgba(0,0,0,0.2);
		width:100%;
	}

	.logo {
		height:75px;
		background: #FF341D;
		border-radius: 0 0 10px 10px;
		text-align:center;
	}
	.logo img {
		padding:10px 0 5px 0;
	}
	.logo a {
		font-weight:700;
		color:#fff;
		text-decoration:none;
		letter-spacing: 1px;
		line-height:5rem;
		font-size: 1rem;
	}

	header ul.main-menu li {
		float:left;
		list-style: none;
		margin: 10px 50px 0px 0
	}

	header ul.main-menu li a {
		color:#fff;
		border-bottom: 1px solid #bbb;
		text-decoration:none;
	}

	header ul.main-menu li a:hover {
		color:#e0e0e0;

	}

	header ul.main-menu li.active a {
		color: #ff341d;
		border: 0;
	}

	header .phone {
		margin-top: 10px;
	}

	.phone a {
		font-size:2rem;
		font-weight: 300;
		text-decoration:none;
	}

	.phone-desc {
		font-size:0.75rem;
		color:#fff;
	}

	/*end header*/

	/* breadcrumb */
	.page-breadcrumb {
		margin:0;
		padding:0;
	}

	.page-breadcrumb li {
		float:left;
		list-style: none;
		margin: 0 10px 0 0;
	}

	.page-breadcrumb li a {
		color:#fff;
		font-weight:300;
		text-shadow: 1px 1px 1px rgba(0,0,0,0.5);
		text-decoration:none;
	}

	.page-breadcrumb .separator{
		color:#fff;
	}


	main {
		display:block;
		margin-top:80px;
	}

	.main-header {
		margin: 30px 0 40px 0px;
		opacity:0;
	}

	/*main gallery*/
	.main-gallery img{
		width:100%;
	}

	.gallery-col1, .gallery-col2, .gallery-col3 {
		margin:0;
		padding:0;
	}

	.gallery-col1 img {
		border-right: 1px solid #fff;
		border-bottom: 1px solid #fff;
	}

	.gallery-col3 img {
		border-left: 1px solid #fff;
		border-bottom: 1px solid #fff;
	}

	/* content-block */
	.content-block {
		padding: 20px 20px 40px 40px;
	}

	.content-block__bg_green {
		background: rgba(229, 244, 214, 0.65);

	}

	.content-block__bg_green2 {
		background: rgba(187, 220, 152, 0.65);

	}

	.book-price {
		text-align:left;
		font-size:1.8rem;
		font-weight:300;
		color:#ff341d;
		margin-top:10px;
	}

	.book-size {
		text-align:left;
		font-size:1.3rem;
		font-weight:700;
		color:#ff341d;
		margin-top:10px;
	}

	.simple-list {
		margin:0;
		padding: 1rem;
	}
	.simple-list li {
		list-style: none;
		padding:0.2rem 0;
	}

	.simple-list li:before {
		display:inline-block;
		content: "\2014";
		padding-right:10px;
		color:#ff341d;
		font-size:1rem;

	}


	/* theme wedding */
	body {
		background:#e0ddd8 url(/_site/i/v2/bg.jpg) no-repeat fixed top center;
		background-size: 100%;
	}

	main .main-content{
		background-color: rgba(255,255,255,1);
		margin-top: 100%;
	}

	/**media**/
	@media (min-width:320px){
		html {
			font-size:10px;
		}
		body {
			font-size:10px;
		}
	}

	@media (min-width:768px){
		html {
			font-size:12px;
		}
		body {
			font-size:12px;
		}

		header ul.main-menu li {
			margin-right:20px;
		}
	}

	@media (min-width: 992px) {
		html {
			font-size:14px;
		}
		body {
			font-size:14px;
		}
		header ul.main-menu li {
			margin-right:50px;
		}
	}


</style>
<header id="header">
	<div class="container">
		<div class="row">
			<div class="col-xs-4 col-sm-2 col-md-2">
				<div class="logo">
					<div><a href="/">FunFotoBook</a></div>
				</div>
			</div>
			<div class="hidden-xs col-sm-6 col-md-7">
				<nav>
					<ul class="main-menu">
						<li class="active"><a href="/">Фотокниги <br>на заказ</a></li>
						<li><a href="/vypusknye_albomi/">Выпускные<br> фотокниги</a></li>
						<li><a href="/fotoknigi/pechat/">Печать<br> фотокниг</a></li>
						<li><a href="/fotoknigi/portfolio/">Наши<br> работы</a></li>
					</ul>
				</nav>
			</div>
			<div class="col-xs-7 col-sm-4 col-md-3 text-center">
				<div class="phone"><a href="">8 800 123-45-67</a></div>
				<div class="phone-desc">бесплатный звонок с любого телефона</div>
			</div>
		</div>
	</div>
</header>

<main>
<div class="container">

	<div class="main-header" id="main-header">
		<div class="col-xs-offset-0 col-sm-offset-2 col-md-offset-1">
			<ul class="page-breadcrumb">
				<li><a href="/">Главная</a></li>
				<li class="separator">&rarr;</li>
				<li><a href="/">Фотокниги на заказ</a></li>
			</ul>
			<div class="clearfix"></div>
			<h1>Свадебные фотокниги</h1>
		</div>
	</div>

	<div class="main-content" id="main-content">
		<div class="main-gallery">
			<div class="gallery-col1 col-sm-12">
				<img src="/_site/i/v2/title1.jpg" />
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="content-block">
			<div class="title">
				<h2>Как мы делаем идеальные свадебные фотокниги</h2>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<h3>Подходим индивидуально</h3>
					<p>Никаких границ для дизайна! Все только так, как вы хотите.</p>
				</div>
				<div class="col-sm-6">
					<h3>Обрабатываем и ретушируем</h3>
					<p>Любые недостатки фотографий, от замены фона до неудачного макияжа. </p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<h3>Оформляем на любой вкус</h3>
					<p>Обложки из кожзама, холста, ткани, бумвинила, с фотопечатью, с тиснением, вышивкой и абсолютно любой отделкой</p>
				</div>
				<div class="col-sm-6">
					<h3>Печатаем сами</h3>
					<p>У нас собственная типография, вы не переплачиваете посредникам. Передовое оборудование, оригинальные расходные материалы, ручная сборка, контроль качества.</p>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-6">
					<h3>Делаем хорошо</h3>
					<p>Более 70% наших клиентов обращается к нам повторно.</p>
				</div>
				<div class="col-sm-6">
					<h3>Доставляем быстро</h3>
					<p>От Калининграда до Камчатки, в любую точку России. Автобусом, почтой, курьером, самолетом!</p>
				</div>
			</div>

		</div>



		<div class="content-block content-block__bg_green">
			<style>
				#slider-with-blocks-1 {
					width: 100%;
				}
				.rsContent {
					float: left;

				}
				.slide {
					text-align:right;
				}
				.bContainer {
					position: relative;
				}
				.rsABlock {
					position: relative;
					display: block;
					left: auto;
					top: auto;
				}

				.deep-link {
					display:inline-block;
					padding:0.7rem;
					font-weight:400;
					text-decoration:none;
					border:1px solid #ff341d;
					border-radius: 6px;
					margin-top: 2rem;
					background: #ff341d;
					color: #fff;
				}

				.deep-link span {
					font-size:2rem;
					display:inline-block;
					line-height:1.4rem;
					font-weight:400;
				}

				.deep-link_prev {
					float:left;
				}

				.deep-link_next {
					float:right;
				}

				.deep-link:hover {
					text-decoration:none;
					color: #fff;
					background: #FF7054;
				}

				.g-title {
					position:absolute;
					color:#fff;
					font-size:1.2rem;
					font-weight:300;
					z-index:999;
					text-shadow: 1px 1px 1px rgba(0,0,0,0.8);
					padding:0.4rem;
					background: rgba(0,0,0, 0.7);
					line-height: 1.6rem;
					text-align: left;
				}
				.g-title.small {
					font-size:1rem;
				}

				@media screen and (min-width: 0px) and (max-width: 500px) {
					.royalSlider,
					.rsOverflow {
						height: 330px !important;
					}
				}
			</style>
			<div class="title">
				<h2>Оформление фотокниги</h2>
			</div>
			<div class="row">
				<div class="col-sm-12">

					<div id="slider-with-blocks-1" class="royalSlider rsMinW  ">
						<div class="rsContent">
							<div class="bContainer">
								<div class="row slide">
									<div class="col-xs-4">
										<div class="rsABlock" data-move-effect="left" data-move-offset="300" data-speed="500">
											<div class="g-title" style="margin:5% 5%;">Отстрочка <br>по периметру</div>
											<div class="g-title" style="margin:25% 5%;">Вышивка на обложке</div>
											<img src="/_site/i/v2/1.jpg" />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="rsABlock" data-move-effect="top" data-move-offset="300" data-speed="500">
											<div class="g-title" style="margin:5% 5%;">Золочение <br>срезов</div>
											<div class="g-title" style="margin:78% 5%;">Серебрение <br>срезов</div>
											<img src="/_site/i/v2/2.jpg" />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="rsABlock" data-move-effect="right" data-move-offset="300" data-speed="500">
											<div class="g-title" style="margin:85% 5%;">Отделка уголками</div>
											<img src="/_site/i/v2/3.jpg"/>
										</div>
									</div>
								</div>
								<div class="row rsABlock" data-move-effect="top" data-move-offset="400" data-speed="300">
									<div class="col-xs-6 text-right"><a href="#g-slide-4" class="deep-link"><span class="deep-link_prev">&larr;</span> Боксы, минибуки</a></div>
									<div class="col-xs-6"><a href="#g-slide-2" class="deep-link">Материалы для обложек<span class="deep-link_next">&rarr;</span></a></div>
								</div>
							</div>
						</div>
						<div class="rsContent">
							<div class="bContainer">
								<div class="row slide">
									<div class="col-xs-4">
										<div class="rsABlock" data-move-effect="left" data-move-offset="300" data-speed="500">
											<div class="g-title" style="margin:78% 5%;">Ламинированные <br>фотообложки</div>
											<img src="/_site/i/v2/4.jpg" />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="rsABlock" data-move-effect="left" data-move-offset="300" data-speed="500">
											<div class="g-title" style="margin:44% 5%;">Обложки из ткани</div>
											<div class="g-title small" style="margin:55% 5%;">40 видов ткани</div>
											<img src="/_site/i/v2/5.jpg" />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="rsABlock" data-move-effect="left" data-move-offset="300" data-speed="500">
											<div class="g-title" style="margin:35% 5%;">Обложки из экокожи</div>
											<div class="g-title small" style="margin:46% 5%;">50 видов экокожи</div>
											<img src="/_site/i/v2/6.jpg" />
										</div>
									</div>
								</div>
								<div class="row rsABlock" data-move-effect="top" data-move-offset="400" data-speed="700">
									<div class="col-xs-6 text-right"><a href="#g-slide-1" class="deep-link"><span class="deep-link_prev">&larr;</span> Вышивка, отделка срезов</a></div>
									<div class="col-xs-6"><a href="#g-slide-3" class="deep-link">Комбинированные обложки<span class="deep-link_next">&rarr;</span></a></div>
								</div>
							</div>
						</div>
						<div class="rsContent">
							<div class="bContainer">
								<div class="row slide">
									<div class="col-xs-4">
										<div class="rsABlock" data-move-effect="top" data-move-offset="300" data-speed="500">
											<div class="g-title" style="margin:59% 5%;">Фотовставки</div>
											<img src="/_site/i/v2/7.jpg" />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="rsABlock" data-move-effect="left" data-move-offset="300" data-speed="500">
											<div class="g-title" style="margin:63% 5%;">Шильды с именами</div>
											<img src="/_site/i/v2/8.jpg" />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="rsABlock" data-move-effect="right" data-move-offset="300" data-speed="500">
											<div class="g-title" style="margin:68% 5%;">Комбинированные<br>обложки</div>
											<img src="/_site/i/v2/9.jpg" />
										</div>
									</div>
								</div>
								<div class="row rsABlock" data-move-effect="top" data-move-offset="400" data-speed="700">
									<div class="col-xs-6 text-right"><a href="#g-slide-2" class="deep-link"><span class="deep-link_prev">&larr;</span> Материалы для обложек</a></div>
									<div class="col-xs-6"><a href="#g-slide-4" class="deep-link">Боксы, минибуки<span class="deep-link_next">&rarr;</span></a></div>
								</div>
							</div>
						</div>
						<div class="rsContent">
							<div class="bContainer">
								<div class="row slide">
									<div class="col-xs-4">
										<div class="rsABlock" data-move-effect="right" data-move-offset="300" data-speed="500">
											<div class="g-title" style="margin:52% 5%;">Проклейка пластиком</div>
											<img src="/_site/i/v2/10.jpg" />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="rsABlock" data-move-effect="bottom" data-move-offset="300" data-speed="500">
											<div class="g-title" style="margin:55% 10%;">Боксы</div>
											<img src="/_site/i/v2/11.jpg" />
										</div>
									</div>
									<div class="col-xs-4">
										<div class="rsABlock" data-move-effect="left" data-move-offset="300" data-speed="500">
											<div class="g-title" style="margin:74% 10%;">Минибуки</div>
											<img src="/_site/i/v2/12.jpg" />
										</div>
									</div>
								</div>
								<div class="row rsABlock" data-move-effect="top" data-move-offset="400" data-speed="700">
									<div class="col-xs-6 text-right"><a href="#g-slide-2" class="deep-link"><span class="deep-link_prev">&larr;</span> Комбинированные обложки</a></div>
									<div class="col-xs-6"><a href="#g-slide-1" class="deep-link">Вышивка, отделка срезов<span class="deep-link_next">&rarr;</span></a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="content-block content-block">
			<div class="title">
				<h2>Цены</h2>
			</div>
			<div class="row price-row">
				<div class="col-sm-1"></div>
				<div class="col-sm-2">
					<div class="book-size">30x21</div>
				</div>
				<div class="col-sm-2">
					<div class="book-size">21x30</div>
				</div>
				<div class="col-sm-2">
					<div class="book-size">25x25</div>
				</div>
				<div class="col-sm-2">
					<div class="book-size">30x30</div>
				</div>
				<div class="col-sm-2">
					<div class="book-size">40x30</div>
				</div>
				<div class="col-sm-1">

				</div>

			</div>
			<div class="row">
				<div class="col-sm-1"></div>
				<div class="col-sm-12">
					<img src="/_site/i/v2/book_sizes.png" class="col-sm-11" />
				</div>
				<div class="cols-sm-1"></div>
			</div>
			<div class="row price-row">
				<div class="col-sm-1">

				</div>
				<div class="col-sm-2">
					<div class="book-price">4 600</div>
				</div>
				<div class="col-sm-2">
					<div class="book-price">4 600</div>
				</div>
				<div class="col-sm-2">
					<div class="book-price">5 000</div>
				</div>
				<div class="col-sm-2">
					<div class="book-price">5 300</div>
				</div>
				<div class="col-sm-2">
					<div class="book-price">5 800</div>
				</div>
				<div class="col-sm-1">

				</div>

			</div>
			<div class="row">
				<div class="col-sm-6">
					<h3>В стоимость включено</h3>
					<ul class="simple-list">
						<li>индивидуальный дизайн фотокниги 16 страниц (8 разворотов)</li>
						<li>высококачественная печать на фотобумаге</li>
						<li>проклейка страниц пластиком</li>
						<li>персональная ламинированная фотообложка</li>
						<li>обработка и верстка 50 фотографий</li>
					</ul>
				</div>
				<div class="col-sm-6">
					<h3>Дополнительно</h3>
					<ul class="simple-list">
						<li>до 40 разворотов</li>
						<li>больше фотографий</li>
						<li>вышивка, обложки из ткани, кожзама, бумвинила, паспарту</li>
						<li>отделка срезов фотокниги</li>
						<li>боксы, минибук</li>
					</ul>
				</div>
			</div>
		</div>

		<div class="content-block content-block__bg_green2">
			<style>
				.order-row {
					margin-top:15px;
				}

				.order-row__phone-desc {
					font-size:0.8rem
				}

				.order-row .btn {
					margin-top:5px;
				}
			</style>
			<div class="row order-row">
				<div class="col-xs-6">
					<button class="btn btn-danger btn-lg scroll-to" type="button" data-target="#order-form">Заказать фотокнигу</button>
				</div>
				<div class="col-xs-6">
					<div><span class="h2">8 800 123-45-67</span></div>
					<div class="order-row__phone-desc">бесплатный звонок с мобильного телефона</div>

				</div>
			</div>
		</div>

		<div class="content-block  content-block__bg_green">
			<div class="title">
				<h2>Как мы работаем</h2>
			</div>
			<div class="row how-to-work">
				<div class="col-sm-3">
					<p class="h4">Разработка макета</p>
					<p>Вы присылаете фотографии. Уже на следующий день мы предложим варианты дизайна Вашей фотокниги.
						По выбранному варианту разрабатываем макет фотокниги и согласовываем с Вами.</p>
				</div>
				<div class="col-sm-3">
					<p class="h4">Оплата</p>
					<p>Только после окончательного согласования макета Вы оплачиваете фотокнигу любым удобным для Вас способом: банковской картой, ЯндексДеньги, переводом</p>
				</div>
				<div class="col-sm-3">
					<p class="h4">Печать</p>
					<p>В течение 1-3 дней мы изготавливаем Вашу фотокнигу.</p>
				</div>
				<div class="col-sm-3">
					<p class="h4">Доставка</p>
					<p>Мы доставляем фотокнигу по всей России от 1 до 7 дней почтой или любой транспортной компанией, которая будет Вам удобна.</p>
				</div>
			</div>
		</div>

		<div class="content-block">
			<style>
				/* faces */
				.faces {
					text-align:center;
					margin-top:2rem;
				}

				.faces img {
					border-radius: 60px;
				}

				.faces_position {
					font-size: 0.9rem;
					color:#555;
				}

			</style>
			<div class="title">
				<h2>Мы делаем фотокниги для Вас</h2>
			</div>
			<div class="row faces">
				<div class="col-sm-3">
					<img src="/_c/placeholder/show/s/?h=120&w=120" />
					<p class="h4">Дунаев Николай</p>
					<p class="faces_position">руководитель</p>
				</div>
				<div class="col-sm-3">
					<img src="/_c/placeholder/show/s/?h=120&w=120" />
					<p class="h4">Дашевская Анна</p>
					<p class="faces_position">руководитель отдела по работе с клиентами</p>
				</div>
				<div class="col-sm-3">
					<img src="/_c/placeholder/show/s/?h=120&w=120" />
					<p class="h4">Елена Кудинова</p>
					<p class="faces_position">руководитель отдела дизайна</p>
				</div>
				<div class="col-sm-3">
					<img src="/_c/placeholder/show/s/?h=120&w=120" />
					<p class="h4">Иванов Петр</p>
					<p class="faces_position">руководитель отдела печати</p>
				</div>
			</div>
		</div>

	</div>

	<style>
		.order-form .h4, .order-form label {
			color:#fff;
			text-shadow: 1px 1px 1px rgba(0,0,0,0.5);
		}

		.order-form {
			min-height:400px;
			position: relative;
			margin-bottom: 100px;
		}

		.order-form .h1 {
			margin-top:150px;
			text-align: center;
		}
		.order-form .h4 {
			font-weight:400;
			line-height: 2rem;
			text-align:center;
		}

	</style>
	<div class="row order-form">

		<div class="col-xs-12 col-sm-8 col-sm-offset-2">
			<div class="h1">Заказать свадебную фотокнигу</div>
			<div class="h4">Оставьте заявку, мы свяжемся с Вами в течение 15 минут</div>

			<form class="form-horizontal" id="order-form">
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-4 control-label">Телефон</label>
					<div class="col-sm-6">
						<input type="email" class="form-control" id="phone" placeholder="Email">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-4 control-label">E-mail</label>
					<div class="col-sm-6">
						<input type="email" class="form-control" id="inputEmail3" placeholder="Email">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail3" class="col-sm-4 control-label">&nbsp;</label>
					<div class="col-sm-6">
						<button class="btn btn-lg btn-danger">Отправить заявку</button>
					</div>
				</div>
			</form>

		</div>

	</div>
</div> <!-- /container -->
</main>
<footer>
	<style>
		footer {
			min-height: 200px;
			background: rgba(0,0,0,0.8);
			color: #fff;
			padding-top:20px;
		}

		footer h4 {
			color:#fff;
			font-weight:700;
			font-size:1rem;
			border-bottom: 1px solid #eee;
			display:inline-block;
			line-height: 2rem;
		}

		footer a {
			color:#fff;
			text-decoration:none;
			font-size:0.9rem;
		}

		footer ul {
			margin:0;
			padding:0;
		}

		footer ul li {
			list-style-type: none;
			margin:0;
		}

		.footer__email {
			color: #ff341d;
		}

		.footer__phone {
			font-weight: 100;
			font-size:1.8rem;
			color: #ff341d;
		}
	</style>
	<div class="container">
		<div class="row">
			<div class="col-xs-6 col-sm-3">
				<h4>Фотокниги на заказ</h4>
				<ul class="link-list">
					<li><a href="/">Свадебные фотокниги</a></li>
					<li><a href="/">Дембельские альбомы</a></li>
					<li><a href="/">Семейные фотокниги</a></li>
					<li><a href="/">Фотокниги путешествий</a></li>
					<li><a href="/">Фотокниги на юбилей</a></li>
				</ul>

			</div>
			<div class="col-xs-6 col-sm-3">
				<h4>Печать фотокниг</h4>
				<ul class="link-list">
					<li><a href="/fotoknigi/pechat/">Премиум печать</a></li>
					<li><a href="/fotoknigi/pechat/">Полиграфическая печать</a></li>
				</ul>
				<br/>
				<ul class="link-list">
					<li><a href="/blog/">Блог</a></li>
				</ul>
			</div>
			<div class="col-xs-6 col-sm-3">
				<h4>Выпускные альбомы</h4>
				<ul class="link-list">
					<li><a href="/vypusknye_albomi/">Фотокниги-бабочки</a></li>
					<li><a href="/vypusknye_albomi/">Выпускные альбомы премиум</a></li>
					<li><a href="/vypusknye_albomi/">Выпускные альбомы стандарт</a></li>
				</ul>
			</div>

			<div class="col-xs-6 col-sm-3">
				<h4>Контакты</h4>
				<p>Москва, Олимпийский пр-т, д. 22</p>
				<p class="footer__phone">8 800 123-45-67</p>
				<p class="footer__email"><a href="mailto:info@funfotobook.ru">info@funfotobook.ru</a></p>
			</div>
		</div>
	</div>
</footer>
<script src="/_site/js/jquery-2.1.3.min.js" type="text/javascript"></script>
<script src="/_site/_js/site.js" type="text/javascript"></script>
<script type="text/javascript" src="/_th/assets/galleries/royalslider/jquery.royalslider.min.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		// Please note that autoHeight option has some conflicts with options like imageScaleMode, imageAlignCenter and autoScaleSlider
		// it's recommended to disable them when using autoHeight module
		$('#slider-with-blocks-1').royalSlider({
			arrowsNav             : true,
			loop                  : true,
			keyboardNavEnabled    : true,
			controlsInside        : true,
			imageAlignCenter		: true,
			imageScaleMode        : 'fill',
			arrowsNavAutoHide     : false,
			autoScaleSlider       : true,
			autoScaleSliderWidth  : 1920,
			autoScaleSliderHeight : 800,
			controlNavigation     : 'bullets',
			thumbsFitInViewport   : false,
			navigateByClick       : true,
			startSlideId          : 0,
			autoPlay              : {
				enabled: false,
				pauseOnHover: true
			},
			transitionType        :'move',
			globalCaption         : false,
			deeplinking           : {
				enabled : true,
				change: true,
				prefix : 'g-slide-'
			},
			imgWidth              : 1920,
			imgHeight             : 800,
			delay: 5000
		});

		if ($(window).scrollTop() > 0) {
			$("#main-content").css({marginTop:"0%"});
		} else {
			setTimeout(function() {
				$("#main-content").animate({marginTop:"0%"}, 2000);
			}, 500);
		}

		setTimeout(function() {
			$("#main-header").animate({opacity: 1}, 1000);
		}, 300)


		$(".scroll-to").click(function() {
			scrollToElement($(this).data("target"), 1000);
		});

	});


	$(window).scroll(function(e) {
		var sc = $(this).scrollTop();
		if (sc < 180) {
			$("#header").css("background", "rgba(0,0,0, "+ (0.2 + 0.65 * sc / 180) +")");
		} else {
			$("#header").css("background", "rgba(0,0,0, 0.85 ");
		}
	});

</script>
</body>
</html>
