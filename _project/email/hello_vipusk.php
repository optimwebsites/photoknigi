<!DOCTYPE html>
<html>
<head lang="en">
	<meta charset="UTF-8">
	<title></title>
</head>
<style>
	.container {
		width:800px;
	}

	.container * {
		font-family: Arial;
	}

	.container a {
		color: #ff4c31;
	}

	a.btn {
		padding: 10px;
		border-radius: 3px;
		color: #fff;
		background-color: #ff4c31;
		border-color: #f7472a;
		text-decoration:none;
	}

	h2 {
		font-size:20px;
		line-height: 30px;
		padding-top:10px;
	}

	ul {
		margin: 25px 0;
		padding:0;
	}

	ul li {
		list-style: none;
		padding-left:0;
		margin:0;
		padding:2px;
	}

	ul li:before {
		content: "\2013";
		padding-right:10px;

	}

	.text-red {
		color: #ff4c31;
	}

</style>
<body>
<table class="container" align="center" cellpadding="0" cellspacing="0" border="0" style="width:800px;">
	<tr>
		<td width="50%">
			<div style="font-size:24px; font-weight:bold; color:#ff4c31; ">FunFotoBook.ru</div>
			<div class="text-red">5 лет на рынке, более 10 000 довольных клиентов</div>
		</td>
		<td>
			<div style="text-align: right">8 (800) 707-91-64</div>
			<div style="text-align: right"><a href="mailto:info@funfotobook.ru">info@funfotobook.ru</a></div>
		</td>
	</tr>
	<tr>
		<td width="100%" colspan="2">
			<div><br/><img src="http://funfotobook.ru/_site/_project/email/email.jpg" style="width:100%"/></div>
			<div>
				<p>Горячая пора выпускных альбомов – время, когда нужно иметь надежного партнера. FunFotoBook всегда выполняет свои обязательства перед фотографами качественно и точно в срок.</p>
				<h2 style="font-size:25px;text-align:center">Почему именно мы?</h2>

				<ul>
					<li>3 собственные полиграфические базы по всей России – вы не переплачиваете посредникам;</li>
					<li>специальные условия для фотографов;</li>
					<li>скидка 30% на все образцы и накопительная система скидок от объема;</li>
					<li>сроки печати – не более 7 дней даже в самый пик сезона;</li>
					<li>доставка в любую точку России;</li>
					<li>бесплатный демо-вариант альбома.</li>
				</ul>
			</div>
		</td>
	</tr>

	<tr>
		<td colspan="2"><h2>Выпускной альбом "Бабочка"</h2></td>
	</tr>
	<tr>
		<td width="50%"><img src="http://funfotobook.ru/_site/i/v2/vipusk/bat_1.jpg" style="width:100%"/></td>
		<td width="50%"><img src="http://funfotobook.ru/_site/i/v2/vipusk/bat_3.jpg" style="width:100%"/></td>
	</tr>
	<tr>
		<td colspan="2">
			<ul>
				<li>Цена <span class="text-red">от <b>260</b> руб</span></li>
				<li>Цифровая печать на бумаге 160 г/м.кв + проклейка картоном + ламинация</li>
				<li>Разворот 180 градусов</li>
				<li>Минимальный объем - 2 разворота</li>
			</ul>

			<a href="http://funfotobook.ru/vypusknye_albomi/" class="btn">Подробнеe &rarr;</a>
		</td>
	</tr>


	<tr>
		<td colspan="2"><h2>Выпускной альбом "Премиум"</h2></td>
	</tr>
	<tr>
		<td width="50%"><img src="http://funfotobook.ru/_site/i/v2/vipusk/premium_1.jpg" style="width:100%"/></td>
		<td width="50%"><img src="http://funfotobook.ru/_site/i/v2/vipusk/premium_4.jpg" style="width:100%"/></td>
	</tr>
	<tr>
		<td colspan="2">
			<ul>
				<li>Цена <span class="text-red">от <b>240</b> руб</span></li>
				<li>Фотопечать + проклейка пластиком + ламинация</li>
				<li>Разворот 180 градусов</li>
				<li>Минимальный объем - 2 разворота</li>
			</ul>
			<a href="http://funfotobook.ru/vypusknye_albomi/" class="btn">Подробнее &rarr;</a>
		</td>
	</tr>


	<tr>
		<td colspan="2"><h2>Выпускной альбом "Стандарт"</h2></td>
	</tr>
	<tr>
		<td width="50%"><img src="http://funfotobook.ru/_site/i/v2/vipusk/std_1.jpg" style="width:100%"/></td>
		<td width="50%"><img src="http://funfotobook.ru/_site/i/v2/vipusk/std_2.jpg" style="width:100%"/></td>
	</tr>
	<tr>
		<td colspan="2">
			<ul>
				<li>Цена <span class="text-red">от <b>340</b> руб</span></li>
				<li>Цифровая печать</li>
				<li>Мелованная бумага плотностью 220 г/м.кв. или 200 г/м.кв. + ламинация</li>
				<li>Минимальный объем - 14 страниц</li>
			</ul>
			<a href="http://funfotobook.ru/vypusknye_albomi/" class="btn">Подробнее &rarr;</a>
		</td>
	</tr>

	<tr>
		<td colspan="2">
			<h2>Контакты</h2>
			<p>Анна Дашевская, <br/> руководитель отдела по работе с клиентами FunFotoBook</p>
			<p>Звоните: <b>8 (800) 707-91-64</b> (бесплатный звонок со всех телефонов)</p>
			<p>Пишите <a href="mailto:info@funfotobook.ru">info@funfotobook.ru</a></p>
		</td>
	</tr>

</table>


</body>
</html>
