<?php

return array(
	'params' => [
		'cms' => [
			"auto_registration" => [
				"event" => 1
			],
			'is_cache_allow' => 0,
		],
	],
	'components' =>[
		'site' => [
			'class' => 'k_site',
		]
	]
);
