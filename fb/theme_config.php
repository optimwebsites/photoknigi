<?php

return array(
	'layouts'=>array(
		'landing'=>array('title'=>'Landing page', 'is_main'=>1),
		'right_column'=>array('title'=>'Right column', 'is_main'=>0),
	),
	'footer' => [
		'js' => [
			["/_site/js/jquery-2.1.3.min.js", 'publish', "base"],
			["/_th/assets/optimweb/base/_js/u.js", 'publish', "base"],
			["/_site/_js/site.js", 'publish' , "base"],
			["/_site/js/revolution/jquery.themepunch.tools.min.js", "dynamic"],
			["/_site/js/revolution/jquery.themepunch.revolution.min.js", "dynamic"],
			['/_site/_js/jquery.poptrox.min.js',"dynamic"],
			999 => ["/_site/_js/fb.js", "dynamic"],
		]
	],
	'header'=>[
		"css"=>[
			["/_th/assets/bootstrap/3.2.0/bootstrap.min.css", "dynamic"],
			["/_site/_css/revolution.css", "dynamic"],
			["/_site/_css/fb.css", "dynamic"],
		],
		"js"=>array(

		)
	],
	"design_mode" => [
		"css"=>array(
			array('/_c/page_editor/css/a/',"dynamic")
		),
		"js"=>array(
			array('/_th/assets/jquery-ui/jquery-ui-1.10.1.custom.min.js',"dynamic"),
			array('/_th/assets/jquery-slimscroll/jquery.slimscroll.min.js',"dynamic"),
			array('/_c/page_editor/js/a/',"dynamic"),
		)
	]

);
