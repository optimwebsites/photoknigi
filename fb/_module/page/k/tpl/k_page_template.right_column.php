<?php
$phone = new Phone(Yii::app()->params['cms']['phone']);
?><!DOCTYPE html>
<html lang="en">
<head>
	<title>##__page_title##</title>
	<meta content="##__description##" name="description" />
	<meta content="##__keywords##" name="keywords" />
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href='https://fonts.googleapis.com/css?family=Roboto:100,400,300,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
	##__header_css##
	##__header_js##
	##__headerscript##

</head>
<body  id_page="##id_page##" class="page-##page_name##">
<!-- Rating@Mail.ru counter -->
<script type="text/javascript">
	var _tmr = window._tmr || (window._tmr = []);
	_tmr.push({id: "2812500", type: "pageView", start: (new Date()).getTime()});
	(function (d, w, id) {
		if (d.getElementById(id)) return;
		var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
		ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
		var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
		if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
	})(document, window, "topmailru-code");
</script><noscript><div style="position:absolute;left:-10000px;">
		<img src="//top-fwz1.mail.ru/counter?id=2812500;js=na" style="border:0;" height="1" width="1" alt="Рейтинг@Mail.ru" />
	</div></noscript>
<!-- //Rating@Mail.ru counter -->
<header id="header" class="template_block" data-title="Шапка1">
	##header##
</header>

<main>
	<div class="container">

		<div class="main-header template_block" id="main_header" data-title="Заголовок страницы">
			##main_header##
		</div>
		<div class="template_block" id="main_gallery" data-title="Основная галерея">
			##main_gallery##
		</div>
		<div class="main-content">
			<div class="row">
				<div class="col-md-9  template_block"  id="main_content" data-title="Контент 1">##main_content##</div>
				<div class="col-md-3  template_block"  id="right_column" data-title="Колонка справа">##right_column##</div>
			</div>
		</div>

		<div class="template_block" id="content2" data-title="Контент 2">
			##content2##
		</div>

	</div> <!-- /container -->
</main>
<?= Template::_transform('k_page_template', 'footer', []); ?>

##__footer_js##

</body>
</html>