<?php
$phone = new Phone(Yii::app()->params['cms']['phone']);
?>
<footer>
	<div class="container">
		<div class="row">
			<div class="col-xs-6 col-sm-3">
				<h4>Фотокниги на заказ</h4>
				<ul class="link-list">
					<li><a href="/fotoknigi/svadebnye-fotoknigi/">Свадебные фотокниги</a></li>
					<li><a href="/fotoknigi/dmb/">Дембельские альбомы</a></li>
					<li><a href="/">Семейные фотокниги</a></li>
					<li><a href="/">Фотокниги путешествий</a></li>
					<li><a href="/">Фотокниги на юбилей</a></li>
				</ul>

			</div>
			<div class="col-xs-6 col-sm-3">
				<h4>Печать фотокниг</h4>
				<ul class="link-list">
					<li><a href="/fotoknigi/pechat/">Премиум печать</a></li>
					<li><a href="/fotoknigi/pechat/">Полиграфическая печать</a></li>
				</ul>
				<br/>
				<ul class="link-list">
					<li><a href="/blog/">Блог</a></li>
				</ul>
			</div>
			<div class="col-xs-6 col-sm-3">
				<h4>Выпускные альбомы</h4>
				<ul class="link-list">
					<li><a href="/vypusknye_albomi/">Фотокниги-бабочки</a></li>
					<li><a href="/vypusknye_albomi/">Выпускные альбомы премиум</a></li>
					<li><a href="/vypusknye_albomi/">Выпускные альбомы стандарт</a></li>
				</ul>
			</div>

			<div class="col-xs-6 col-sm-3">
				<h4>Контакты</h4>
				<p>Москва, Олимпийский пр-т, д. 22</p>
				<p class="footer__phone"><?=$phone->prettyFormat()?></p>
				<p class="footer__phone-free">бесплатно с любого телефона</p>
				<p class="footer__email"><a href="mailto:<?=Yii::app()->site->getEmail();?>"><?=Yii::app()->site->getEmail();?></a></p>
			</div>
		</div>
	</div>
</footer>