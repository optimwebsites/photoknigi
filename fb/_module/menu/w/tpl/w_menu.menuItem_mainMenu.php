<?php
/**
 * @var w_menu $this
 * @var array $page
 */
$page = $data['page'];

$class = "";



if (
	(isset($this->activeBranch[$page['id_page']])
		&& $this->activeBranch[$page['id_page']]['level'] ==  1)
	|| $this->activePage['id_page'] == $page['id_page']
) {
	$class.=' selected';
}


echo '<li class="'.$class.'"><a href="'.$page['path'].'">'.$page['title'].'</a>';
