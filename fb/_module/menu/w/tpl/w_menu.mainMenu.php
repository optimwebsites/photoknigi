<div class="primary">
  <div class="navbar navbar-default" role="navigation">
	<button type="button" class="navbar-toggle btn-navbar collapsed" data-toggle="collapse" data-target=".primary .navbar-collapse">
	  <span class="text">Меню</span>
	  <span class="icon-bar"></span>
	  <span class="icon-bar"></span>
	  <span class="icon-bar"></span>
	</button>
	<nav class="collapse collapsing navbar-collapse">
	  <ul class="nav navbar-nav navbar-center">
	  
	  <?php
		//все дочерние страницы первого уровня для главной
		foreach ($data['widget']->tree[0] as $id_page => $page) {

			
			echo $data['widget']->getMenuItemHtml($page);

			//для главного меню сдела только 0 и 1 уровни
			if (isset($data['widget']->tree[$id_page])) {
				foreach ($data['widget']->tree[$id_page] as $id_child_page => $child_page) {
					echo $data['widget']->getMenuItemHtml($child_page);
				}
			}
		}
	  
	  ?>
		<!--	<li class="selected">
	  <a href="index.html">Главная</a>
	</li>
	<li>
	  <a href="index.html">Сборка</a>
	</li>
	<li class="parent">
	  <a href="#">Каталог товаров</a>
	  <ul class="sub">
		<li class="parent">
		  <a href="#">Моторы</a>
		  <ul class="sub">
			<li><a href="header1.html">Мотор 1</a></li>
			<li><a href="header2.html">Мотор 2</a></li>
		  </ul>
		</li>
		<li><a href="sidebar-blocks.html">Пропеллеры</a></li>
		<li><a href="full-width.html">Рамы</a></li>
		<li><a href="left-sidebar.html">Подвесы для камер</a></li>
		<li class="parent">
		  <a href="about-us.html">Производители</a>
		  <ul class="sub">
			<li><a href="about-us.html">производитель такой-то</a></li>
		  </ul>
		</li>
	  </ul>
	</li>
	<li>
	  <a href="shop.html">Блог</a>
	</li>
	<li>
	  <a href="#">Контакты</a>
	</li>
 -->
	  </ul>
	</nav>
  </div>
</div>