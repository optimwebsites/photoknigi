<?php
/**
 * @var w_menu $this
 */
?>
<ul class="breadcrumb">
	<?php
	$pages = array_reverse($this->activeBranch);

	foreach ($pages as $p) {
		if ($p['id_page'] == $this->activePage['id_page'])
			echo '<li>'.$p['title'].'</li>';
		else
			echo '<li><a href="'.$p['path'].'">'.$p['title'].'</a></li>';
	}
	?>
</ul>