<?php

$class = '';
$page = $data['page'];

if ($page['id_page'] == $data['widget']->activePage['id_page'] ) {
	$class .= ' active';
}

$parent = false;
if (isset($data['widget']->tree[$page['id_page']])) {
	$class .= ' parent';
	$parent = true;
}
	
echo '<li class="'.$class.'">';

echo '<a href="'.$page['path'].'">'.$page['title'].'</a>';

if ($parent) {
	echo '<ul class="sub">';
	foreach ($data['widget']->tree[$page['id_page']] as $child_page) {
		echo $data['widget']->getMenuItemHtml($child_page);
	}
	echo "</ul>";
	
}

echo '</li>';