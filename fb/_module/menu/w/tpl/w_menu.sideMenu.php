<nav class="sidebar">
<aside>
<?php

if (!empty($data['widget']->menuTitle)) {
	echo '<h3 class="title">'.$data['widget']->menuTitle.'</h3>';
}
?>
<ul class="menu">
<?php

foreach ($data['widget']->tree[$data['widget']->parentPageId] as $page) {
	echo $data['widget']->getMenuItemHtml($page);			
}

?>
</ul>
</aside>
</nav>
		