<?php

return array(
	
	//проверка int, обязательных для заполнения
	array(
		'id_client',
		'numerical',
		'allowEmpty' => true,
	),
	//проверка int, НЕобязательных для заполнения
	//array(
	//	'',
	//	'numerical',
	//	'allowEmpty' => true,
	//),
	//проверка string, обязательных для заполнения
	array(
		'firstname',
		'required'
	),
	//удаление тегов
	array(
		'lastname, firstname, middlename, passport_seria, passport_number, passport_date, passport_kem, address, fax, phone, email',
		'filter',
		'filter' => 'strip_tags',
	),
	//проверка date, обязательных для заполнения
	//array(
	//	'',
	//	'date',
	//	'format' => 'yyyy-MM-dd',
	//	'allowEmpty' => false,
	//),
	//проверка date, НЕобязательных для заполнения
	array(
		'date_birthday',
		'date',
		'format' => 'yyyy-MM-dd',
		'allowEmpty' => true,
	),
	//безопасные аттрибуты при insert. УДАЛИТЬ ОТСЮДА ТО, ЧТО НЕ ДОЛЖНО ПРИХОДИТЬ С ФОРМЫ В АДМИНКЕ ПРИ СОЗДАНИИ ЗАПИСИ
	array(
		'id_client, lastname, firstname, middlename, date_birthday, passport_seria, passport_number, passport_date, passport_kem, address, fax, phone, email',
		'safe',
		'on' => 'insert',
	),
	//безопасные аттрибуты при update. УДАЛИТЬ ОТСЮДА ТО, ЧТО НЕ ДОЛЖНО ПРИХОДИТЬ С ФОРМЫ В АДМИНКЕ ПРИ СОЗДАНИИ ЗАПИСИ
	array(
		'id_client, lastname, firstname, middlename, date_birthday, passport_seria, passport_number, passport_date, passport_kem, address, fax, phone, email',
		'safe',
		'on' => 'update',
	),	
);