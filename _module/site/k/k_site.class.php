<?php
/**
 * Created by PhpStorm.
 * User: nikolay
 * Date: 31.08.16
 * Time: 19:43
 */

class k_site extends CApplicationComponent {

	const SITE_PECHATAI_RU    = 'pechatai.ru';
	const SITE_FUNFOTOBOOK_RU = 'funfotobook.ru';

	private $site = self::SITE_FUNFOTOBOOK_RU;

	public function init()
	{
		if (isset($_SERVER['HTTP_HOST']) && self::SITE_PECHATAI_RU == $_SERVER['HTTP_HOST']) {
			$this->site = self::SITE_PECHATAI_RU;
		}
	}

	public function getEmail()
	{
		return $this->site == self::SITE_PECHATAI_RU ?
			'info@pechatai.ru' :
			'info@funfotobook.ru';
	}


	public function getName()
	{
		return $this->site == self::SITE_PECHATAI_RU ?
			'Печатай.ру' :
			'FunFotoBook.ru';
	}
}