<?php

return [
    'default' => [
        "theme_variant"     => [
            "value"    => "blackandwhite",
            "title"    => "Вариант темы",
            "editable" => true,
        ],
        "themes"     => [
            "value"    => ["pgsv", "fb"],
            "title"    => "Темы",
            "editable" => true,
        ],
        "auto_registration" => [
            "value" => ["html" => 1, "seo" => 1, 'photobook' => 1],
        ],
        'hosts' => [
            'value' => [
                'front' => 'funfotobook.ru',
            ],
            'title' => 'Хосты',
            'editable' => true,
        ],
        'admin_email' => [
            'value' => 'info@funfotobook.ru',
            'title' => 'E-mail администратора сайта',
            'editable' => true,
        ],
        'phone' => [
            'value' => '88007079164',
            'title' => 'Телефон',
            'editable' => true,
        ],
    ],
    'dev'     => [
        "is_debug"       => [
            "value"    => 1,
            "title"    => "Режим отладки",
            "editable" => true,
        ],
        'env'            => [
            'value'    => 'dev',
            'title'    => 'Имя контура (dev/prod)',
            'editable' => true,
        ],
        "is_cache_allow" => [
            "value"    => 0,
            "title"    => "Кеширование включено",
            "editable" => true,
        ],
        'hosts' => [
            'value' => [
                'front' => 'nz7zia.srv001.optimweb.ru',
            ],
            'title' => 'Хосты',
            'editable' => true,
        ]
    ]
];