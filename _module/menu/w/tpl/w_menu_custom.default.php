<?php
/**
 * @var w_menu_custom $this
 */
?>
<div>
    <nav>
        <ul class="nav-main-menu">
            <?php
            foreach ($this->items as $path => $link) {
                $class = $path == $this->page ? 'class="active-item"' : "";
                echo ' <li '.$class.'><a href="'.$link['href'].'">'.$link['title'].'</a></li>';
            }
            ?>
        </ul>
        <div class="clearfix"></div>
        <?php if (isset($this->sub_items[$this->page])) { ?>
            <ul class="nav-submenu min1000">
                <?php
                foreach ($this->sub_items[$this->page] as $path => $link) {

                    echo ' <li>'.CHtml::link($link['title'], $link['href'], $link['attr']).'</a></li>';
                }
                ?>
            </ul>
        <?php } ?>

    </nav>
    <div class="clearfix"></div>

</div>