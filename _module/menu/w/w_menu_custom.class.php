<?php
/**
 * Created by PhpStorm.
 * User: nikolay
 * Date: 13.01.16
 * Time: 9:44
 */

class w_menu_custom extends widget {

    public $items = [];

    public $sub_items = [];

    public $page = 'main';

    public function run()
    {
        $this->getItems();

        $this->page = Page::page()->m_page->getPageUrl();

    }

    public function getItems()
    {
        $this->items = [
            '/' => ['title' => 'Фотокиги на заказ', 'href' => '/'],
            '/vypusknye_albomi/' => ['title' => 'Выпускные фотоальбомы', 'href' => '/vypusknye_albomi/'],
            '/fotoknigi/pechat/' => ['title' => 'Печать фотокниг', 'href' => '/fotoknigi/pechat/'],
            '/fotoknigi/portfolio/' => ['title' => 'Наши работы', 'href' => '/fotoknigi/portfolio/'],
            //'/fotoknigi/konstruktor_fotoknig/' => ['title' => 'Конструктор', 'href' => '/fotoknigi/konstruktor_fotoknig/'],
        ];

        $this->sub_items = [
            '/' => [
                ['title' => 'Как мы работаем', 'href' => "#photobooks",  'attr' => ["class" => "scroll-to"]],
                ['title' => 'Наши цены', 'href' => "#price-panel", 'attr' => ["class" => "scroll-to"]],
                ['title' => 'Заказать фотокнигу', 'href' => "#photobooks", 'attr' => ["data-toggle"=>"modal", "data-target"=>"#orderBook"]],
            ],
            '/vypusknye_albomi/' => [
                ['title' => 'Печать выпускных фотоальбомов', 'href' => "#printalbum",  'attr' => ["class" => "scroll-to"]],
                ['title' => 'Как мы работаем', 'href' => "#photobooks",  'attr' => ["class" => "scroll-to"]],
                ['title' => 'Наши цены', 'href' => "#price-panel", 'attr' => ["class" => "scroll-to"]],
               // ['title' => 'Калькулятор', 'href' => "#aCalculator", 'attr' => ["class" => "scroll-to"]],
                ['title' => 'Заказать', 'href' => "#photobooks", 'attr' => ["data-toggle"=>"modal", "data-target"=>"#orderBook"]],
            ],
            '/fotoknigi/pechat/' => [
                ['title' => 'Оборудование', 'href' => "#equipments",  'attr' => ["class" => "scroll-to"]],
                ['title' => 'Наши цены', 'href' => "#price-panel", 'attr' => ["class" => "scroll-to"]],
                ['title' => 'Оставить заявку', 'href' => "#photobooks", 'attr' => ["data-toggle"=>"modal", "data-target"=>"#orderBook"]],
            ],

        ];
    }
}