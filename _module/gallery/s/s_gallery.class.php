<?php
class s_gallery extends controller
{
	protected $accessRules = array("*"=>array("*"=>1));

	public function galleryActionRouter($m_page_module)
	{
		if (isset($_GET['gallery'])) {
			$gallery = $_GET['gallery'];
			if ($gallery === "dembelskiy-albom") {
				Yii::app()->request->redirect("/fotoknigi/dmb/", true, 301);
			}

			$this->tplFile = "showGallery";
			$m_page_module->cms_class_action->widget_name = 'gallery';
			$m_page_module->cms_class_action->id_cms_class_action = 'showGallery';
			$widget_attributes = json_decode($m_page_module->widget_attributes, true);
			$widget_attributes['name'] = ['value' => $_GET['gallery']];
			$m_page_module->widget_attributes = json_encode($widget_attributes);
			return $this->showGallery($m_page_module);
		} else {
			$this->tplFile = "showGalleryList";
			return $this->showGalleryList($m_page_module);
		}
	}

	/**
	 * Покаать список галерей в какой-то группе
	 *
	 * @param m_page_module $m_page_module
	 * @return widget
	 */
	function showGalleryList(m_page_module $m_page_module)
	{
		$id_gallery_list = Yii::app()->request->getQuery('gallery_list');
		if (empty($id_gallery_list)) {
			$id_gallery_list = $m_page_module->id_list;
		}

		return $m_page_module->getWidget('gallery_list', ['id_gallery_list' => $id_gallery_list]);
	}
	
	/**
	* вывод фотогалереи
	*/
	function showGallery(m_page_module $m_page_module)
	{
		$w = $m_page_module->getWidget();
		return $w;
	}

	/**
	 * @param m_page_module $m_page_module
	 * @return w_page
	 */
	public function showGalleryHead(m_page_module $m_page_module)
	{
		$gallery = $this->_getGallery();

		$w = new w_page();
		$w->tpl = 'head';
		if ($gallery) {
			$head = empty($gallery->seo_h1) ? $gallery->gallery_title : $gallery->seo_h1;
			$page_title = empty($gallery->seo_title) ? $gallery->gallery_title : $gallery->seo_title;
			Page::page()->setHead($head);
			Page::page()->setTitle($page_title);
			$w->head = $head;

		} else {
			$gallery_list = m_gallery_list::model()->findByPk($m_page_module->id_list);
			if ($gallery_list) {
				Page::page()->setHead($gallery_list->list_title);
				Page::page()->setTitle($gallery_list->list_title);
				$w->head = $gallery_list->list_title;
			}
		}

		return $w;
	}


	public function showGalleryListMenu(m_page_module $m_page_module)
	{
		$gallery_list = m_gallery_list::model()->findByPk($m_page_module->id_list);

		if (!$gallery_list) {
			return null;
		}

		$gallery = $this->_getGallery();

		$tree_arr = [];
		foreach ($gallery_list->gallery as $g) {
			$tree_arr[0][] = [
				'id_page' => $g->id_gallery,
				'path' => Page::page()->m_page->path."/gallery/" . $g->rewrite_name,
				'level' => 0,
				'id_parent' => 0,
				'title' =>  $g->gallery_title,
	  			'target' => null,
			];
		}

		$data['tree'] = $tree_arr;

		$data['activePage'] = [
			'id_page' => $gallery ? $gallery->id_gallery : null,
			'id_parent' => 0
		];

		$id_menu_template = $m_page_module->page_module_data[0]->data;
		$template = m_menu_template::model()->findByPk($id_menu_template);
		$data['tpl'] = $template->template;

		return $m_page_module->getWidget('menu', $data);
	}

	/**
	 * @return m_gallery|null
	 */
	private function _getGallery()
	{
		$gallery = null;
		if (isset($_GET['gallery'])) {
			$gallery = m_gallery::model()->byRewriteName($_GET['gallery'])->find();
		}

		return $gallery;
	}
}