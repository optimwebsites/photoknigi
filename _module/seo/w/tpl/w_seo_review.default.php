<?php
/**
 * @var w_seo_review $this
 */
?>
<div class="row">
    <style>
        .rating-description {
            padding: 0 0 10px 0;
            font-size:80%;
        }
        .rating-recommendation {
            font-size:90%;
            font-weight:bold;
            padding:10px 0;
        }
        .review-garant {
            font-size: 80%;
            padding-top:10px;
        }

        .review-garant img {
            width: 120px;
        }
        .rating-value {
            font-size: 90%;
        }
        .rating-stars {
            display: inline-block;
            width:125px;
            height:23px;
            background: url(/_site/i/stars-big.png) no-repeat;
        }
        .rating-total {
            font-weight:bold;
            font-size:110%;
        }
        .review-name {
            margin-bottom:10px;
            font-weight:bold;
        }
        .review-text blockquote {
            font-style: italic;
            font-size:90%;
        }
        .rating-start-small {
            background-size: 60%;
            height:15px;
        }
    </style>
    <div class="col-md-4">
        <div>
            <h4><?=$this->seo->review_title?></h4>
            <div class="rating-description">Стильные, яркие, качественные фотокниги</div>
            <div>
                <div class="rating-value"><div class="rating-stars"></div>
                    <br/> Средний рейтинг <span itemprop="ratingValue" class="rating-total">5</span>
                    на основании <span>3</span> отзывов
                </div>
            </div>
            <div class="rating-recommendation">100% покупателей рекомендуют этот товар</div>
            <p class="review-garant"><img src="/_site/i/logo_review.png" /> гарантирует достоверность отзывов</p>
        </div>
    </div>
    <div class="col-md-8">
        <ul class="list-unstyled">
        <?php foreach ($this->reviews as $r) { ?>
            <li>
                <div class="row review-name">
                    <div class="col-sm-6"><?=$r->name?>, <?=$r->city?> <div class="rating-stars rating-start-small"></div></div>
                    <div class="col-sm-6 text-right"><?=Utils::_dateFromMysql($r->date_rating)?></div>
                </div>
                <div class="review-text"><blockquote><?=$r->review?></blockquote></div>
            </li>
        <?php } ?>
        </ul>
    </div>
</div>