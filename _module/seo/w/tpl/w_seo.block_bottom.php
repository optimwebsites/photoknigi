<?php
/**
 * @var w_seo $this
 */
?>
<div class="row">
    <div class="col-sm-12">
        <?php
        $w_description = new w_seo();
        $w_description->tpl = "text_bottom";
        echo $w_description->getHtml();
        ?>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <?php
        $w = new w_html();
        $w->tpl = "book_price_inner";
        echo $w->getHtml();
        ?>
    </div>
</div>
<?php if (count($this->seo->seo_review)) { ?>
<div class="row">
    <div class="col-xs-12 bottom-padding-mini">
        <div class="title-box">
            <h3 class="title">Отзывы</h3>
        </div>
        <?php
        $w = new w_seo_review();
        $w->seo = $this->seo;
        $w->reviews = $this->seo->seo_review;
        echo $w->getHtml();
        ?>
    </div>
</div>
<?php } ?>
<div class="row">
    <div class="col-xs-12 bottom-padding-mini">
        <div class="title-box">
            <h3 class="title"><?= !empty($this->seo->delivery_title) ? $this->seo->delivery_title : "Мы доставляем наши фотокниги по всей России" ?></h3>
        </div>

        <?php
        $delivery_text = new w_html();
        $delivery_text->tpl = "cities";
        echo $delivery_text->getHtml();
        ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 bottom-padding-mini">
        <div class="title-box">
            <h3 class="title">Наши фотокниги</h3>
        </div>
        <?php
        $w = new w_gallery_list();
        $w->setProperties([
            'id_gallery_list' => 12,
            'widget' => "gallery_photolist",
            'base_path' => '/fotoknigi/portfolio',
            'title' => 'Примеры фотокниг',
            'widget_params' => [
                'columns' => 6,
            ]
        ]);
        echo $w->getHtml();
        ?>
    </div>
</div>
