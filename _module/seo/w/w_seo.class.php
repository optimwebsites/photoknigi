<?php
/**
 * Created by PhpStorm.
 * User: nikolay
 * Date: 26.11.15
 * Time: 0:08
 */

class w_seo extends widget
{
    /**
     * @var string
     */
    public $gallery = null;

    /**
     * @var m_seo
     */
    public $seo = null;


    public function run()
    {
        $this->gallery = Yii::app()->request->getQuery('gallery');

        if (empty($this->gallery)) {
            return " ";
        }

        $this->seo = m_seo::model()->byGallery($this->gallery)->find();

        if (!$this->seo) {
            return " ";
        }
    }

}