<?php
/**
 * Created by PhpStorm.
 * User: nikolay
 * Date: 26.11.15
 * Time: 0:12
 */

class s_seo extends controller {

    protected $accessRules = array("*" => array("*" => 1));

    public function showSeo(m_page_module $pm)
    {
        return $pm->getWidget('seo', ['tpl' => 'text_top']);
    }

    public function showSeoBottom(m_page_module $pm)
    {
        return $pm->getWidget('seo', ['tpl' => 'block_bottom']);
    }
}