<?php
/**
* модель m_seo_review
*
*/

class m_seo_review extends m_seo_review_base
{
	/**
	 * Returns the static model of the specified AR class.
     * @param string $className
	 * @return m_seo_review the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function beforeValidate()
	{
		if (!empty($this->date_rating)) {
			$this->date_rating = date('Y-m-d', strtotime($this->date_rating));
		} else {
			$this->date_rating = null;
		}

		return true;
	}
	
}
