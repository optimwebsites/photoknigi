<?php
$model = array(
	'class_name'=>'m_seo',
	'table' => 'seo',
	'pk'=>'id_seo',
	'auto_increment'=>'id_seo',
	'fields'=> array(
		'id_seo' => 
		  array (
			'Type'=>'int',
			'Null'=>'NOT NULL',
			'Default'=>'',
			'length'=>11,
			'is_required' => 0,
			'reg_name' => "",
			'title'=>'ID'
		  ),
		'gallery' =>
			array (
				'Type'=>'varchar',
				'Null'=>'NOT NULL',
				'Default'=>'',
				'length'=> 255,
				'is_required' => 1,
				'reg_name' => NULL,
				'title'=>'Имя галереи'
			),
		'menu' =>
		  array (
			'Type'=>'text',
			'Null'=>'NULL',
			'Default'=>'',
			'length'=> '',
			'is_required' => 1,
			'reg_name' => NULL,
			'title'=>'код для меню'
		  ),
		'text' =>
		  array (
			'Type'=>'text',
			'Null'=>'NULL',
			'Default'=>'',
			'length'=> '',
			'is_required' => 1,
			'reg_name' => null,
			'title'=>'Текст'
		  ),
		'text_top' =>
			array (
				'Type'=>'text',
				'Null'=>'NULL',
				'Default'=>'',
				'length'=> '',
				'is_required' => 1,
				'reg_name' => null,
				'title'=>'Текст вверху'
			),
		'delivery_title' =>
			array (
				'Type'=>'text',
				'Null'=>'NULL',
				'Default'=>'',
				'length'=> '',
				'is_required' => 0,
				'reg_name' => null,
				'title'=>'Заголовок для доставки'
			),
		'review_title' => [
			'Type'=>'text',
			'Null'=>'NULL',
			'Default'=>'',
			'length'=> '',
			'is_required' => 0,
			'reg_name' => null,
			'title'=>'Заголовок для отзывов'
		],
	),
	"indexes"=> array(
		),
	"fk"=>array(
		
	),
	"form"=>array(
	),
	"relations"=>array(
		"seo_review"=>array("HAS_MANY", "m_seo_review", "id_seo"),

	),
	"defaultScope"=>array(
		),		
	"filters" => array(
	)
);
?>