<?php

return array(
	
	//проверка int, обязательных для заполнения
	array(
		'id_seo, rating',
		'numerical',
		'allowEmpty' => false,
	),
	//проверка int, НЕобязательных для заполнения
	//array(
	//	'',
	//	'numerical',
	//	'allowEmpty' => true,
	//),
	//проверка string, обязательных для заполнения
	array(
		'name, review, city, goods_name',
		'required'
	),
	//удаление тегов
	array(
		'name, review, city, date_rating',
		'filter',
		'filter' => 'strip_tags',
	),
	//проверка date, обязательных для заполнения
	//array(
	//	'',
	//	'date',
	//	'format' => 'yyyy-MM-dd',
	//	'allowEmpty' => false,
	//),
	//проверка date, НЕобязательных для заполнения
	//array(
	//	'',
	//	'date',
	//	'format' => 'yyyy-MM-dd',
	//	'allowEmpty' => true,
	//),
	//безопасные аттрибуты при insert. УДАЛИТЬ ОТСЮДА ТО, ЧТО НЕ ДОЛЖНО ПРИХОДИТЬ С ФОРМЫ В АДМИНКЕ ПРИ СОЗДАНИИ ЗАПИСИ
	array(
		'id_seo, name, review, rating, city, date_rating, goods_name',
		'safe',
		'on' => 'insert',
	),
	//безопасные аттрибуты при update. УДАЛИТЬ ОТСЮДА ТО, ЧТО НЕ ДОЛЖНО ПРИХОДИТЬ С ФОРМЫ В АДМИНКЕ ПРИ СОЗДАНИИ ЗАПИСИ
	array(
		'id_seo, name, review, rating, city, date_rating, goods_name',
		'safe',
		'on' => 'update',
	),	
);