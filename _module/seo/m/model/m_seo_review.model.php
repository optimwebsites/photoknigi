<?php
$model = array(
	'class_name'=>'m_seo_review',
	'table' => 'seo_review',
	'pk'=>'id_seo_review',
	'auto_increment'=>'id_seo_review',
	'fields'=> array(
		'id_seo_review' => 
		  array (
			'Type'=>'int',
			'Null'=>'NOT NULL',
			'Default'=>'',
			'length'=>11,
			'is_required' => 0,
			'reg_name' => "",
			'title'=>'ID'
		  ),
		'id_seo' =>
			array (
				'Type'=>'int',
				'Null'=>'NOT NULL',
				'Default'=>'',
				'length'=>11,
				'is_required' => 1,
				'reg_name' => "",
				'title'=>'ID'
			),
		'goods_name' =>
			array (
				'Type'=>'varchar',
				'Null'=>'NOT NULL',
				'Default'=>'',
				'length'=> 255,
				'is_required' => 1,
				'reg_name' => NULL,
				'title'=>'Название товара'
			),
		'name' =>
			array (
				'Type'=>'varchar',
				'Null'=>'NOT NULL',
				'Default'=>'',
				'length'=> 255,
				'is_required' => 1,
				'reg_name' => NULL,
				'title'=>'Имя клиента'
			),
		'review' =>
		  array (
			'Type'=>'text',
			'Null'=>'NULL',
			'Default'=>'',
			'length'=> '',
			'is_required' => 1,
			'reg_name' => NULL,
			'title'=>'Отзыв'
		  ),
		'rating' =>
		  array (
			'Type'=>'tinyint',
			'Null'=>'NULL',
			'Default'=>'',
			'length'=> '',
			'is_required' => 1,
			'reg_name' => null,
			'title'=>'Оценка'
		  ),
		'city' =>
			array (
				'Type'=>'varchar',
				'Null'=>'NULL',
				'Default'=>'',
				'length'=> 255,
				'is_required' => 1,
				'reg_name' => null,
				'title'=>'Город'
			),
		'date_rating' =>
			array (
				'Type'=>'date',
				'Null'=>'NULL',
				'Default'=>'',
				'length'=> '',
				'is_required' => 0,
				'reg_name' => null,
				'title'=>'Дата отзыва'
			),
	),
	"indexes"=> array(
		),
	"fk"=>array(
		"id_seo"
	),
	"form"=>array(
	),
	"relations"=>array(
		"seo"=>array("BELONGS_TO", "m_seo", "id_seo"),
		),
	"defaultScope"=>array(
		),		
	"filters" => array(
		'id_seo' => array(
			"query"=>array("condition"=>"t.id_seo=:id_seo"),
			"args"=>array("id_seo"=>0),
		),
	)
);
