<?php

return array(
	
	//проверка int, обязательных для заполнения
	//array(
	//	'',
	//	'numerical',
	//	'allowEmpty' => false,
	//),
	//проверка int, НЕобязательных для заполнения
	//array(
	//	'',
	//	'numerical',
	//	'allowEmpty' => true,
	//),
	//проверка string, обязательных для заполнения
	array(
		'gallery, menu, text',
		'required'
	),
	//удаление тегов
	array(
		'gallery',
		'filter',
		'filter' => 'strip_tags',
	),
	//проверка date, обязательных для заполнения
	//array(
	//	'',
	//	'date',
	//	'format' => 'yyyy-MM-dd',
	//	'allowEmpty' => false,
	//),
	//проверка date, НЕобязательных для заполнения
	//array(
	//	'',
	//	'date',
	//	'format' => 'yyyy-MM-dd',
	//	'allowEmpty' => true,
	//),
	//безопасные аттрибуты при insert. УДАЛИТЬ ОТСЮДА ТО, ЧТО НЕ ДОЛЖНО ПРИХОДИТЬ С ФОРМЫ В АДМИНКЕ ПРИ СОЗДАНИИ ЗАПИСИ
	array(
		'gallery, menu, text, text_top, delivery_title,review_title',
		'safe',
		'on' => 'insert',
	),
	//безопасные аттрибуты при update. УДАЛИТЬ ОТСЮДА ТО, ЧТО НЕ ДОЛЖНО ПРИХОДИТЬ С ФОРМЫ В АДМИНКЕ ПРИ СОЗДАНИИ ЗАПИСИ
	array(
		'gallery, menu, text, text_top, delivery_title, review_title',
		'safe',
		'on' => 'update',
	),	
);