<?php

/**
*   Базовый класс-модель m_seo_base
*	Рализует основное поведение модели, описанное в файле /_module/seo/m/m_seo_base.class.php
*	НЕ ИЗМЕНЯТЬ ГЕНЕРИРУЕТСЯ АВТОМАТИЧЕСКИ ПРИ ИЗМЕНЕНИИ ФАЙЛА /_module/seo/m/model/m_seo.model.php
*/

/**
 * @property int $id_seo
 * @property string $gallery
 * @property string $menu
 * @property string $text
 * @property string $text_top
 * @property string $delivery_title
 * @property string $review_title
 * @property m_seo_review $seo_review
 */
abstract class m_seo_base extends BaseModel {

	
	/**
	 * @return string
	 */
	public function tableName()
	{
		return 'seo';
	}

	/**
	 * @return string
	 */
	public function primaryKey()
	{
		return 'id_seo';
	}

	/**
	 * Returns the attribute labels.
	 * Attribute labels are mainly used in error messages of validation.
	 * By default an attribute label is generated using {@link generateAttributeLabel}.
	 * This method allows you to explicitly specify attribute labels.
	 *
	 * Note, in order to inherit labels defined in the parent class, a child class needs to
	 * merge the parent labels with child labels using functions like array_merge().
	 *
	 * @return array attribute labels (name=>label)
	 * @see generateAttributeLabel
	 */
	public function attributeLabels()
	{
		return array("id_seo"=>"ID",
			"gallery"=>"Имя галереи",
			"menu"=>"код для меню",
			"text"=>"Текст",
			"text_top"=>"Текст вверху",
			"delivery_title"=>"Заголовок для доставки",
			"review_title"=>"Заголовок для отзывов");
	}

    /**
     * Зависимости для модели
     * @return array
     */
    public function relations()
    {
        return [
                "seo_review" => [self::HAS_MANY, "m_seo_review", "id_seo"],
            ];
    }

	/**
	*  перед удалением удаляем данные во всех зависимых таблицах
	*  если не запрещено
	*/
	public function beforeDelete()
	{

		$restrict = array();
		foreach ($this->relations() as $rel => $r)
		{
			if ($r[0] === self::HAS_MANY)
			{
                /** @var CActiveRecord $d */
				foreach ($this->$rel as $d) {
					$d->delete();
                }
			}
		}

		return true;
	}

	/*
	*   Правила валидации для модели подключаются через внешний файл
	*	файл /_module/seo/m/model/m_seo.rules.php	*/
	public function rules()
	{
		$rules = Modules::_find('m_seo','model','rules');
		if ($rules)
			return include($rules);
		else
			return array();
	}




}
