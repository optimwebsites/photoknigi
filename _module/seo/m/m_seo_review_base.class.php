<?php

/**
*   Базовый класс-модель m_seo_review_base
*	Рализует основное поведение модели, описанное в файле /_module/seo/m/m_seo_review_base.class.php
*	НЕ ИЗМЕНЯТЬ ГЕНЕРИРУЕТСЯ АВТОМАТИЧЕСКИ ПРИ ИЗМЕНЕНИИ ФАЙЛА /_module/seo/m/model/m_seo_review.model.php
*/

/**
 * @property int $id_seo_review
 * @property int $id_seo
 * @property string $goods_name
 * @property string $name
 * @property string $review
 * @property int $rating
 * @property string $city
 * @property string $date_rating
 * @property m_seo $seo
 */
abstract class m_seo_review_base extends BaseModel {

	
	/**
	 * @return string
	 */
	public function tableName()
	{
		return 'seo_review';
	}

	/**
	 * @return string
	 */
	public function primaryKey()
	{
		return 'id_seo_review';
	}

	/**
	 * Returns the attribute labels.
	 * Attribute labels are mainly used in error messages of validation.
	 * By default an attribute label is generated using {@link generateAttributeLabel}.
	 * This method allows you to explicitly specify attribute labels.
	 *
	 * Note, in order to inherit labels defined in the parent class, a child class needs to
	 * merge the parent labels with child labels using functions like array_merge().
	 *
	 * @return array attribute labels (name=>label)
	 * @see generateAttributeLabel
	 */
	public function attributeLabels()
	{
		return array("id_seo_review"=>"ID",
			"id_seo"=>"ID",
			"goods_name"=>"Название товара",
			"name"=>"Имя клиента",
			"review"=>"Отзыв",
			"rating"=>"Оценка",
			"city"=>"Город",
			"date_rating"=>"Дата отзыва");
	}

    /**
     * Зависимости для модели
     * @return array
     */
    public function relations()
    {
        return [
                "seo" => [self::BELONGS_TO, "m_seo", "id_seo"],
            ];
    }

	/**
	*  перед удалением удаляем данные во всех зависимых таблицах
	*  если не запрещено
	*/
	public function beforeDelete()
	{

		$restrict = array();
		foreach ($this->relations() as $rel => $r)
		{
			if ($r[0] === self::HAS_MANY)
			{
                /** @var CActiveRecord $d */
				foreach ($this->$rel as $d) {
					$d->delete();
                }
			}
		}

		return true;
	}

	/*
	*   Правила валидации для модели подключаются через внешний файл
	*	файл /_module/seo/m/model/m_seo_review.rules.php	*/
	public function rules()
	{
		$rules = Modules::_find('m_seo_review','model','rules');
		if ($rules)
			return include($rules);
		else
			return array();
	}



	public function filter_id_seo($id_seo=0)
	{

		$this->getDbCriteria()->mergeWith(array(
			"condition"=>"t.id_seo=:id_seo",
				'params'=>array(":id_seo"=>$id_seo)
		));
		return $this;
	}

}
