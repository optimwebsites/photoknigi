<?php
/**
* модель m_seo
*
*/

class m_seo extends m_seo_base
{
	/**
	 * Returns the static model of the specified AR class.
     * @param string $className
	 * @return m_seo the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
}
