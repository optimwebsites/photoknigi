<?php

return [
    'default' => [
        "admin_email" => [
            "value"    => "info@funfotobook.ru",
            "title"    => "E-mail для отправки уведомлений при сохранении формы обратной связи",
            "editable" => true,
        ],
        'admin_phone' => [
            'value'    => '79625050945',
            'title'    => 'Номер телефона для уведомлений об обратной связи',
            'editable' => true,
        ],
    ],
];
