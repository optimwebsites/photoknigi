<div>
    <div id="orderBook" class="modal fade" data-state="default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть">
                        <span aria-hidden="true">X</span>
                    </button>
                    <div id="saveCallbackForm_container" class="show">
                        <div id="callback_button">
                        <form class="register-form contact-form" method="post" action="/_j/callback/save/s/" id="saveCallbackForm">
                            <input type="hidden" class="form-control" name="m_callback[msg]"/>
                            <br/><h3 id="register-form-title" class="text-center">
                               Мы перезвоним Вам через 30 секунд
                            </h3>
                            <div class="row">
                                <div class="col-xs-12 col-sm-12"><label>Телефон</label>
                                <input type="text" class="form-control" name="m_callback[phone]" /></div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="buttons-box clearfix">
                                <div class="alert hide" id="formMsg_saveCallbackForm"></div>
                                <button type="button" class="btn btn-warning form-submit" data-form="saveCallbackForm">
                                    Жду звонка
                                </button>
                            </div>
                        </form>
                        </div>
                        <div id="callback_60"  class="text-center">
                            <h3>Bы были у нас на сайте более 2 минут. Вы нашли то, что искали?</h3>
                            <p><button class="btn btn-warning" id="callback_60_yes">Да. нашли</button>
                                <button class="btn  btn-default" id="callback_60_no">Нет, не нашли</button></p>
                            <br/><p>Мы искренне ценим вас. Нам важно ваше мнение, чтобы стать лучше.</p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        var reachedGoals = {startOrder:false, order:false, orderButton:false};

        //подписываемся на событие успешного сабмита
        $(document).on('form_submit_success', function(e, data) {
            $("button.form-submit").hide();
        });

        $('#orderBook').on('show.bs.modal', function () {
            showCallBackForm('default');
        });

        $('#orderBook').on('shown.bs.modal', function () {
            $(document).trigger("reachGoal", {"name": "showModal"});
        });

        $("[name='m_callback[phone]']").click(function () {
            $(document).trigger("reachGoal", {name: "startPhoneEnter"});
        });

        $("button.form-submit").click(function () {
                $(document).trigger("reachGoal", {name: "orderCallback"});
        });

        $(document).on("reachGoal", function (e, data) {
            if (window['yaCounter30888196'] && !reachedGoals[data.name]) {
                yaCounter30888196.reachGoal(data.name);
            }

            reachedGoals[data.name] = true;
        });

        $("#callback_60_yes").click(function() {
            showCallBackForm('60_sec_yes');
            return false;
        });
        $("#callback_60_no").click(function() {
            showCallBackForm('60_sec_no');
            return false;
        });

        var callBack60 = null;

        function showCallBackForm(state)
        {
            if (callBack60) {
                clearTimeout(callBack60);
            }

            if (state=='default') {
                $("#register-form-title").html("Мы перезвоним Вам через 30 секунд");
                $("#callback_60").hide();
                $("#callback_button").show();
            }

            if (state == "60_sec") {
                $("#register-form-title").html("Bы искали информацию на этой странице больше 2 минут. <br/>Вы нашли то, что искали?");
                $("#callback_60").show();
                $("#callback_button").hide();
                $(document).trigger("reachGoal", {name: "show60Sec"});
            }

            if (state == "60_sec_yes") {
                $("#register-form-title").html("Мы очень рады. Давайте мы перезвоним вам бесплатно за 30 секунд и обсудим детали?");
                $("#callback_60").hide();
                $("#callback_button").show();
                $(document).trigger("reachGoal", {name: "show60SecYes"});
            }

            if (state == "60_sec_no") {
                $("#register-form-title").html(" Давайте сэкономим Ваше время и перезвоним вам бесплатно?");
                $("#callback_60").hide();
                $("#callback_button").show();
                $(document).trigger("reachGoal", {name: "show60SecNo"});
            }
        }

        callBack60 = setTimeout(function() {
            $("#orderBook").modal('show');
            showCallBackForm('60_sec');

        }, 120000);

    </script>
</div>
