<?php // http://leadback.ru/ ?>
<div>
<style>
    .clbh_banner {
        width:800px!important;
        height:390px!important;
        position:relative!important;
        border-radius:16px!important;
        top:100px!important;
        background-color:rgba(255,255,255,.85)!important;
        overflow:hidden!important;
        -webkit-transition:.6s ease-out!important;
        -moz-transition:.6s ease-out!important;
        -o-transition:.6s ease-out!important;
        transition:.6s ease-out!important;
        margin:0 auto!important;
        z-index:200001!important;
    }
    .clbh_banner *{
        font-family:'Open Sans',sans-serif!important;
        color:#333!important;
        font-weight:100!important;
        letter-spacing:normal!important;
    }
    .clbh_banner-arrow {
        width:37px!important;
        height:62px!important;
        position:absolute!important;
        background:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACUAAAA+CAYAAACsj9JbAAAACXBIWXMAAAsTAAALEwEAmpwYAAABNmlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarY6xSsNQFEDPi6LiUCsEcXB4kygotupgxqQtRRCs1SHJ1qShSmkSXl7VfoSjWwcXd7/AyVFwUPwC/0Bx6uAQIYODCJ7p3MPlcsGo2HWnYZRhEGvVbjrS9Xw5+8QMUwDQCbPUbrUOAOIkjvjB5ysC4HnTrjsN/sZ8mCoNTIDtbpSFICpA/0KnGsQYMIN+qkHcAaY6addAPAClXu4vQCnI/Q0oKdfzQXwAZs/1fDDmADPIfQUwdXSpAWpJOlJnvVMtq5ZlSbubBJE8HmU6GmRyPw4TlSaqo6MukP8HwGK+2G46cq1qWXvr/DOu58vc3o8QgFh6LFpBOFTn3yqMnd/n4sZ4GQ5vYXpStN0ruNmAheuirVahvAX34y/Axk/96FpPYgAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAD50lEQVR42szaeYydUxjH8c+9ndZaqmQ0isYS1aDCqKYJEYJYGqQhYpkqaaolFVIR+x+WP9TyRy1tUWJKEFKxBKkSWxrhLYJGpbGVVGWkWtsos/jjPDe5mO3OvHfmfZKb++bc9z33e59zznOe83tuqampSY62FyZgKibjcEzCDjX0MaMhB5AxmIKzMBujov03bMIn+BV/9bO/jYOB2g2zcFF4BN7GO/gMn+NbbOmtkyzL/tc2UKgrMA8HhwduwWt4D+2DdX2tUEfhHhyLNlyNZ7Ahz4lZC9QcLEEJj+HamDO5W3+hFmE+Nsb7CnW0vqC2w3Kcg7dwMb5WZ+sNamQV0FOx0rYZAusN6o4AehLN6DBEVu6h/TJchZURh4YMqCeoJtyLnzAXfxtiK3cznA9Fe/NQTOr+QM3HEXgQrxomq4Yaj2vQiusMo1WvvvMwLry1eTihylU7/lz8jKcNs1WgjsMBuDOGb9ihGnBBpBwvK4CV0RhZ42p8WhSoieGtN9FZFKhpcV0IL1WgjsTv+LJIUJPwA74qEtSE2Hy3FglqpzgEKBKUGg6KQwpVLhpUJ7YvGlQrRhfJW+UIBY2hmBQGah32iNBQGKj3Q745qEhQq+N6SpGgvgnV5HTsXhSoX/A49q3KGAoRPJ+P9wuLBPUxVuFcSZ0rzN63KK6vLwoUvCJpUM04pihQ7bgJXbhP0qeGHYokNy+RJOjbigJVmVPrQlc4syhQW3B2pMeP4LAiQMFanI+xkmg/ucZ+GzEdh+QJJY7wl2DPWJUn1NDvWNwolUXmSdp7LlDwqKR5jsHrksDfny/YIBUAOvEAnq3F2/3JNltwGn7EUjwc+2R3VgoJ4A8sxikBNAPvxuLZOQ+oSmCdhhdjSNdEkB3Vzb0jqtrXYGY8s0mSwVfi+DygSKLsGTGEI8ODb+DSqoNHV7yqFcK2mAYn4674cauwsKcUvDTAyuj+UsGxoo2uD8AnImnsTXefjLtxolSgXIgVWZa1DRaqYvtJ9Zo5sUo7I859EGn2WnwnyZZbw2t/Strq5Vgg6fTLsTiLimQppxpyGSfFPDs6QsIuNe6f62PFtpRyLmxXbCIOjGHeRxJ6d6z6vCO8NjrumRrtrZjZUAegMr6IV1/33RBJ5Wa8EGn5R3lDlWL19Wa74lapQjYuVuLteCnLsm0MvLDdG1Q5hue/cHtLBfHZMZwfRv7WkmXZv1SfegxfQ6zCCtT4CJ6zYv58H/viUnTk+ReAnqwr8v3OyBROlSryh8b5ckGcBdr7+lV5Q3VJtecrI7i24WYskwrj/XJ1PWyEpDbfj+ciw6hp/OthbbE3LhvIw/8MAEZL3pSMpxnpAAAAAElFTkSuQmCC")!important;
        left:-48px!important;
        bottom:7px!important;
        opacity:.75!important;
    }
    .clbh_banner-body {
        width:580px!important;
        height:100%!important;
        position:relative!important;
        top:100px!important;
        margin:0 auto!important;
    }
    .clbh_banner-body .clbh_banner-h1 {
        font-size:24px!important;
        line-height:34px!important;
        text-align:left!important;
        letter-spacing:normal!important;
        height:auto!important;
    }
    .clbh_banner-button {
        width:158px!important;
        height:33px!important;
        font-size:14px!important;
        -webkit-border-radius:4px!important;
        -moz-border-radius:4px!important;
        border-radius:4px!important;
        border:1px solid #333!important;
        color:#fff!important;
        line-height:30px!important;
        float:left!important;
        cursor:pointer!important;
        margin-right:10px!important;
        margin-left:0!important;
        -webkit-transition:.25s ease-out!important;
        -moz-transition:.25s ease-out!important;
        -o-transition:.25s ease-out!important;
        transition:.25s ease-out!important;
        -webkit-box-shadow:none!important;
        -moz-box-shadow:none!important;
        box-shadow:none!important;
        padding:0!important;
        background:#333!important;
        letter-spacing:normal!important;
        text-indent:0;
        position:static;
    }
    .clbh_banner-button .clbh_large {
        width:200px!important;
    }
    .clbh_banner-button:hover {
        background-color:#52aff7!important;
        border-color:#52aff7!important;
    }
    .clbh_banner-exit {
        -webkit-border-radius:2px!important;
        -moz-border-radius:2px!important;
        border-radius:2px!important;
        position:absolute!important;
        right:25px!important;
        top:23px!important;
        height:15px!important;
        width:15px!important;
        background:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA8AAAAPCAYAAAA71pVKAAAACXBIWXMAAAsTAAALEwEAmpwYAAABNmlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarY6xSsNQFEDPi6LiUCsEcXB4kygotupgxqQtRRCs1SHJ1qShSmkSXl7VfoSjWwcXd7/AyVFwUPwC/0Bx6uAQIYODCJ7p3MPlcsGo2HWnYZRhEGvVbjrS9Xw5+8QMUwDQCbPUbrUOAOIkjvjB5ysC4HnTrjsN/sZ8mCoNTIDtbpSFICpA/0KnGsQYMIN+qkHcAaY6addAPAClXu4vQCnI/Q0oKdfzQXwAZs/1fDDmADPIfQUwdXSpAWpJOlJnvVMtq5ZlSbubBJE8HmU6GmRyPw4TlSaqo6MukP8HwGK+2G46cq1qWXvr/DOu58vc3o8QgFh6LFpBOFTn3yqMnd/n4sZ4GQ5vYXpStN0ruNmAheuirVahvAX34y/Axk/96FpPYgAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAAzElEQVR42qyTsRHCMAxFX7xBVoARpFVghKSjhRUyAhmBFVjhMwsDUIgCwwWwA3egOzey35Nt2ZhZa2ariOCbYbdYRAQJOAIHd+/4EO5ueb3cvU3AmOf2c4IJ2AKDpHOSNAIOnLNgWwBXE7CXNAA0EVEyj5L6nO+Affb0uRhPcEkAnGrgG1wQUAMB0mtC0gnYABcggG0J/G/lwpn7uTamGijpXq0qSAVwd29TvoMRWE/ewUPQmFk3147KznaShsbMBNgcWBAALPnlV10HACwiuaGZ5ZweAAAAAElFTkSuQmCC") no-repeat center center transparent!important;
        opacity:.3!important;
        cursor:pointer!important;
        -webkit-transition:.3s ease-out!important;
        -moz-transition:.3s ease-out!important;
        -o-transition:.3s ease-out!important;
        transition:.3s ease-out!important;
    }
    .clbh_banner-exit:hover {
        opacity:.7!important;
    }
    .clbh_banner-pr {
        float:left!important;
        letter-spacing:normal!important;
    }
    .clbh_phone_line {
        position:relative!important;
        z-index:20001!important;
        float:left!important;
    }
    .clbh_banner-form-row-1 {
        position:relative!important;
        top:22px!important;
        height:33px!important;
        float:left!important;
        width:100%!important;
        text-align:left!important;
        letter-spacing:normal!important;
    }
    .clbh_banner-form-row-2 {
        position:relative!important;
        top:56px!important;
        float:left!important;
        height:auto!important;
        text-align:left!important;
        letter-spacing:normal!important;
    }
    .clbh_banner-form-row-2 span {
        font-size:14px!important;
        text-align:left!important;
        letter-spacing:normal!important;
    }
    .clbh_banner-form-row-2.thx span {
        color:#52aff7!important;
        text-align:left!important;
        letter-spacing:normal!important;
    }
    .clbh_banner-textbox {
        min-width:164px!important;
        width:164px!important;
        height:34px!important;
        border:1px solid #c2c2c2!important;
        font-size:14px!important;
        -webkit-border-radius:4px!important;
        -moz-border-radius:4px!important;
        border-radius:4px!important;
        background:0 0!important;
        color:#333!important;
        line-height:30px!important;
        float:left!important;
        display:block!important;
        -webkit-transition:.25s ease-out!important;
        -moz-transition:.25s ease-out!important;
        -o-transition:.25s ease-out!important;
        transition:.25s ease-out!important;
        margin:0 15px 0 0!important;
        padding:0 8px!important;
        text-align:left!important;
        letter-spacing:normal!important;
        left:auto!important;
    }
    .banner-textbox:active, .clbh_banner-textbox:focus {
        border-color:#52aff7!important;
    }
    .clbh_banner-textbox.ierror {
        border-color:#ff0000!important;
    }
    .clbh_banner-work-to {
        bottom:15px!important;
        color:#333!important;
        font-size:11px!important;
        position:absolute!important;
        right:25px!important;
        text-align:left!important;
        letter-spacing:normal!important;
        height:auto!important;
    }
    .clbh_banner-work-to a {
        color:#333!important;
        background:-webkit-gradient(linear,left top,right top,color-stop(0,#4d4d4d),color-stop(0.4,#4d4d4d),color-stop(0.5,#fff),color-stop(0.6,#4d4d4d),color-stop(1,#4d4d4d));
        -webkit-background-clip:text;
        -webkit-text-fill-color:transparent;
        -webkit-animation:slidetounlock 25s infinite;
        text-decoration:none;
        text-align:left!important;
        font-size:11px!important;
        letter-spacing:normal!important;
    }
    @-webkit-keyframes slidetounlock {
        0% {
            background-position:-720px 0;
        }
        100% {
            background-position:720px 0;
        }
    }
    .clbh_banner-work-to a:hover {
        text-decoration:none!important;
    }
    .clbh_banner-wrapper {
        position:fixed!important;
        width:100%!important;
        height:0!important;
        top:0!important;
        z-index:2000000!important;
        overflow:visible!important;
    }
    .clbh_banner_bg {
        display:none;
        position:fixed!important;
        width:100%!important;
        height:100%!important;
        background-color:rgba(0,0,0,.3)!important;
        top:0!important;
        left:0!important;
        z-index:20000!important;
    }
    .clbh_banner.clbh_closed {
        opacity:0!important;
        display:none!important;
    }
    .clbh_banner-line-wrapper.clbh_banner-pr-line {
        visibility:visible!important;
    }
    .clbh_timer {
        position:relative!important;
        left:20px!important;
        text-align:left!important;
        letter-spacing:normal!important;
        float:left!important;
    }
    .clbh_timer p {
        line-height:30px!important;
        font-size:36px!important;
        margin:0!important;
        text-align:left!important;
        letter-spacing:normal!important;
    }

    .clbh_banner-line-wrapper{position:relative!important;width:800px!important;height:20px!important;margin:0 auto!important;border-radius:16px!important;top:-12px!important;
        background-color:rgba(255,255,255,.7)!important;overflow:hidden!important;z-index:200001!important;}
    .clbh_vk{
        background:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAVCAYAAAAuJkyQAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAotJREFUeNqMVjtyAjEMBYeGkooZSi5AsZQpoUy6cITlCHCE5QhwBGjTsUcIV+AK29IRvR2JeQh5E88slrEl6+nr/mw26/X7/R7G/X5/0LbG4H0dU/lvGuxfhG5yvKBJxtOwcwMhCpkX/qCsG9k7OL6R/HeUvYUJAR9fLvRWyB3v04VTBfMABpl052kg848XTqigwI4Ry3yQ/0YyFxFCgAOPt5DynqFEh7WKJD8H5w6mN878sNpJ1jsItO+f4wtujixHMupk5mU0dBnMWXIcqKC6K04CYKBLViYAcpXvlJSoWRmOC5k3HgksRfsvrg6QL/R7AU4DsdckXey8PznwDB1f6hTs9JXwVdEZAoOwOeG/pBuwUB0oY3SlGcaWCy8I5o1m8otFVf5FrdPelQj1OlcjNDVL2h9565jFWIYG8cYrQefh+rXVrtZCFJBX1B3PTHNFSIsOy4wIxNHWnJWk+BYW4jKQXGxsNbWfsoHWZ5nLHGq9uJCv0ppTZCp9T8H7wtt7G4/HvL7pxYtIkKyHMn1wtfXBrWffhZ5EmWddQKZPWd+821NgShS92mdFFNBe4VwMBnwNlw4eKWpyCDTUBC/MF72oPXQVPwKAYN9HgFIGwVWbZLYz+7YRtR7XdH0BRSzuvdxHDAW+xlNiKN+7D3JvsagvBXH1sqcNeiLrb5OXIuSEdos3TpRNf/ChAa8spXPtQvlQ3/ZRHcpdtvRBnmmObMW1vgqWauknl/k6B6Xka0tJ8n4O2geCe2nlPZdxFB+wSqO08V6jqu5irrL3UKffaeCZMrenRy7d9aXJwKDUShMlfGXS2TJFGZJ5rLWBrm6AYlavGn6WwFWBLPDN9Unc+BqlSrbPoF8BBgD7CFpHrvGnMAAAAABJRU5ErkJggg==") no-repeat center center transparent!important;
        height:25px!important;
        width:50px!important;
        display:inline-block;
    }
    .clbh_vk:hover {
        background:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAVCAYAAAAuJkyQAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAvtJREFUeNp0VoGRGjEMlG6+AVogJUAJUEJSAl8ClAAlQAn/M0kBuRJCCaGFm6QBRZZsWZIvzHC2z7ZuJa9WRvj+BwCQ/8RNaUuXtC1j6ed52PJja+O2B/HJcwu0122r/5m52sE498ZGdtw9yExbLEDYMMGj9hu4DXc+hvUNvP4u/L9VgLqmL2Qn+N/ntmLTvgGfb9z5ZYhDhKS/4f5N32Hz6CHvAXaj1zI6cHuL0bWo/GQ7PbJtT//2buLRI3mofRnSWftmYOHnp3ywHWuLAEH/SAfno/eV262upQ6kbVB7MwPCW/A0/kokTs7DtnAODnggnoMQwJ066OZw4NCL931O/OJVkNlmSJ4RR8nAGp+WEY0Hgp6LpX9Q3uWTAM/bS7E7afgaT1ajtLUoQToOHwlK+CgYuipP3L52bPp4KBUIJkWMMw/mIT17lK782OSA9NCv5bhl2FkSYOCoScmzZqa8m/r543sgZDg+LKl5ctHZGCltrQNHLrqI5yHmLUIg0vKurW6anIGX6A6skBNr2MVT6e8G1B3MRj2XhPgQnbGMTOdaeAP09AYQfvz1i0skfptYQQYmZL5IChMdQkhi1t1kvogujdyvNh9yKhljB2QbzsIZL2y4EnJfWkyRnaFBbEPkubzgF6kGsSSFI2ttUdl5SFGCKGZZhzKY/6W4DharecmZKYbTFLQSLWvFilj5eoUYU56SIwZUatp91KOSZWGhCdrLUtETkqr1nE3eU4JYv/J5dbBF2+5ZZybjwSh2TDo+Pq+6vj40R4Z9NMhR0KtYWLmc0N1zbRqIG7lRovQc6lWuVblcFNUF+CZ7aSUZIJQM1je8dx3K5z5m05E3zwNvIHEl1pySzqUUHNn2M1wxcnnSuVMT0Gn4SC4beuVgUHSJ3uG61yRCV2+Nkk1HEd3MudHBa70P+XQe9MIbKAV4rzcDijUJvUP1JtCBLvX4XuGKgsn7ekWJWbYmfNGLp0QLcV8BzvWDjeRLq9qRyLJvX6v6+tVFozj/E2AAyOKHxNSW6f8AAAAASUVORK5CYII=") no-repeat center center transparent!important;
        height:25px!important;
        width:50px!important;
        display:inline-block;
    }
    .clbh_fb {
        height:25px!important;
        width:50px!important;
        background:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAdCAYAAABWk2cPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAixJREFUeNqMV4FtwyAQrBELZAVPEMkruCOkI7QjNCMkI7QjJCt4hHqEeoWM4P5HuDpd7iFIyPDAP39/PLjb7/cvpQzrun52XTfad/cSFBu/f23Og9xlOF7ai9XJ6tFkNxeksubd6o9NOrhBn4xVGeQxNgilt77r/3XHNqPe+PKJ20L2AhWjcWzzOt5AceZidZcd0kiB+rInAk7pPXqdrTFuSpUnkRJcU2Qet8n6M87deAKbHbK72zKm4CPZh9Vv9qz0naAjcGCXIlgYRhXnIju6QZShE6zLvykiS3QUKPaL1bOCHucyF7IiT0QWQY5JQH3wIwgx7Vl/VpBuuyOiKOMLbc4NXCKEtpIUtTEmzGzFZoCw5zGFWIpgRFJw9olIp44dG74TScVLTVSKWblisOJJVgdeeVYWvDp5ouRu1ZNDh5u29sreJxXLCOIoBHxEkLnK08RnkpVGcVa5V4SkVxtLKmbKc8HUh2yDaNTYnFUGUTAX+cmaN+hfKQUOJj/BBnuFUG4ld4JmIPJMlGf98hhb93Dm+NQuYmJlNW3WEkxSh74mU4m/dS1KIrViqhQ+SzpFtlR7dEX3qno9tI4c6k/PPsBq1110MURccKOz8iiCF5WrbITeBmGZ3ei1RgjFSpUgWs8d0D351XbevOUnZ0ScWgxrby4r57vR0vHb48oJIXob1RisUmSR+W/F8f+54v8Y1nkr/zNjC2rY0EzKl6IcN3qz/rQ9bbz+CTAAj4Gefja+rOAAAAAASUVORK5CYII=") no-repeat center center transparent!important;
        display:inline-block;
    }
    .clbh_fb:hover {
        background:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB0AAAAdCAYAAABWk2cPAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAtRJREFUeNp8Votx2zAMBXRawCtoBa2QjNCOkI4Qj9COUI/Q3GUCreAVvEKuXQAFfyDwRFp3NimIBEDg4RFMn/+ISIiYd5286+9FXy9Vlof8cB2lyqlsy/Igq8K2nuih6w59v+r8K6ulz79pw5sKfpsSAgOm3I0ExtExr6g49qWTV53fF53s2aB/xG3Mhv1mMEhgsL1wkzWPNHpMf1TfRY3Ke/DUjHAfvSNBJl2W5xMH+rilqK769xJDxLDRxZT5HDoLZ87bocvu5kR59FBqo2/Z13RcMyaD8OEYHDHA/FDRzfZabLNgL+A0nZc1oM6jVQiBUL4jcimj8tYd9QDyQOvzpefCoTSEGL5Hh7Qc5FfZ9wxYEf1rzJPLFztvMY19fkBNpueb6nszvcxbdFzUqHejL4xI9MDw30gewQtRdJKWhTmKwCvztSeYu2LPNALHE49iIsDERsOnsVRZu5gxy0s9NQKHINw0AQ25CjCi4P6ew4sE4DlXBnmVAf0h9THFkIpzkLnlFOsPSkUMLK8FPFOEJnJgRxjJiARHc3hDGTAQ+4B9GHJvvg5qpOU4gI9qeMPt4e3w+TT0pCblVAHbiWUyOXiX2d8wTi6O3PEqE8xtIJctEL60k2LIEJEWhSz/mS/ivuRD5ze3fq9r2rNFUDX0Du/HCR+zknfIdwVVr9VLv7UG7GZA8kwkQlMC9Rc4oDGylZszYEWsZHighCYAogEJT6IU9LRalQYkIAMBYOFp0InQkE2qINzJ3NA7UCRPZIhWwdBirxUPsoTQMLDTKMW+UUMjNKDHwNUGJO1pvKJTHfr6xJ6X47U3Ssv5PbWgWmszwJjiEVJHZMBjsvBOa5ktud0gvp8WEvKrxOtMZmATSI2vV06tzbHUEKXO+yN2C1hnfA7/iP6YY7Pd79KrDtc0X+sJErV9V4HSWGUULHq8AYviO/S/j6w81rzq5qN8K079F2AA/Vt6NCjUpugAAAAASUVORK5CYII=") no-repeat center center transparent!important;
        display:inline-block;
    }
    .clbh_tw {
        height:25px!important;
        width:50px!important;
        background:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAYCAYAAADtaU2/AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAmZJREFUeNqMVsFxwkAMNA4N+EmeVMCMUwJ88yMlQAlJCaSEuARSQlxCXAJ884u//IiW2SPyRmfimZu7k8+nlbSSPFksFgWe8/lcpGcymQz2Ksfsv7H93Ka1jSVlRxudjUbvs3Vl637KC5Y4aMI+XahKFFy60J5nk+/c3gPb2HrF79a2Britjb50wg9bV5ESrB1ir3ugVIHaujbZweZv277ZaOENnCl5EC6qqbwugkcUQgncu1O5B0qwFb9/hVICrZLFcyKG0g+6JIy1s3zjw6JWB26Hd2D1O2KcLO7c3UC4BwAAiqyn8jrigLjaA+8RbxsXXSUPt4Fr4f4DAawjtyu7RwiI+SkZCNmULxoysFIikfFL7lvb42Owfy7nQiBe5r0xdQLQfK9xlf0VRM7iG4QsBhab4JOMA9Xnty7U1Bo77wzr/LlpcqHGMXdJdGnO5e45+uJ0IRc3Tc6aHGNVPlbpbG71bOksbiIL/NCc/UfxKNz9g6d0h7Z24CW6YCymQcPQuWfRGAAtBQjSBQC6yBoFdSMD0v5VwV7J5RAjVXZj7fEWocQbIFUT5XYpljWppEXEUVdGoMQbCF2v9Rv7UtAiHitWpz91d6xFBi6GEe9RhuC5m81m6r4TLf/i+mTy+4g4udpNjjzm/mIulcvlmlrVsKO8JVC5ZiCp1dm71QjZflktcURbRMNAX97jr0RjpMpcbOGpFZvIgCNKQtTqZyjjT0Caww6Tixd/7l5STL11mgVXi5lnLYlV5y73H7qBdIHCB1UaNY9BW0yFgwOdCblc85d1KQQBaY5kPcB2udo+Vgsg+xFgAK4+TBY2olQOAAAAAElFTkSuQmCC") no-repeat center center transparent!important;
        display:inline-block;
    }
    .clbh_tw:hover {
        background:url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAYCAYAAADtaU2/AAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAvNJREFUeNp0VouR4zAIBU8acAtpIVeCU0KuhE0JmxKSEjYlJDN3BWxKOJewKeE8sw2wIGEEkuIZW7LQBx4PEMLfbwACAOSXpCN91D7qOECZ5PpFtuWfA/9PKn5yf+beFdpnZPmy0T0mbmc+cLHD143XJymjh659Sv13IDwXnbyC8Mb/ezWEFWPlCI4sXgadcGDJJwvHtMgQcAevY4imF3/e+XOOQOjCbMCOB764/c/jHzz8YMFTZIMeMLGAJ8FnmowQ4cdKkdwyvHRugDQXmYKjjl+4zy5AWTMOKtzqLDmULYdDOcjhHRV6s817iKxKFNTYJfDB7V1cOigks5sw8ga3ZL0ohB24s893hQ+1xeSIapKF3306i59BifLIE8gtFsLRl8J/MMjNEkcy8G5ZYda36PY7MV1JudHJV4VuLASxZ8pvYiYriHPWHreBhNhxC3SiQudvXMweub0FdprMFhcl1olUMT2gT/Hg4ibQOMZ//H1yR6i+7fiz0t4pR3UEYGmj2bNz4xpODGGKOSETlliEavNVocYqag+NxBODFm+9hhO2qa22Eqj99y4xH2KrcOZGGB90vwcLr93w8PhShZ7P771sB5ZiHwWcrM3gID3y91SELgG46Ii1wsNKEEPOEork/3uN5BAgFc0QjkaEV0hbRnPw+TQZrb40iBirC2xTTvp1YoCqPL5gekAh9Z+ZPxhZb6y2PRLJSvr0ClixqCwj5xK3sW5wysnGEdHCKSSINZ/SXJShF8nfVS3P2LLXld97KJXukoHw57sKfld9IJVLSY27hq5mdSd+c1r9Ff0d0RhiLiVPAPHNXetutCpwoIpfQHHVPlan6tZiPoaQAsd0ZclV6Zb+/R2MOjFbDBF499ll2M8BqsBGC/SYUma6+sgNBCt2QgsnoU8gwt5TKvK96lQnFoQUTpfsS4EUp8aHvUtc8dUzRYIQiWBpDiR6WRYxXW8LSbb5/qUFI10GggVzPiy1kmbn/g20CiuqMgjLfgQYAFvei8pT9IpsAAAAAElFTkSuQmCC") no-repeat center center transparent!important;
        display:inline-block;
    }

    .clbh_banner_office_wrapper{display:inline!important;position:relative}

    .clbh_banner-choose-office {
        font-size:14px!important;
        letter-spacing:normal!important;
        text-align:left!important;
    }

    #clbh_banner_h1 #clbh_night_hour, #clbh_banner_h1 #clbh_night_minute {
        border:1px solid #c2c2c2!important;
        padding:2px 4px!important;
        background:none!important;
        -webkit-border-radius:4px!important;
        -moz-border-radius:4px!important;
        border-radius:4px!important;
        font-size:16px!important;
        width:60px!important;
        height:32px!important;
        display:inline!important;
    }

    /*************/

    .cbh-phone {
        position:fixed;
        display:none;
        background-color:transparent;
        width:200px;
        height:200px;
        cursor:pointer;
        z-index:200000!important;
        -webkit-backface-visibility:hidden;
        -webkit-transform:translateZ(0);
        -webkit-transition:visibility .5s;
        -moz-transition:visibility .5s;
        -o-transition:visibility .5s;
        transition:visibility .5s;
        bottom:50px;
        left:50px;
    }
    .cbh-phone.cbh-show {
        display:block;
    }
    @-webkit-keyframes fadeInRight {
        0% {
            opacity:0;
            -webkit-transform:translate3d(100%,0,0);
            transform:translate3d(100%,0,0);
        }
        100% {
            opacity:1;
            -webkit-transform:none;
            transform:none;
        }
    }
    @keyframes fadeInRight {
        0% {
            opacity:0;
            -webkit-transform:translate3d(100%,0,0);
            -ms-transform:translate3d(100%,0,0);
            transform:translate3d(100%,0,0);
        }
        100% {
            opacity:1;
            -webkit-transform:none;
            -ms-transform:none;
            transform:none;
        }
    }

    @-webkit-keyframes fadeInRightBig {
        0% {
            opacity:0;
            -webkit-transform:translate3d(2000px,0,0);
            transform:translate3d(2000px,0,0);
        }
        100% {
            opacity:1;
            -webkit-transform:none;
            transform:none;
        }
    }
    @-webkit-keyframes fadeOutRight {
        0% {
            opacity:1;
        }
        100% {
            opacity:0;
            -webkit-transform:translate3d(100%,0,0);
            transform:translate3d(100%,0,0);
        }
    }
    @keyframes fadeOutRight {
        0% {
            opacity:1;
        }
        100% {
            opacity:0;
            -webkit-transform:translate3d(100%,0,0);
            -ms-transform:translate3d(100%,0,0);
            transform:translate3d(100%,0,0);
        }
    }
    .fadeOutRight {
        -webkit-animation-name:fadeOutRight;
        animation-name:fadeOutRight;
    }
    .cbh-phone.cbh-static {
        opacity:.6;
    }
    .clbh_blur {
        -webkit-filter:blur(3px);
        -ms-filter:blur(3px);
        -moz-filter:blur(3px);
        -o-filter:blur(3px);
        filter:blur(3px);
        filter:url("data:image/svg+xml;utf8,%3Csvg%20xmlns%3D%22http%3A//www.w3.org/2000/svg%22%3E%3Cfilter%20id%3D%22blur%22%3E%3CfeGaussianBlur%20stdDeviation%3D%223%22/%3E%3C/filter%3E%3C/svg%3E#blur");
    }

    .cbh-phone.cbh-static:hover, .cbh-phone.cbh-hover {
        opacity:1;
    }

    .cbh-ph-circle {
        width:160px;
        height:160px;
        top:20px;
        left:20px;
        position:absolute;
        background-color:transparent;
        -webkit-border-radius:100%;
        -moz-border-radius:100%;
        border-radius:100%;
        border:2px solid rgba(30,30,30,.4);
        opacity:.1;
        -webkit-animation:cbh-circle-anim 1.2s infinite ease-in-out;
        -moz-animation:cbh-circle-anim 1.2s infinite ease-in-out;
        -ms-animation:cbh-circle-anim 1.2s infinite ease-in-out;
        -o-animation:cbh-circle-anim 1.2s infinite ease-in-out;
        animation:cbh-circle-anim 1.2s infinite ease-in-out;
        -webkit-transition:all .5s;
        -moz-transition:all .5s;
        -o-transition:all .5s;
        transition:all .5s;
        -webkit-transform-origin:50% 50%;
        -moz-transform-origin:50% 50%;
        -ms-transform-origin:50% 50%;
        -o-transform-origin:50% 50%;
        transform-origin:50% 50%;
    }
    .cbh-phone.cbh-active .cbh-ph-circle {
        -webkit-animation:cbh-circle-anim 1.1s infinite ease-in-out!important;
        -moz-animation:cbh-circle-anim 1.1s infinite ease-in-out!important;
        -ms-animation:cbh-circle-anim 1.1s infinite ease-in-out!important;
        -o-animation:cbh-circle-anim 1.1s infinite ease-in-out!important;
        animation:cbh-circle-anim 1.1s infinite ease-in-out!important;
    }
    .cbh-phone.cbh-static .cbh-ph-circle {
        -webkit-animation:cbh-circle-anim 2.2s infinite ease-in-out!important;
        -moz-animation:cbh-circle-anim 2.2s infinite ease-in-out!important;
        -ms-animation:cbh-circle-anim 2.2s infinite ease-in-out!important;
        -o-animation:cbh-circle-anim 2.2s infinite ease-in-out!important;
        animation:cbh-circle-anim 2.2s infinite ease-in-out!important;
    }
    .cbh-phone.cbh-hover .cbh-ph-circle, .cbh-phone.cbh-static:hover .cbh-ph-circle  {
        border-color:rgba(0,175,242,1);
        opacity:.5;
    }
    .cbh-phone.cbh-green.cbh-hover .cbh-ph-circle, .cbh-phone.cbh-green.cbh-static:hover  .cbh-ph-circle {
        border-color:rgba(117,235,80,1);
        opacity:.5;
    }
    .cbh-phone.cbh-green .cbh-ph-circle {
        border-color:rgba(0,175,242,1);
        opacity:.5;
    }
    .cbh-phone.cbh-gray.cbh-hover .cbh-ph-circle, .cbh-phone.cbh-gray.cbh-static:hover .cbh-ph-circle {
        border-color:rgba(204,204,204,1);
        opacity:.5;
    }
    .cbh-phone.cbh-gray .cbh-ph-circle {
        border-color:rgba(117,235,80,1);
        opacity:.5;
    }
    .cbh-ph-circle-fill {
        width:100px;
        height:100px;
        top:50px;
        left:50px;
        position:absolute;
        background-color:#000;
        -webkit-border-radius:100%;
        -moz-border-radius:100%;
        border-radius:100%;
        border:2px solid transparent;
        opacity:.1;
        -webkit-animation:cbh-circle-fill-anim 2.3s infinite ease-in-out;
        -moz-animation:cbh-circle-fill-anim 2.3s infinite ease-in-out;
        -ms-animation:cbh-circle-fill-anim 2.3s infinite ease-in-out;
        -o-animation:cbh-circle-fill-anim 2.3s infinite ease-in-out;
        animation:cbh-circle-fill-anim 2.3s infinite ease-in-out;
        -webkit-transition:all .5s;
        -moz-transition:all .5s;
        -o-transition:all .5s;
        transition:all .5s;
        -webkit-transform-origin:50% 50%;
        -moz-transform-origin:50% 50%;
        -ms-transform-origin:50% 50%;
        -o-transform-origin:50% 50%;
        transform-origin:50% 50%;
    }
    .cbh-phone.cbh-active .cbh-ph-circle-fill {
        -webkit-animation:cbh-circle-fill-anim 1.7s infinite ease-in-out!important;
        -moz-animation:cbh-circle-fill-anim 1.7s infinite ease-in-out!important;
        -ms-animation:cbh-circle-fill-anim 1.7s infinite ease-in-out!important;
        -o-animation:cbh-circle-fill-anim 1.7s infinite ease-in-out!important;
        animation:cbh-circle-fill-anim 1.7s infinite ease-in-out!important;
    }
    .cbh-phone.cbh-static .cbh-ph-circle-fill {
        -webkit-animation:cbh-circle-fill-anim 2.3s infinite ease-in-out!important;
        -moz-animation:cbh-circle-fill-anim 2.3s infinite ease-in-out!important;
        -ms-animation:cbh-circle-fill-anim 2.3s infinite ease-in-out!important;
        -o-animation:cbh-circle-fill-anim 2.3s infinite ease-in-out!important;
        animation:cbh-circle-fill-anim 2.3s infinite ease-in-out!important;
        opacity:0!important
    }
    .cbh-phone.cbh-hover .cbh-ph-circle-fill, .cbh-phone.cbh-static:hover .cbh-ph-circle-fill {
        background-color:rgba(0,175,242,.5);
        opacity:.75!important;
    }
    .cbh-phone.cbh-green.cbh-hover .cbh-ph-circle-fill, .cbh-phone.cbh-green.cbh-static:hover .cbh-ph-circle-fill {
        background-color:rgba(117,235,80,.5);
        opacity:.75!important;
    }
    .cbh-phone.cbh-green .cbh-ph-circle-fill {
        background-color:rgba(0,175,242,.5);
        opacity:.75!important;
    }
    .cbh-phone.cbh-gray.cbh-hover .cbh-ph-circle-fill, .cbh-phone.cbh-gray.cbh-static:hover .cbh-ph-circle-fill {
        background-color:rgba(204,204,204,.5);
        opacity:.75!important;
    }
    .cbh-phone.cbh-gray .cbh-ph-circle-fill {
        background-color:rgba(117,235,80,.5);
        opacity:.75!important;
    }
    .cbh-ph-img-circle {
        width:60px;
        height:60px;
        top:70px;
        left:70px;
        position:absolute;
        background-image:url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAACXBIWXMAAAsTAAALEwEAmpwYAAABNmlDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjarY6xSsNQFEDPi6LiUCsEcXB4kygotupgxqQtRRCs1SHJ1qShSmkSXl7VfoSjWwcXd7/AyVFwUPwC/0Bx6uAQIYODCJ7p3MPlcsGo2HWnYZRhEGvVbjrS9Xw5+8QMUwDQCbPUbrUOAOIkjvjB5ysC4HnTrjsN/sZ8mCoNTIDtbpSFICpA/0KnGsQYMIN+qkHcAaY6addAPAClXu4vQCnI/Q0oKdfzQXwAZs/1fDDmADPIfQUwdXSpAWpJOlJnvVMtq5ZlSbubBJE8HmU6GmRyPw4TlSaqo6MukP8HwGK+2G46cq1qWXvr/DOu58vc3o8QgFh6LFpBOFTn3yqMnd/n4sZ4GQ5vYXpStN0ruNmAheuirVahvAX34y/Axk/96FpPYgAAACBjSFJNAAB6JQAAgIMAAPn/AACA6AAAUggAARVYAAA6lwAAF2/XWh+QAAAB/ElEQVR42uya7W3CMBCG31QM4A1aNggTlG6QbpBMkHYC1AloJ4BOABuEDcgGtBOETnD9c1ERCH/lwxeaV8oPFGP86Hy+DxMREW5Bd7gRjSDSNGn4/RiAOvm8C0ZCRD5PSkQVXSr1nK/xE3mcWimA1ZV3JYBZCIO4giQANoYxMwYS6+xKY4lT5dJPreWZY+uspqSCKPYN27GJVBDXheVSQe494ksiEWTuMXcu1dld9SARxDX1OAJ4lgjy4zDnFsC076A4adEiRwAZg4hOUSpNoCsBPDGM+HqkNGynYBCuILuWj+dgWysGsNe8nwL4GsrW0m2fxZBq9rW0rNcX5MOQ9eZD8JFahcG5g/iKT671alGAYQggpYWvpEPYWrU/HDTOfeRIX0q2SL3QN4tGhZJukVobQyXYWw7WtLDKDIuM+ZSzscyCE9PCy5IttCvnZNaeiGLNHKuz8ZVh/MXTVu/1xQKmIqLEAuJ0fNo3iG5B51oSkeKnsBi/4bG9gYB/lCytU5G9DryFW+3Gm+JLwU7ehbJrwTjq4DJU8bHcVbEV9dXXqqP6uqO5e2/QZRYJpqu2IUAA4B3tXvx8hgKp05QZW6dJqrLTNkB6vrRURLRwPHqtYgkC3cLWQAcDQGGKH13FER/NATzi786+BPDNjm1dMkfjn2pGkBHkf4D8DgBJDuDHx9BN+gAAAABJRU5ErkJggg==);
        background-color:rgba(30,30,30,.1);
        background-position:center center;
        background-repeat:no-repeat;
        -webkit-border-radius:100%;
        -moz-border-radius:100%;
        border-radius:100%;
        border:2px solid transparent;
        opacity:.7;
        -webkit-animation:cbh-circle-img-anim 1s infinite ease-in-out;
        -moz-animation:cbh-circle-img-anim 1s infinite ease-in-out;
        -ms-animation:cbh-circle-img-anim 1s infinite ease-in-out;
        -o-animation:cbh-circle-img-anim 1s infinite ease-in-out;
        animation:cbh-circle-img-anim 1s infinite ease-in-out;
        -webkit-transform-origin:50% 50%;
        -moz-transform-origin:50% 50%;
        -ms-transform-origin:50% 50%;
        -o-transform-origin:50% 50%;
        transform-origin:50% 50%;
    }
    .cbh-phone.cbh-active .cbh-ph-img-circle, .cbh-phone.cbh-static:hover .cbh-ph-img-circle {
        -webkit-animation:cbh-circle-img-anim 1s infinite ease-in-out!important;
        -moz-animation:cbh-circle-img-anim 1s infinite ease-in-out!important;
        -ms-animation:cbh-circle-img-anim 1s infinite ease-in-out!important;
        -o-animation:cbh-circle-img-anim 1s infinite ease-in-out!important;
        animation:cbh-circle-img-anim 1s infinite ease-in-out!important;
    }
    .cbh-phone.cbh-static .cbh-ph-img-circle {
        -webkit-animation:cbh-circle-img-anim 0s infinite ease-in-out!important;
        -moz-animation:cbh-circle-img-anim 0s infinite ease-in-out!important;
        -ms-animation:cbh-circle-img-anim 0s infinite ease-in-out!important;
        -o-animation:cbh-circle-img-anim 0s infinite ease-in-out!important;
        animation:cbh-circle-img-anim 0s infinite ease-in-out!important;
    }
    .cbh-phone.cbh-hover .cbh-ph-img-circle, .cbh-phone.cbh-static:hover .cbh-ph-img-circle {
        background-color:rgba(0,175,242,1);
    }
    .cbh-phone.cbh-green.cbh-hover .cbh-ph-img-circle, .cbh-phone.cbh-green.cbh-static:hover .cbh-ph-img-circle {
        background-color:rgba(117,235,80,1);
    }
    .cbh-phone.cbh-green .cbh-ph-img-circle {
        background-color:rgba(0,175,242,1);
    }
    .cbh-phone.cbh-gray.cbh-hover .cbh-ph-img-circle, .cbh-phone.cbh-gray.cbh-static:hover .cbh-ph-img-circle {
        background-color:rgba(204,204,204,1);
    }
    .cbh-phone.cbh-gray .cbh-ph-img-circle {
        background-color:rgba(117,235,80,1);
    }
    @-moz-keyframes cbh-circle-anim {
        0% {
            -moz-transform:rotate(0deg) scale(0.5) skew(1deg);
            opacity:.1;
            -moz-opacity:.1;
            -webkit-opacity:.1;
            -o-opacity:.1;
        }
        30% {
            -moz-transform:rotate(0deg) scale(.7) skew(1deg);
            opacity:.5;
            -moz-opacity:.5;
            -webkit-opacity:.5;
            -o-opacity:.5;
        }
        100%{
            -moz-transform:rotate(0deg) scale(1) skew(1deg);
            opacity:.6;
            -moz-opacity:.6;
            -webkit-opacity:.6;
            -o-opacity:.1;
        }
    }
    @-webkit-keyframes cbh-circle-anim {
        0% {
            -webkit-transform:rotate(0deg) scale(0.5) skew(1deg);
            -webkit-opacity:.1;
        }
        30% {
            -webkit-transform:rotate(0deg) scale(.7) skew(1deg);
            -webkit-opacity:.5;
        }
        100% {
            -webkit-transform:rotate(0deg) scale(1) skew(1deg);
            -webkit-opacity:.1;
        }
    }
    @-o-keyframes cbh-circle-anim {
        0% {
            -o-transform:rotate(0deg) scale(0.5) skew(1deg);
            -o-opacity:.1;
        }
        30% {
            -o-transform:rotate(0deg) scale(.7) skew(1deg);
            -o-opacity:.5;
        }
        100% {
            -o-transform:rotate(0deg) scale(1) skew(1deg);
            -o-opacity:.1;
        }
    }
    @keyframes cbh-circle-anim {
        0% {
            transform:rotate(0deg) scale(0.5) skew(1deg);
            opacity:.1;
        }
        30% {
            transform:rotate(0deg) scale(.7) skew(1deg);
            opacity:.5;
        }
        100% {
            transform:rotate(0deg) scale(1) skew(1deg);
            opacity:.1;
        }
    }
    @-moz-keyframes cbh-circle-fill-anim {
        0% {
            -moz-transform:rotate(0deg) scale(0.7) skew(1deg);
            opacity:.2;
        }
        50% {
            -moz-transform:rotate(0deg) -moz-scale(1) skew(1deg);
            opacity:.2;
        }
        100% {
            -moz-transform:rotate(0deg) scale(0.7) skew(1deg);
            opacity:.2;
        }
    }
    @-webkit-keyframes cbh-circle-fill-anim {
        0% {
            -webkit-transform:rotate(0deg) scale(0.7) skew(1deg);
            opacity:.2;
        }
        50% {
            -webkit-transform:rotate(0deg) scale(1) skew(1deg);
            opacity:.2;
        }
        100% {
            -webkit-transform:rotate(0deg) scale(0.7) skew(1deg);
            opacity:.2;
        }
    }
    @-o-keyframes cbh-circle-fill-anim {
        0% {
            -o-transform:rotate(0deg) scale(0.7) skew(1deg);
            opacity:.2;
        }
        50% {
            -o-transform:rotate(0deg) scale(1) skew(1deg);
            opacity:.2;
        }
        100% {
            -o-transform:rotate(0deg) scale(0.7) skew(1deg);
            opacity:.2;
        }
    }
    @keyframes cbh-circle-fill-anim {
        0% {
            transform:rotate(0deg) scale(0.7) skew(1deg);
            opacity:.2;
        }
        50% {
            transform:rotate(0deg) scale(1) skew(1deg);
            opacity:.2;
        }
        100% {
            transform:rotate(0deg) scale(0.7) skew(1deg);
            opacity:.2;
        }
    }
    @keyframes cbh-circle-img-anim {
        0% {
            transform:rotate(0deg) scale(1) skew(1deg);
        }
        10% {
            transform:rotate(-25deg) scale(1) skew(1deg);
        }
        20% {
            transform:rotate(25deg) scale(1) skew(1deg);
        }
        30% {
            transform:rotate(-25deg) scale(1) skew(1deg);
        }
        40% {
            transform:rotate(25deg) scale(1) skew(1deg);
        }
        50% {
            transform:rotate(0deg) scale(1) skew(1deg);
        }
        100% {
            transform:rotate(0deg) scale(1) skew(1deg);
        }
    }
    @-moz-keyframes cbh-circle-img-anim {
        0% {
            -moz-transform:rotate(0deg) scale(1) skew(1deg);
        }
        10% {
            -moz-transform:rotate(-25deg) scale(1) skew(1deg);
        }
        20% {
            -moz-transform:rotate(25deg) scale(1) skew(1deg);
        }
        30% {
            -moz-transform:rotate(-25deg) scale(1) skew(1deg);
        }
        40% {
            -moz-transform:rotate(25deg) scale(1) skew(1deg);
        }
        50% {
            -moz-transform:rotate(0deg) scale(1) skew(1deg);
        }
        100% {
            -moz-transform:rotate(0deg) scale(1) skew(1deg);
        }
    }
    @-webkit-keyframes cbh-circle-img-anim {
        0% {
            -webkit-transform:rotate(0deg) scale(1) skew(1deg);
        }
        10% {
            -webkit-transform:rotate(-25deg) scale(1) skew(1deg);
        }
        20% {
            -webkit-transform:rotate(25deg) scale(1) skew(1deg);
        }
        30% {
            -webkit-transform:rotate(-25deg) scale(1) skew(1deg);
        }
        40% {
            -webkit-transform:rotate(25deg) scale(1) skew(1deg);
        }
        50% {
            -webkit-transform:rotate(0deg) scale(1) skew(1deg);
        }
        100% {
            -webkit-transform:rotate(0deg) scale(1) skew(1deg);
        }
    }
    @-o-keyframes cbh-circle-img-anim {
        0% {
            -o-transform:rotate(0deg) scale(1) skew(1deg);
        }
        10% {
            -o-transform:rotate(-25deg) scale(1) skew(1deg);
        }
        20% {
            -o-transform:rotate(25deg) scale(1) skew(1deg);
        }
        30% {
            -o-transform:rotate(-25deg) scale(1) skew(1deg);
        }
        40% {
            -o-transform:rotate(25deg) scale(1) skew(1deg);
        }
        50% {
            -o-transform:rotate(0deg) scale(1) skew(1deg);
        }
        100%{
            -o-transform:rotate(0deg) scale(1) skew(1deg);
        }
    }

    /*success*/
    .ch-status.none, .none {
        display:none;
    }



    @media screen and (max-width: 579px) {

        .clbh_banner-body .clbh_banner-h1 {
            font-size: 15px!important;
            line-height: 20px!important;
        }

        .clbh_banner-wrapper .clbh_banner {
            height: 280px!important;
            top: 10px !important;
        }

        .cbh-phone {
            width:100px;
            height:100px;

            bottom:70px;
            left:50%;
        }

        .cbh-ph-circle {
            display:none;
        }
</style>
<div id="clbh_phone_div" class="cbh-phone cbh-green cbh-static cbh-show" data-toggle="modal" data-target="#orderBook">
    <div class="cbh-ph-circle"></div>	<div class="cbh-ph-circle-fill"></div>
    <div class="cbh-ph-img-circle"></div></div>
<?php
$callback_modal = new w_callback();
$callback_modal->tpl = "default";
echo $callback_modal->getHtml();

?>
</div>
