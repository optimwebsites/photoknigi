<?php
/**
 * @var w_callback $this
 */
?><div class="container">
    <div class="row order-form" id="order-form">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 order-form-bg">
            <div class="h1"><?=$this->form_title?></div>
            <div class="h4">Оставьте заявку, мы свяжемся с Вами в течение 15 минут</div>

            <form class="form-horizontal" method="post" id="saveCallbackForm" action="/_j/callback/save/s/">
                <input type="hidden" class="form-control" name="m_callback[msg]"/>
                <div class="form-group">
                    <label for="inputPhone" class="col-sm-4 control-label">Телефон</label>
                    <div class="col-sm-6">
                        <input type="text" class="form-control" name="m_callback[phone]" placeholder="" id="inputPhone">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">E-mail</label>
                    <div class="col-sm-6">
                        <input type="email" class="form-control" id="inputEmail3" placeholder="" name="m_callback[email]">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="alert hide" id="formMsg_saveCallbackForm"></div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-4 control-label">&nbsp;</label>
                    <div class="col-sm-6">
                        <button class="btn btn-lg btn-danger form-submit" data-form="saveCallbackForm">Отправить заявку</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
