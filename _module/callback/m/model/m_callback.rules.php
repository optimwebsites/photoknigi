<?php

return array(
	
	//проверка int, обязательных для заполнения
	//array(
	//	'',
	//	'numerical',
	//	'allowEmpty' => false,
	//),
	//проверка int, НЕобязательных для заполнения
	//array(
	//	'',
	//	'numerical',
	//	'allowEmpty' => true,
	//),
	//удаление тегов
	array(
		'phone',
		'required',
	),
	array(
		'name, email, phone, msg',
		'filter',
		'filter' => 'strip_tags',
	),
	//проверка date, обязательных для заполнения
	//array(
	//	'',
	//	'date',
	//	'format' => 'yyyy-MM-dd',
	//	'allowEmpty' => false,
	//),
	//проверка date, НЕобязательных для заполнения
	//array(
	//	'',
	//	'date',
	//	'format' => 'yyyy-MM-dd',
	//	'allowEmpty' => true,
	//),
	//проверка reg_exp = email
	array(
		'email',
		'k_validator_rule',
		'rule' => 'email',
		'allowEmpty' => true,
	),
	//безопасные аттрибуты при insert. УДАЛИТЬ ОТСЮДА ТО, ЧТО НЕ ДОЛЖНО ПРИХОДИТЬ С ФОРМЫ В АДМИНКЕ ПРИ СОЗДАНИИ ЗАПИСИ
	array(
		'name, email, phone, msg',
		'safe',
		'on' => 'insert',
	),
	//безопасные аттрибуты при update. УДАЛИТЬ ОТСЮДА ТО, ЧТО НЕ ДОЛЖНО ПРИХОДИТЬ С ФОРМЫ В АДМИНКЕ ПРИ СОЗДАНИИ ЗАПИСИ
	array(
		'name, email, phone, msg',
		'safe',
		'on' => 'update',
	),	
);