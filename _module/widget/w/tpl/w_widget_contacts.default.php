<div class="contacts pull-right">
	<?php if (!empty($data['widget']->phone1)) {
		
		$phone = preg_replace("/[^\d]/", "", $data['widget']->phone1);
		
		echo '<div class="contacts-phone">
				<a href="tel:'.$data['widget']->phone1.'" style="text-decoration:none">
					<span class="text-warning">+'.$phone[0].' '.$phone[1].$phone[2].$phone[3].'  '.$phone[4].$phone[5].$phone[6].'</span>
					<span class="text-phone"> '.$phone[7].$phone[8].' '.$phone[9].$phone[10].'</span>
				</a>
			</div>';
	
	}

	if (!empty($data['widget']->email)) {

		$email_parts = explode("@", $data['widget']->email);

		echo '<div class="contacts-email">
				<a href="mailto:'.$data['widget']->email.'"><span class="text-warning">'.$email_parts[0].'@</span><span class="text-email">'.$email_parts[1].'</span></a>
			</div>';

	}
	
	?>
	<div class="contacts-info">
		<span class="text-warning">Без выходных!</span>
	</div>
</div>