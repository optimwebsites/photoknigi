<?php

return [
    'default' => [
        'm_callback' => [
            'value'    => [
                'onCallbackCreate' => [
                    'k_callback.sendAdminEmail',
                    'k_callback.sendAdminSms',
                ],
            ],
            'title'    => 'События при соханении формы обратной связи',
            'editable' => true,
        ],
        'm_order' =>
            array(
                'value' =>
                    array(
                        'onOrderPay' => null,
                            //array(
                            //    0 => 'k_order.turnOnService',
                            //    1 => 'k_order.sendEmail',
                            //    2 => 'k_order.sendAdminEmail',
                            //),
                    ),
                'title' => 'События при заказе в магазине (onOrderCreate, onOrderPay)',
                'editable' => true,
            ),
        'm_bill' =>
            array(
                'value' =>
                    array(
                        'onBillDelete' =>
                            array(
                                0 => 'k_order.billDeleted',
                            ),
                        'onBillPay' =>
                            array(
                                0 => 'k_order.billPayOk',
                            ),
                    ),
                'title' => 'События при работе со счетом (onBillDelete, onBillPay)',
                'editable' => true,
            ),
        'm_payment' =>
            array(
                'value' =>
                    array(
                        'onPayOk' =>
                            array(
                                0 => 'k_bill.payOk',
                            ),
                    ),
                'title' => 'События при обрработке платежа (onPayOk)',
                'editable' => true,
            ),
    ],
];
