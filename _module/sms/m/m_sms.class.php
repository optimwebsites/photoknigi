<?php

/**
 * модель m_sms
 *
 */
class m_sms extends m_sms_base
{
    /**
     * @var m_callback
     */
    private $callback;

    private $template = 'm_callback.default';

    /**
     * Returns the static model of the specified AR class.
     *
     * @param string $className
     *
     * @return m_sms the static model class
     */
    public static function model($className = __CLASS__)
    {
        return parent::model($className);
    }

    public function readyForSending()
    {
        return ($this->datetime_send_after === null || strtotime($this->datetime_send_after) >= time())
            && $this->is_ready === 1
            && $this->is_send === 0;
    }

    public function setSuccessfulSend()
    {
        $this->is_send = 1;
        $this->datetime_send = date('Y-m-d H:i:s');
    }

    public function setErrorSend($text)
    {
        $this->is_send    = 1;
        $this->is_ready   = 0;
        $this->error_text = $text;
    }

    /**
     * @param m_callback $callback
     */
    public function setCallback(m_callback $callback)
    {
        $this->callback = $callback;
    }

    protected function beforeSave()
    {
        if (isset($this->callback)) {
            if ($this->callback->msg === 'конструктор, подождать 5 мин') {
                return false;
            }
            $this->text = Template::_transform('m_sms', $this->template, $this->callback);
        }

        if (empty($this->text)) {
            $this->addError('m_sms', 'Empty sms body.');
            return false;
        }

        $this->prepareText();

        return true;
    }

    protected function afterSave()
    {
        if ($this->readyForSending()) {
            $this->send();
        }

        parent::afterSave();
    }

    private function prepareText()
    {
        $this->text = Utils::_latFromCyr($this->text);
    }

    private function send()
    {
        $result = k_sms::send($this->text, $this->phone);

        if ($result['ok']) {
            $this->setSuccessfulSend();
        } else {
            $this->setErrorSend($result['error']);
        }

        $this->scenario = 'update';
        $this->isNewRecord = false;
        $this->save();
    }
}
