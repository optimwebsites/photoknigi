<?php

/**
*   Базовый класс-модель m_photobook_client_base
*	Рализует основное поведение модели, описанное в файле /_module/photobook/m/m_photobook_client_base.class.php
*	НЕ ИЗМЕНЯТЬ ГЕНЕРИРУЕТСЯ АВТОМАТИЧЕСКИ ПРИ ИЗМЕНЕНИИ ФАЙЛА /_module/photobook/m/model/m_photobook_client.model.php
*/

/**
 * @property int $id_photobook_client
 * @property int $id_client
 * @property string $login
 * @property string $user_hash
 * @property int $user_id
 * @property m_client $client
 */
abstract class m_photobook_client_base extends BaseModel {

	
	/**
	 * @return string
	 */
	public function tableName()
	{
		return 'photobook_client';
	}

	/**
	 * @return string
	 */
	public function primaryKey()
	{
		return 'id_photobook_client';
	}

	/**
	 * Returns the attribute labels.
	 * Attribute labels are mainly used in error messages of validation.
	 * By default an attribute label is generated using {@link generateAttributeLabel}.
	 * This method allows you to explicitly specify attribute labels.
	 *
	 * Note, in order to inherit labels defined in the parent class, a child class needs to
	 * merge the parent labels with child labels using functions like array_merge().
	 *
	 * @return array attribute labels (name=>label)
	 * @see generateAttributeLabel
	 */
	public function attributeLabels()
	{
		return array("id_photobook_client"=>"ID",
			"id_client"=>"Клиент",
			"login"=>"Логин для ЛК",
			"user_hash"=>"User Hash",
			"user_id"=>"ID пользователя");
	}

    /**
     * Зависимости для модели
     * @return array
     */
    public function relations()
    {
        return [
                "client" => [self::BELONGS_TO, "m_client", "id_client"],
            ];
    }

	/**
	*  перед удалением удаляем данные во всех зависимых таблицах
	*  если не запрещено
	*/
	public function beforeDelete()
	{

		$restrict = array();
		foreach ($this->relations() as $rel => $r)
		{
			if ($r[0] === self::HAS_MANY)
			{
                /** @var CActiveRecord $d */
				foreach ($this->$rel as $d) {
					$d->delete();
                }
			}
		}

		return true;
	}

	/*
	*   Правила валидации для модели подключаются через внешний файл
	*	файл /_module/photobook/m/model/m_photobook_client.rules.php	*/
	public function rules()
	{
		$rules = Modules::_find('m_photobook_client','model','rules');
		if ($rules)
			return include($rules);
		else
			return array();
	}




}
