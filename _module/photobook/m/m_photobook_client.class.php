<?php
/**
* модель m_photobook_client
*
*/

class m_photobook_client extends m_photobook_client_base
{
	const USER_ID   = "2262";
	const USER_HASH = "1f4a370c8171d4fc6444ac895c866354";

	/**
	 * Returns the static model of the specified AR class.
     * @param string $className
	 * @return m_photobook_client the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * регистрация клиента в фотисе
	 *
	 */
	public function createFromOrder(m_order $m_order)
	{
		$this->id_client = $m_order->id_client;
		$this->login = $m_order->client->client->email;
		$this->registration();
	}

	public function registration()
	{
		$this->user_id = self::USER_ID;
		$this->user_hash = self::USER_HASH;
		$this->save();
	}
}
