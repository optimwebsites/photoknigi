<?php
/**
 * Created by PhpStorm.
 * User: nikolay
 * Date: 06.01.16
 * Time: 16:10
 */

class m_photobook {

    const TYPE_PHOTOBOOK_PREMIUM = 'premium';
    const TYPE_PHOTOBOOK_STANDART = 'standart';
    const TYPE_VIPUSK_PREMIUM = 'vprimium';
    const TYPE_VIPUSK_STANDART = 'vstandart';
    const TYPE_VIPUSK_BUTTERFLY = 'vbutterfly';

    const COVER_HARD = 'cover_hard';
    const COVER_SOFT = 'cover_soft';
    const COVER_TKAN = 'cover_tkan';
    const COVER_LEATHER = 'cover_leather';
    const COVER_VINIL = 'cover_vinil';
    const COVER_LEATHER_TKAN = 'cover_leather_tkan';

    const PROFIT_STANDART = 1.4;

    const EDITOR_KOEFF = 1.25;

    /**
     * Цена за одну страницу
     */
    const PRICE_DESIGN = 180;

    /**
     * стоимость отделки уголками
     *
     * @var int
     */
    private $_corner_price = 200;

    /**
     * стоимость фотовставки
     *
     * @var int
     */
    private $_photo_price = 500;

    /**
     * стоимость отделки срезов
     *
     * @var int
     */
    private $_cut_price = 600;

    /**
     * Стоимость форзаца
     *
     * @var int
     */
    private $_forzac_price = 400;


    /*
     * 20х20	406х203	450х240	1410	115
	21х30	430х305	478х330	1720	145
	25х20	510х203	550х240	1510	125
	25х25	508х254	560х300	1920	160
	30х21	610х203	670х250	1810	150
	30х25	590х254	670х300	2015	170
	30х30	610х305	670х330	2620	220
	35х30	720х305	780х330	2820	235
	40х30	810х305	870х330	3225	270

	20х20	200х200	450х240	930	31
	21х30	210х297	478х330	975	32
	25х25	250х250	560х300	1280	43
	30х21	297х210	670х250	975	32
	30х30	300х300	670х330	1475	50
	30х40	300х400	670х430	1935	56
	40х30	400х300	870х330	1935	56
	*/
    private $_format = [
        self::TYPE_PHOTOBOOK_PREMIUM => [
            '20x20', '21x30', '25x20', '25x25', '30x21', '30x25', '30x30', '35x30', '40x30'
        ],
        self::TYPE_PHOTOBOOK_STANDART => [
            '20x20', '21x30', '25x25', '30x21', '30x30', '30x40', '40x30'
        ],
        self::TYPE_VIPUSK_BUTTERFLY => [
            '21x30', '23x23'
        ],
        self::TYPE_VIPUSK_PREMIUM => [
            '15x21', '18x25', '20x20', '25x25', '21x30', '30x21', '30x30',
        ],
        self::TYPE_VIPUSK_STANDART => [
            '15x21', '21x30', '25x25',  '30x21', '30x30',
        ]
    ];

    private $_params = [
        self::TYPE_PHOTOBOOK_PREMIUM => [
            '20x20' => [
                'width' => 406,
                'height' => 203,
                'src_price'  =>  91,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //21х30	430х305	478х330	1720	145
            '21x30' => [
                'width' => 430,
                'height' => 305,
                'src_price' => 111,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //25х20	510х203	550х240	1510	125
            '25x20' => [
                'width' => 510,
                'height' => 203,
                'src_price' => 98,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //25х25	508х254	560х300	1920	160
            '25x25' => [
                'width' => 508,
                'height' => 254,
                'src_price' => 124,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //30х21	610х203	670х250	1810	150
            '30x21' => [
                'width' => 610,
                'height' => 203,
                'src_price' => 117,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //30х25	590х254	670х300	2015	170
            '30x25' => [
                'width' => 590,
                'height' => 254,
                'src_price' => 130,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //30х30	610х305	670х330	2620	220
            '30x30' => [
                'width' => 610,
                'height' => 305,
                'src_price' => 169,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //35х30	720х305	780х330	2820	235
            '35x30' => [
                'width' => 720,
                'height' => 305,
                'src_price' => 182,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //40х30	810х305	870х330	3225	270
            '40x30' => [
                'width' => 810,
                'height' => 305,
                'src_price' => 208,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
        ],
        self::TYPE_PHOTOBOOK_STANDART => [
            //20х20	200х200	450х240	930	31
            '20x20' => [
                'width' => 200,
                'height' => 200,
                'src_price' => 24,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //21х30	210х297	478х330	975	32
            '21x30' => [
                'width' => 210,
                'height' => 297,
                'src_price' => 25,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //25х25	250х250	560х300	1280	43
            '25x25' => [
                'width' => 250,
                'height' => 250,
                'src_price' => 33,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //30х21	297х210	670х250	975	32
            '30x21' => [
                'width' => 297,
                'height' => 210,
                'src_price' => 25,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //30х30	300х300	670х330	1475	50
            '30x30' => [
                'width' => 300,
                'height' => 300,
                'src_price' => 38,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //30х40	300х400	670х430	1935	56
            '30x40' => [
                'width' => 300,
                'height' => 400,
                'src_price' => 43,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //40х30	400х300	870х330	1935	56
            '40x30' => [
                'width' => 400,
                'height' => 300,
                'src_price' => 43,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
        ],
        self::TYPE_VIPUSK_PREMIUM => [
            '15x21' => [
                'width' => null,
                'height' => null,
                'src_price' => 55,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            '18x25' => [
                'width' => 200,
                'height' => 200,
                'src_price' => 75,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //20х20	200х200	450х240	930	31
            '20x20' => [
                'width' => 200,
                'height' => 200,
                'src_price' => 65,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //21х30	210х297	478х330	975	32
            '21x30' => [
                'width' => 210,
                'height' => 297,
                'src_price' => 95,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //25х25	250х250	560х300	1280	43
            '25x25' => [
                'width' => 250,
                'height' => 250,
                'src_price' => 115,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //30х21	297х210	670х250	975	32
            '30x21' => [
                'width' => 297,
                'height' => 210,
                'src_price' => 95,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //30х30	300х300	670х330	1475	50
            '30x30' => [
                'width' => 300,
                'height' => 300,
                'src_price' => 145,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
        ],
        self::TYPE_VIPUSK_BUTTERFLY => [
            '21x30' => [
                'width' => null,
                'height' => null,
                'src_price' => 45,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            '23x23' => [
                'width' => null,
                'height' => null,
                'src_price' => 50,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
        ],
        self::TYPE_VIPUSK_STANDART => [
            '15x21' => [
                'width' => 150,
                'height' => 210,
                'src_price' => 18,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //21х30	210х297	478х330	975	32
            '21x30' => [
                'width' => 210,
                'height' => 297,
                'src_price' => 30,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //25х25	250х250	560х300	1280	43
            '25x25' => [
                'width' => 250,
                'height' => 250,
                'src_price' => 47,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //30х21	297х210	670х250	975	32
            '30x21' => [
                'width' => 297,
                'height' => 210,
                'src_price' => 30,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
            //30х30	300х300	670х330	1475	50
            '30x30' => [
                'width' => 300,
                'height' => 300,
                'src_price' => 50,
                'discount' => 0,
                'is_hit' => 0,
                'profit' => self::PROFIT_STANDART,
            ],
        ],
    ];

    private $_cover_size = [
        self::COVER_HARD => [
            self::TYPE_PHOTOBOOK_PREMIUM => [
                '20x20' => ['width' => 450, 'height' => 240],
                '21x30' => ['width' => 478, 'height' => 330],
                '25x20' => ['width' => 550, 'height' => 240],
                '25x25' => ['width' => 560, 'height' => 300],
                '30x21' => ['width' => 670, 'height' => 250],
                '30x25' => ['width' => 670, 'height' => 300],
                '30x30' => ['width' => 670, 'height' => 330],
                '35x30' => ['width' => 780, 'height' => 330],
                '40x30' => ['width' => 870, 'height' => 330],
            ],
            self::TYPE_PHOTOBOOK_STANDART => [
                '20x20' => ['width' => 450, 'height' => 240],
                '21x30' => ['width' => 478, 'height' => 330],
                '25x25' => ['width' => 560, 'height' => 300],
                '30x21' => ['width' => 670, 'height' => 250],
                '30x30' => ['width' => 670, 'height' => 330],
                '30x40' => ['width' => 670, 'height' => 430],
                '40x30' => ['width' => 870, 'height' => 330],
            ]
        ]
    ];

    private $_covers = [
        self::TYPE_PHOTOBOOK_PREMIUM => [
            '20x20' => [
                self::COVER_HARD => ['src_price' => 175, 'profit' => self::PROFIT_STANDART,],
                self::COVER_TKAN => ['src_price' => 420, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => 350, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 630, 'profit' => self::PROFIT_STANDART,],
            ],
            '21x30' => [
                self::COVER_HARD => ['src_price' => 190, 'profit' => self::PROFIT_STANDART,],
                self::COVER_TKAN => ['src_price' => 508, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => 423, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 763, 'profit' => self::PROFIT_STANDART,],
            ],
            '25x20' => [
                self::COVER_HARD => ['src_price' => 188, 'profit' => self::PROFIT_STANDART,],
                self::COVER_TKAN => ['src_price' => 448, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => 373, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 673, 'profit' => self::PROFIT_STANDART,],
            ],
            '25x25' => [
                self::COVER_HARD => ['src_price' => 238, 'profit' => self::PROFIT_STANDART,],
                self::COVER_TKAN => ['src_price' => 568, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => 473, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 853, 'profit' => self::PROFIT_STANDART,],
            ],
            '30x21' => [
                self::COVER_HARD => ['src_price' => 225, 'profit' => self::PROFIT_STANDART,],
                self::COVER_TKAN => ['src_price' => 540, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => 450, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 810, 'profit' => self::PROFIT_STANDART,],
            ],
            '30x25' => [
                self::COVER_HARD => ['src_price' => 250, 'profit' => self::PROFIT_STANDART,],
                self::COVER_TKAN => ['src_price' => 600, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => 500, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 900, 'profit' => self::PROFIT_STANDART,],
            ],
            '30x30' => [
                self::COVER_HARD => ['src_price' => 325, 'profit' => self::PROFIT_STANDART,],
                self::COVER_TKAN => ['src_price' => 780, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => 650, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 1170, 'profit' => self::PROFIT_STANDART,],
            ],
            '35x30' => [
                self::COVER_HARD => ['src_price' => 350, 'profit' => self::PROFIT_STANDART,],
                self::COVER_TKAN => ['src_price' => 840, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => 700, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 1260, 'profit' => self::PROFIT_STANDART,],
            ],
            '40x30' => [
                self::COVER_HARD => ['src_price' => 400, 'profit' => self::PROFIT_STANDART,],
                self::COVER_TKAN => ['src_price' => 960, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => 800, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 1440, 'profit' => self::PROFIT_STANDART,],
            ]
        ],
        self::TYPE_PHOTOBOOK_STANDART => [
            '20x20' => [
                self::COVER_HARD => ['src_price' => 238,'profit' => self::PROFIT_STANDART, ],
                self::COVER_TKAN => ['src_price' => 571, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => 476, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 856, 'profit' => self::PROFIT_STANDART,],
            ],
            '21x30' => [
                self::COVER_HARD => ['src_price' => 250, 'profit' => self::PROFIT_STANDART,],
                self::COVER_TKAN => ['src_price' => 600, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => 500, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 900, 'profit' => self::PROFIT_STANDART,],
            ],
            '25x25' => [
                self::COVER_HARD => ['src_price' => 325, 'profit' => self::PROFIT_STANDART,],
                self::COVER_TKAN => ['src_price' => 780, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => 650, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 1170, 'profit' => self::PROFIT_STANDART,],
            ],
            '30x21' => [
                self::COVER_HARD => ['src_price' => 250, 'profit' => self::PROFIT_STANDART,],
                self::COVER_TKAN => ['src_price' => 600, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => 500, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 900, 'profit' => self::PROFIT_STANDART,],
            ],
            '30x30' => [
                self::COVER_HARD => ['src_price' => 375, 'profit' => self::PROFIT_STANDART,],
                self::COVER_TKAN => ['src_price' => 900, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => 750, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 1350, 'profit' => self::PROFIT_STANDART,],
            ],
            '30x40' => [
                self::COVER_HARD => ['src_price' => 630, 'profit' => self::PROFIT_STANDART,],
                self::COVER_TKAN => ['src_price' => 1020, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => 850, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 1530, 'profit' => self::PROFIT_STANDART,],
            ],
            '40x30' => [
                self::COVER_HARD => ['src_price' => 630, 'profit' => self::PROFIT_STANDART,],
                self::COVER_TKAN => ['src_price' => 1020, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => 850, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 1530, 'profit' => self::PROFIT_STANDART,],
            ]
        ],
        self::TYPE_VIPUSK_BUTTERFLY => [
            '21x30' => [
                self::COVER_SOFT => ['src_price' => null, 'profit' => self::PROFIT_STANDART,],
                self::COVER_HARD => ['src_price' => 150, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => 220, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 450, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER_TKAN => ['src_price' => 480, 'profit' => self::PROFIT_STANDART,],
            ],
            '23x23' => [
                self::COVER_SOFT => ['src_price' => null, 'profit' => self::PROFIT_STANDART,],
                self::COVER_HARD => ['src_price' => 170, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => 220, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 450, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER_TKAN => ['src_price' => 480, 'profit' => self::PROFIT_STANDART,],
            ]
        ],
        self::TYPE_VIPUSK_PREMIUM => [
            '15x21' => [
                self::COVER_SOFT => ['src_price' => 60, 'profit' => self::PROFIT_STANDART,],
                self::COVER_HARD => ['src_price' => 110, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => null,'price' => null, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => null,'price' => null, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER_TKAN => ['src_price' => null,'price' => null, 'profit' => self::PROFIT_STANDART,],
            ],
            '18x25' => [
                self::COVER_SOFT => ['src_price' => 75, 'profit' => self::PROFIT_STANDART,],
                self::COVER_HARD => ['src_price' => 140, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => 160, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => null,'price' => null, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER_TKAN => ['src_price' => null,'price' => null, 'profit' => self::PROFIT_STANDART,],
            ],
            '20x20' => [
                self::COVER_SOFT => ['src_price' => 135, 'profit' => self::PROFIT_STANDART,],
                self::COVER_HARD => ['src_price' => 205, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => null,'price' => null, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => null,'price' => null, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER_TKAN => ['src_price' => null,'price' => null, 'profit' => self::PROFIT_STANDART,],
            ],
            '25x25' => [
                self::COVER_SOFT => ['src_price' => 120, 'profit' => self::PROFIT_STANDART,],
                self::COVER_HARD => ['src_price' => 170, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => 230, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 450, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER_TKAN => ['src_price' => 500, 'profit' => self::PROFIT_STANDART,],
            ],
            '21x30' => [
                self::COVER_SOFT => ['src_price' => 100, 'profit' => self::PROFIT_STANDART,],
                self::COVER_HARD => ['src_price' => 155, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price' => 210,  'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 430,  'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER_TKAN => ['src_price' => 480, 'profit' => self::PROFIT_STANDART,],
            ],
            '30x21' => [
                self::COVER_SOFT => ['src_price' => 105, 'profit' => self::PROFIT_STANDART,],
                self::COVER_HARD => ['src_price' => 155, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price'=> 220, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 430, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER_TKAN => ['src_price'=> 480,  'profit' => self::PROFIT_STANDART,],
            ],
            '30x30' => [
                self::COVER_SOFT => ['src_price' => 140, 'profit' => self::PROFIT_STANDART,],
                self::COVER_HARD => ['src_price' => 190, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price'=> 230, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 490, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER_TKAN => ['src_price'=> 530, 'profit' => self::PROFIT_STANDART,],
            ],
        ],
        self::TYPE_VIPUSK_STANDART => [
            '15x21' => [
                self::COVER_SOFT => ['src_price' => 110, 'profit' => self::PROFIT_STANDART,],
                self::COVER_HARD => ['src_price' => 135, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price'=> null, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => null, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER_TKAN => ['src_price'=> null, 'profit' => self::PROFIT_STANDART,],
            ],
            '25x25' => [
                self::COVER_SOFT => ['src_price' => 200, 'price' => null, 'profit' => self::PROFIT_STANDART,],
                self::COVER_HARD => ['src_price' => 240, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price'=> 250, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 450,  'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER_TKAN => ['src_price'=> 500, 'profit' => self::PROFIT_STANDART,],
            ],
            '21x30' => [
                self::COVER_SOFT => ['src_price' => 150, 'profit' => self::PROFIT_STANDART,],
                self::COVER_HARD => ['src_price' => 200,  'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price'=> 220, 'price' => 308,'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 430, 'price' => 602,'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER_TKAN => ['src_price'=> 480, 'price' => 672,'profit' => self::PROFIT_STANDART,],
            ],
            '30x21' => [
                self::COVER_SOFT => ['src_price' => null, 'price' => null, 'profit' => self::PROFIT_STANDART,],
                self::COVER_HARD => ['src_price' => 220, 'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price'=> 220,  'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 430, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER_TKAN => ['src_price'=> 480, 'profit' => self::PROFIT_STANDART,],
            ],
            '30x30' => [
                self::COVER_SOFT => ['src_price' => null, 'price' => null, 'profit' => self::PROFIT_STANDART,],
                self::COVER_HARD => ['src_price' => 270,  'profit' => self::PROFIT_STANDART,],
                self::COVER_VINIL => ['src_price'=> 270, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER => ['src_price' => 490, 'profit' => self::PROFIT_STANDART,],
                self::COVER_LEATHER_TKAN => ['src_price'=> 530, 'profit' => self::PROFIT_STANDART,],
            ],
        ],

    ];

    private $_cover_title = [
        self::COVER_SOFT => 'Мягкая',
        self::COVER_HARD => 'Твердая',
        self::COVER_VINIL => 'Бумвинил',
        self::COVER_TKAN => 'Ткань',
        self::COVER_LEATHER => 'Кожзам',
        self::COVER_LEATHER_TKAN => 'Кожзам+холст',
    ];

    private $_type_title = [
        self::TYPE_VIPUSK_PREMIUM => 'Выпускные альбомы ПРЕМИУМ',
        self::TYPE_VIPUSK_BUTTERFLY => 'Выпускные альбомы "бабочка"',
        self::TYPE_VIPUSK_STANDART => 'Выпускные альбомы СТАНДАРТ',
        self::TYPE_PHOTOBOOK_PREMIUM => 'Фотокнига ПРЕМИУМ',
        self::TYPE_PHOTOBOOK_STANDART => 'Фотокнига СТАНДАРТ',

    ];

    /**
     * @param string $type
     * @return array
     */
    public function getFormats($type)
    {
        return $this->_format[$type];
    }

    public function getTypeParams($type) {
        $params = null;
        if (isset($this->_params[$type])) {
            $params = $this->_params[$type];
            foreach ($params as $format => $p) {
                $params[$format]['price_page'] = ceil($p['src_price'] * $p['profit']);
            }
        }

        return $params;
    }

    /**
     * @param string $type
     * @param string $format
     * @return array
     */
    public function getParams($type, $format)
    {
        $params = null;
        if (isset($this->_params[$type][$format])) {
            $params = $this->_params[$type][$format];
            $params['price_page'] = ceil($params['src_price'] * $params['profit']);
        }
        return  $params;
    }

    /**
     * @param $type
     * @param $format
     * @param int $page_num
     * @param string $cover_type
     * @return int
     */
    public function getPrice($type, $format, $page_num = 10, $cover_type = self::COVER_HARD, $design = false, $discount = false)
    {
        $params = $this->getParams($type, $format);
        $price = $page_num * $params['price_page'] + $this->getCoverPrice($type, $format, $cover_type);

        if ($design) {
            $pn = $this->isSinglePage($cover_type) ? $page_num * 2 : $page_num;
            $price += self::PRICE_DESIGN * $pn;
        }

        if ($discount) {
            $price = ceil($price * (1 - $params['discount'] / 100));
        }

        return $price;
    }

    /**
     * @param        $type
     * @param        $format
     * @param int    $page_num
     * @param string $cover_type
     * @param bool   $design
     * @param bool   $discount
     *
     * @return int
     */
    public function getEditorPrice($type, $format, $page_num = 10, $cover_type = self::COVER_HARD, $design = false, $discount = false)
    {
        return round(
            $this->getPrice($type, $format, $page_num = 10, $cover_type = self::COVER_HARD, $design = false, $discount = false) * self::EDITOR_KOEFF / 100
        ) * 100;
    }

    /**
     * Развороты - false, страницы true
     *
     * @param $type
     *
     * @return bool
     */
    public function isSinglePage($type)
    {
        return !in_array($type, [self::TYPE_PHOTOBOOK_PREMIUM, self::TYPE_VIPUSK_PREMIUM, self::TYPE_VIPUSK_BUTTERFLY]);
    }

    public function getCoverPrice($type, $format, $cover_type)
    {
        return  isset($this->_covers[$type][$format][$cover_type]) ?
            round($this->_covers[$type][$format][$cover_type]['src_price'] * $this->_covers[$type][$format][$cover_type]['profit']) :
            null;
    }


    public function getCoverSize($type, $format, $cover_type) {
        return  isset($this->_cover_size[$cover_type][$type][$format]) ?
            $this->_cover_size[$cover_type][$type][$format] :
            null;
    }


    /**
     * @param $type
     * @return array
     */
    public function getTypeCovers($type) {
        $covers = null;

        if (isset($this->_covers[$type])) {
            $covers = $this->_covers[$type];
            foreach ($covers as $format => $cvrs) {
                foreach ($cvrs as $cover_type => $cover) {
                    $covers[$format][$cover_type]['price'] = ceil($cover['src_price'] * $cover['profit']);
                }
            }
        }

        return $covers;
    }

    /**
     * @param $type
     * @param $format
     * @return array
     */
    public function getCovers($type, $format) {
        $covers = null;

        if (isset($this->_covers[$type][$format])) {
            $covers = $this->_covers[$type][$format];
            foreach ($covers as $cover_type => $cover) {
                $covers[$cover_type]['price'] = ceil($cover['src_price'] * $cover['profit']);
            }
        }

        return $covers;
    }

    public function getCoverTitle($cover_type)
    {
        return $this->_cover_title[$cover_type];
    }

    public function getBookTitle($type)
    {
        return $this->_type_title[$type];
    }

    /**
     * @param $type
     * @param $format
     * @return bool
     */
    public function hasDiscount($type, $format) {
        $params = $this->getParams($type, $format);
        return (bool)$params['discount'];
        throw new CHttpException(404);
    }

    /**
     * @param $type
     * @return string
     */
    public function toJson($type)
    {
        $params = $this->getTypeParams($type);
        $covers = $this->getTypeCovers($type);

        $data = [
            'formats' => $this->_format[$type],
            'params' => $params,
            'covers' => $covers,
        ];

        return json_encode($data);
    }

    /**
     * Стоимость отделки уголками
     *
     * @return float
     */
    public function getCornerPrice()
    {
        return $this->_corner_price;
    }

    /**
     * Стоимость фотовставки
     *
     * @return float
     */
    public function getPhotoPrice()
    {
        return $this->_photo_price;
    }

    /**
     * Стоимость отделки срезов
     *
     * @return float
     */
    public function getCutPrice()
    {
        return $this->_cut_price;
    }

    /**
     * @return int
     */
    public function getForzacPrice()
    {
        return $this->_forzac_price;
    }
}