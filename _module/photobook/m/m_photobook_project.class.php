<?php
/**
* модель m_photobook_project
*
*/

class m_photobook_project extends m_photobook_project_base
{
	private $project;

	/**
	 * Returns the static model of the specified AR class.
     * @param string $className
	 * @return m_photobook_project the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @param m_photobook_client $client
	 * @param array $params
	 */
	public function create(m_photobook_client $client, $params)
	{
		$this->id_photobook_client = $client->id_photobook_client;
		$params['user_id'] = $client->user_id;

		$api = new k_photobook_api();
		$this->project = $api->addProject($params);

		$this->photobook_id = $this->project->getId();
		$this->photobook_name = $this->project->name;
		$this->params = serialize($this->project);
		$this->save();
	}

	/**
	 * @return k_photobook_api_project
	 */
	public function getProject()
	{
		if (!empty($this->project)) {
			return $this->project;
		}

		if (!empty($this->params)) {
			$this->project = unserialize($this->params);
		}

		return $this->project;
	}
}
