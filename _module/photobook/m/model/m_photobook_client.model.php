<?php
$model = array(
	'class_name'=>'m_photobook_client',
	'table' => 'photobook_client',
	'pk'=>'id_photobook_client',
	'auto_increment'=>'id_photobook_client',
	'fields'=> array(
		'id_photobook_client' => 
		  array (
			'Type'=>'int',
			'Null'=>'NOT NULL',
			'Default'=>'',
			'length'=>11,
			'is_required' => 0,
			'reg_name' => "",
			'title'=>'ID'
		  ),
		'id_client' =>
			array (
				'Type'=>'int',
				'Null'=>'NOT NULL',
				'Default'=>'',
				'length'=> 11,
				'is_required' => 1,
				'reg_name' => NULL,
				'title'=>'Клиент'
			),
		'login' =>
		  array (
			'Type'=>'varchar',
			'Null'=>'NULL',
			'Default'=>'',
			'length'=> 255,
			'is_required' => 1,
			'reg_name' => NULL,
			'title'=>'Логин для ЛК'
		  ),
		'user_hash' =>
			array (
				'Type'=>'text',
				'Null'=>'NULL',
				'Default'=>'',
				'length'=> '',
				'is_required' => 1,
				'reg_name' => null,
				'title'=>'User Hash'
			),
		'user_id' =>
			array (
				'Type'=>'int',
				'Null'=>'NULL',
				'Default'=>'',
				'length'=> 11,
				'is_required' => 1,
				'reg_name' => null,
				'title'=>'ID пользователя'
			),
	),
	"indexes"=> array(
		),
	"fk"=>array(
		
	),
	"form"=>array(
	),
	"relations"=>array(
		"client"=>array("BELONGS_TO", "m_client", "id_client"),

	),
	"defaultScope"=>array(
		),		
	"filters" => array(
	)
);
