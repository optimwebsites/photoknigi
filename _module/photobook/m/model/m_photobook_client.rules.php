<?php

return array(
	
	//проверка int, обязательных для заполнения
	array(
		'id_client, user_id',
		'numerical',
		'allowEmpty' => false,
	),
	//проверка int, НЕобязательных для заполнения
	//array(
	//	'',
	//	'numerical',
	//	'allowEmpty' => true,
	//),
	//проверка string, обязательных для заполнения
	array(
		'login, user_hash',
		'required'
	),
	//удаление тегов
	array(
		'login, user_hash',
		'filter',
		'filter' => 'strip_tags',
	),
	//проверка date, обязательных для заполнения
	//array(
	//	'',
	//	'date',
	//	'format' => 'yyyy-MM-dd',
	//	'allowEmpty' => false,
	//),
	//проверка date, НЕобязательных для заполнения
	//array(
	//	'',
	//	'date',
	//	'format' => 'yyyy-MM-dd',
	//	'allowEmpty' => true,
	//),
	//безопасные аттрибуты при insert. УДАЛИТЬ ОТСЮДА ТО, ЧТО НЕ ДОЛЖНО ПРИХОДИТЬ С ФОРМЫ В АДМИНКЕ ПРИ СОЗДАНИИ ЗАПИСИ
	array(
		'id_client, login, user_hash, user_id',
		'safe',
		'on' => 'insert',
	),
	//безопасные аттрибуты при update. УДАЛИТЬ ОТСЮДА ТО, ЧТО НЕ ДОЛЖНО ПРИХОДИТЬ С ФОРМЫ В АДМИНКЕ ПРИ СОЗДАНИИ ЗАПИСИ
	array(
		'id_client, login, user_hash, user_id',
		'safe',
		'on' => 'update',
	),	
);