<?php

return array(
	
	//проверка int, обязательных для заполнения
	array(
		'id_photobook_client',
		'numerical',
		'allowEmpty' => false,
	),
	//проверка int, НЕобязательных для заполнения
	//array(
	//	'',
	//	'numerical',
	//	'allowEmpty' => true,
	//),
	//проверка string, обязательных для заполнения
	array(
		'params, photobook_name',
		'required'
	),
	//удаление тегов
	array(
		'params, photobook_name',
		'filter',
		'filter' => 'strip_tags',
	),
	//проверка date, обязательных для заполнения
	//array(
	//	'',
	//	'date',
	//	'format' => 'yyyy-MM-dd',
	//	'allowEmpty' => false,
	//),
	//проверка date, НЕобязательных для заполнения
	//array(
	//	'',
	//	'date',
	//	'format' => 'yyyy-MM-dd',
	//	'allowEmpty' => true,
	//),
	//безопасные аттрибуты при insert. УДАЛИТЬ ОТСЮДА ТО, ЧТО НЕ ДОЛЖНО ПРИХОДИТЬ С ФОРМЫ В АДМИНКЕ ПРИ СОЗДАНИИ ЗАПИСИ
	array(
		'id_photobook_client, photobook_id, params, photobook_name',
		'safe',
		'on' => 'insert',
	),
	//безопасные аттрибуты при update. УДАЛИТЬ ОТСЮДА ТО, ЧТО НЕ ДОЛЖНО ПРИХОДИТЬ С ФОРМЫ В АДМИНКЕ ПРИ СОЗДАНИИ ЗАПИСИ
	array(
		'id_photobook_client, photobook_id, params, photobook_name',
		'safe',
		'on' => 'update',
	),	
);