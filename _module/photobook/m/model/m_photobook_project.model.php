<?php
$model = array(
	'class_name'=>'m_photobook_project',
	'table' => 'photobook_project',
	'pk'=>'id_photobook_project',
	'auto_increment'=>'id_photobook_project',
	'fields'=> array(
		'id_photobook_project' => 
		  array (
			'Type'=>'int',
			'Null'=>'NOT NULL',
			'Default'=>'',
			'length'=>11,
			'is_required' => 0,
			'reg_name' => "",
			'title'=>'ID'
		  ),
		'id_photobook_client' =>
			array (
				'Type'=>'int',
				'Null'=>'NOT NULL',
				'Default'=>'',
				'length'=> 11,
				'is_required' => 1,
				'reg_name' => NULL,
				'title'=>'Клиент'
			),
		'photobook_id' => array (
			'Type'=>'varchar',
			'Null'=>'NOT NULL',
			'Default'=>'',
			'length'=> 50,
			'is_required' => 1,
			'reg_name' => NULL,
			'title'=>'ID фотокниги'
		),
		'params' =>
		  array (
			'Type'=>'text',
			'Null'=>'NULL',
			'Default'=>'',
			'length'=> '',
			'is_required' => 1,
			'reg_name' => NULL,
			'title'=>'Параметры фотокниги'
		  ),
		'photobook_name' => array (
			'Type'=>'varchar',
			'Null'=>'NOT NULL',
			'Default'=>'',
			'length'=> 255,
			'is_required' => 1,
			'reg_name' => NULL,
			'title'=>'Клиент'
		),
	),
	"indexes"=> array(
		),
	"fk"=>array(
		
	),
	"form"=>array(
	),
	"relations"=>array(
		"photobook_client"=>array("BELONGS_TO", "m_photobook_client", "id_photobook_client"),
	),
	"defaultScope"=>array(
		),		
	"filters" => array(
	)
);
