<?php

/**
*   Базовый класс-модель m_photobook_project_base
*	Рализует основное поведение модели, описанное в файле /_module/photobook/m/m_photobook_project_base.class.php
*	НЕ ИЗМЕНЯТЬ ГЕНЕРИРУЕТСЯ АВТОМАТИЧЕСКИ ПРИ ИЗМЕНЕНИИ ФАЙЛА /_module/photobook/m/model/m_photobook_project.model.php
*/

/**
 * @property int $id_photobook_project
 * @property int $id_photobook_client
 * @property int $photobook_id
 * @property string $photobook_hash
 * @property string $params
 * @property string $photobook_name
 * @property m_photobook_client $photobook_client
 */
abstract class m_photobook_project_base extends BaseModel {

	
	/**
	 * @return string
	 */
	public function tableName()
	{
		return 'photobook_project';
	}

	/**
	 * @return string
	 */
	public function primaryKey()
	{
		return 'id_photobook_project';
	}

	/**
	 * Returns the attribute labels.
	 * Attribute labels are mainly used in error messages of validation.
	 * By default an attribute label is generated using {@link generateAttributeLabel}.
	 * This method allows you to explicitly specify attribute labels.
	 *
	 * Note, in order to inherit labels defined in the parent class, a child class needs to
	 * merge the parent labels with child labels using functions like array_merge().
	 *
	 * @return array attribute labels (name=>label)
	 * @see generateAttributeLabel
	 */
	public function attributeLabels()
	{
		return array("id_photobook_project"=>"ID",
			"id_photobook_client"=>"Клиент",
			"photobook_id"=>"Клиент",
			"photobook_hash"=>"Клиент",
			"params"=>"Параметры фотокниги",
			"photobook_name"=>"Клиент");
	}

    /**
     * Зависимости для модели
     * @return array
     */
    public function relations()
    {
        return [
                "photobook_client" => [self::BELONGS_TO, "m_photobook_client", "id_photobook_client"],
            ];
    }

	/**
	*  перед удалением удаляем данные во всех зависимых таблицах
	*  если не запрещено
	*/
	public function beforeDelete()
	{

		$restrict = array();
		foreach ($this->relations() as $rel => $r)
		{
			if ($r[0] === self::HAS_MANY)
			{
                /** @var CActiveRecord $d */
				foreach ($this->$rel as $d) {
					$d->delete();
                }
			}
		}

		return true;
	}

	/*
	*   Правила валидации для модели подключаются через внешний файл
	*	файл /_module/photobook/m/model/m_photobook_project.rules.php	*/
	public function rules()
	{
		$rules = Modules::_find('m_photobook_project','model','rules');
		if ($rules)
			return include($rules);
		else
			return array();
	}




}
