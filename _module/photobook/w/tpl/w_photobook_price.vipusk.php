<?php
/**
 * @var w_photobook_price $this
 */
$covers = $this->getCovers();

?>
<div id="book_price_<?=$this->type?>">
<div class="bottom-padding-mini vipusk-calc__format">Выберите формат:
    <?php foreach ($this->getFormats() as $format) {
        $class = $format == $this->format ? 'btn-warning' : 'btn-default';
    ?>
        <button type="button" class="load-book-price btn <?=$class?>" data-type="<?=$this->type?>" data-format="<?=$format?>"><?=$format?></button>
    <?php } ?>
</div>
<h3>Формат <?=$this->format?></h3>

<table class="table table-striped">
    <thead>
    <tr>
        <th class="text-center">Обложка &#8594</th>
        <?php foreach ($covers as $cover_type => $params) { ?>
        <th class="text-center"><?=$this->photobook->getCoverTitle($cover_type)?></th>
        <?php } ?>
    </tr>
    </thead>
    <tbody class="text-center">
    <tr>
        <td>Стоимость <?= $this->photobook->isSinglePage($this->type) ? 'страницы' : 'разворота' ?></td>
        <?php foreach ($covers as $format => $params) { ?>
            <td><?= $this->photobook->isSinglePage($this->type) ? round($this->getPagePrice()/2, 1) : $this->getPagePrice();?></td>
        <?php } ?>
    </tr>
    <tr>
        <td>Стоимость обложки</td>
        <?php foreach ($covers as $cover_type => $params) { ?>
            <td><?=$params['price'] ?: "-" ?></td>
        <?php } ?>
    </tr>
    <tr>
        <th colspan="<?=count($covers)+1?>" class="text-center">Пример стоимости печати выпускного альбома</th>
    </tr>
    <?php foreach ($this->samples[$this->type] as $page_num) {
       $pn=  $page_num;
        if ($this->photobook->isSinglePage($this->type)) {
            $pn  = $page_num * 2;
            $page_title = Utils::pluralForm($pn, "страница", "страницы", "страниц");
        } else {
            $page_title =Utils::pluralForm($pn, "разворот", "разворота", "разворотов");
        }

        ?>
    <tr>
        <td><?=$pn." ".$page_title?></td>
        <?php foreach ($covers as $cover_type => $params) { ?>
            <td><?=$params['price'] ? $this->photobook->getPrice($this->type, $this->format, $page_num, $cover_type) : "-";?></td>
        <?php } ?>
    </tr>
    <?php } ?>
    </tbody>
</table>
    </div>
