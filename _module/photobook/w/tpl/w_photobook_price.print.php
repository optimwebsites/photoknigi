<?php
/**
 * @var w_photobook_price $this
 */
$covers = $this->getCovers();
$form_id = "book_price_". $this->type;
?>
<div id="<?=$form_id?>" class="calc-container">
    <input type="hidden" name="single_page" value="<?=(int)$this->photobook->isSinglePage($this->type)?>" />
    <style>
        .calc-container {
            margin-top:20px;
        }
        .calc-params {
            margin:0;
            padding:0;
        }
        .calc-params li {
            margin:10px 0;
            list-style: none;
        }

        .calc-label {
            width:13rem;
        }

        .calc-result-label {
            background: #f5f5f5; height:30px;
            padding:0 20px;
        }

        .calc-result-dotted {
            border-bottom: 3px dotted #999;
        }

        .calc-prices {
            border:1px solid #ddd;
            border-radius:4px;
            background:#f5f5f5;
            font-size:1.1rem;
        }
        .calc-prices .row,
        .calc-price-html > *
        {
            margin:10px 0;
            min-height:30px;
        }

        .calc-prices .row.total {
            border-top: 1px solid #d0d0d0;
            padding:10px 0;
            font-size:1.5rem;
        }

        .calc-total {
            font-size:1.7rem;
        }


    </style>
    <script type="text/javascript">
        if (!photoBookData) {
           var photoBookData = {};
        }
        photoBookData['<?=$form_id?>'] = <?=$this->photobook->toJson($this->type)?>;
    </script>
    <div class="row">
        <div class="col-sm-4">
            <ul class="calc-params">
                <li>
                    <div class="dropdown">
                        <div class="btn-group">
                            <label class="btn btn-default calc-label">
                                Формат:

                            </label>
                            <label id="<?=$form_id?>_format_value_label" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-danger">
                                <span class="label-value"><?=$this->getFormatTitle()?></span>
                                <span class="caret"></span>
                            </label>
                            <ul class="dropdown-menu calc-dropdown calc-format" data-target="<?=$form_id?>">
                                <?php foreach ($this->getFormats() as $format) {
                                    $class = $format == $this->format ? 'active' : '';
                                    ?>
                                    <li class="<?=$class?>"><a href="javascript:void(0)" data-value="<?=$format?>" data-name="<?=$form_id . "_format_value"?>"><?=$format?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="dropdown">
                        <div class="btn-group">
                            <label class="btn btn-default calc-label">
                                Обложка:
                                <input type="hidden" class="calc-value" name="<?=$form_id?>_cover_value" value="<?=$this->getCoverPrice() ?>"
                                    data-label="Стоимость обложки"
                                    />
                            </label>
                            <label id="<?=$form_id?>_cover_value_label" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn btn-danger">
                                <span class="label-value"><?= $this->getCoverTitle(); ?></span>
                                <span class="caret"></span>
                            </label>
                            <ul class="dropdown-menu calc-dropdown calc-cover" data-target="<?=$form_id?>">
                                <?php foreach ($covers as $cover_type => $cover) {
                                    $class = $this->photobook->getCoverTitle($cover_type) == $this->getCoverTitle() ? "active" : "";
                                    ?>
                                    <li class="<?=$class?>"><a href="javascript:void(0)" data-value="<?=$cover_type?>" data-name="<?=$form_id . "_cover_value"?>"><?=$this->photobook->getCoverTitle($cover_type)?></a></li>
                                <?php } ?>
                            </ul>
                        </div>
                    </div>
                </li>
                <li>
                    <div class="btn-group" data-toggle="buttons">
                        <?php
                            $active_yes = $this->has_corners ? "active" : "";
                            $active_no = !$this->has_corners ? "active" : "";
                        ?>
                        <label class="btn btn-default calc-label">
                            Уголки
                            <input type="hidden" class="calc-value" name="<?=$form_id?>_has_corners_value" data-value="0"
                                data-label="Отделка уголками" value="0"
                                />
                        </label>
                        <label class="btn btn-danger <?=$active_no?> radio-tool" data-name="<?=$form_id?>_has_corners_value" for="<?=$form_id?>_has_corners_1">
                            <input type="radio" id="<?=$form_id?>_has_corners_1" name="has_corners" autocomplete="off" value="0"> Нет
                        </label>
                        <label class="btn btn-danger <?=$active_yes?> radio-tool" data-name="<?=$form_id?>_has_corners_value" for="<?=$form_id?>_has_corners_2">
                            <input type="radio" id="<?=$form_id?>_has_corners_2" name="has_corners" autocomplete="off" value="<?=$this->getCornerPrice()?>"> Да
                        </label>
                    </div>
                </li>
               <!-- <li>
                    <div class="btn-group" data-toggle="buttons">
                        <?php
                        $active_yes = $this->has_photo ? "active" : "";
                        $active_no  = !$this->has_photo ? "active" : "";
                        ?>
                        <label class="btn btn-default calc-label">
                            Фотовставка
                            <input type="hidden" class="calc-value" name="<?=$form_id?>_has_photo_value" data-value="0" data-label="Фотовставка"
                                   data-label="Фотовставка"
                                />
                        </label>
                        <label class="btn btn-danger <?=$active_no?> radio-tool" data-name="<?=$form_id?>_has_photo_value" for="<?=$form_id?>_has_photo_1">
                            <input type="radio" id="<?=$form_id?>_has_photo_1" name="has_photo" autocomplete="off" value="0"> Нет
                        </label>
                        <label class="btn btn-danger <?=$active_yes?> radio-tool" data-name="<?=$form_id?>_has_photo_value" for="<?=$form_id?>_has_photo_2">
                            <input type="radio" id="<?=$form_id?>_has_photo_2" name="has_photo" autocomplete="off" value="<?=$this->getPhotoPrice()?>"> Да
                        </label>
                    </div>
                </li>-->
                <li>
                    <div class="btn-group" data-toggle="buttons">
                        <?php
                        $active_yes = $this->has_cut_decor ? "active" : "";
                        $active_no  = !$this->has_cut_decor ? "active" : "";
                        ?>
                        <label class="btn btn-default calc-label">
                            Отделка срезов
                            <input type="hidden" class="calc-value" name="<?=$form_id?>_has_cut_decor_value" data-value="0"
                                   data-label="Отделка срезов" value="0"
                                />
                        </label>
                        <label class="btn btn-danger <?=$active_no?> radio-tool" data-name="<?=$form_id?>_has_cut_decor_value" for="<?=$form_id?>_has_cut_decor_1">
                            <input type="radio"  id="<?=$form_id?>_has_cut_decor_1" name="has_cut_decor" autocomplete="off" value="0"> Нет
                        </label>
                        <label class="btn btn-danger <?=$active_yes?> radio-tool" data-name="<?=$form_id?>_has_cut_decor_value" for="<?=$form_id?>_has_cut_decor_2">
                            <input type="radio"  id="<?=$form_id?>_has_cut_decor_2" name="has_cut_decor" autocomplete="off" value="<?=$this->getCutPrice()?>"> Да
                        </label>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col-sm-8">
            <div class="calc-prices">
                <div class="row calc-price-html">

                </div>
                <div class="row">
                    <?php
                    $page_num = $this->photobook->isSinglePage($this->type) ? 14 : 8;
                    $maximum = $this->photobook->isSinglePage($this->type) ? 80 : 40;
                    $minimum = $this->photobook->isSinglePage($this->type) ? 14 : 2;
                    ?>
                    <div class="col-xs-12">
                        <div class="calc-result-dotted ">
                            <div class="calc-result-label pull-left">Количество <?= $this->photobook->isSinglePage($this->type) ? 'страниц' : 'разворотов' ?></div>
                            <div class="calc-result-label pull-right">
                                <div class="btn-group num-iterator">
                                    <label class="btn btn-danger less" data-minimum="<?=$minimum?>" > - </label>
                                    <label class="btn btn-default"><span class="num-iterator-label"><?=$page_num?></span><input type="hidden" class="num-iterator-num" value="<?=$page_num?>"/></label>
                                    <label class="btn btn-danger more" data-maximum="<?=$maximum?>"> + </label>
                                </div>
                            </div>
                            <br/>
                        </div>
                    </div>
                </div>
            <div class="row total">
                <div class="col-xs-6">
                    <button class="btn btn-danger scroll-to" data-target="#order-form">Заказать печать</button>
                </div>
                <div class="col-xs-6 text-right">
                    Итого:&nbsp;&nbsp; <span class="calc-total text-red">1234</span> <span class="text-red">р.</span>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
