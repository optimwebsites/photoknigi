/**
 * Created by nikolay on 06.02.16.
 */
$(document).on('click', '.load-book-price', function (e) {
    var format = $(this).data('format');
    var type = $(this).data('type');
    $.get("/_c/photobook/loadPrice/s/?format="+format+"&type="+type, function(data) {
        $("#book_price_"+type).html(data);

    });
});


function calcBookInit()
{
    $(".calc-container").each(function() {
       calcBook(this);
    });
}

function calcBook(c)
{
    var id = $(c).attr('id');
    var format = $(c).find(".calc-format li.active a").data('value');
    var cover = $(c).find(".calc-cover li.active a").data('value');
    var singlePage = parseInt($(c).find("[name=single_page]").val());
    var page_num = $(c).find(".num-iterator-num").val();
    var pagePrice =  photoBookData[id]['params'][format]['price_page'];

    var coverPrice = photoBookData[id]['covers'][format][cover]['price'];
    var corners = parseInt($("[name=" + id + "_has_corners_value]").val());
    var cutDecor = parseInt($("[name=" + id + "_has_cut_decor_value]").val());

    var priceHtml = $(c).find(".calc-price-html");
    var html = '';
   /* $(c).find(".calc-value").each(function() {
        var page_multiply = is_for_page == undefined ? 1 : page_num;
        var val = $(this).val();
        total += page_multiply * val;
        if (val > 0) {
            html += '<div class="col-xs-12">' +
            '<div class="calc-result-dotted ">' +
            '<div class="calc-result-label pull-left">' + $(this).data('label') + '</div>' +
            '<div class="calc-result-label pull-right">' + val + '</div>' +
            '<br/>' +
            '</div>' +
            '</div>';
        }

    });*/
    html += drawCalc(singlePage == 1 ? 'Стоимость страницы' : 'Стоимость разворота', pagePrice);
    html += drawCalc('Стоимость обложки', coverPrice);

    priceHtml.html(html);
    $(c).find(".calc-total").html(pagePrice * page_num + coverPrice + corners + cutDecor);

    function drawCalc(label, value) {
        return '<div class="col-xs-12">' +
            '<div class="calc-result-dotted ">' +
            '<div class="calc-result-label pull-left">' + label + '</div>' +
            '<div class="calc-result-label pull-right">' + value + '</div>' +
            '<br/>' +
            '</div>' +
            '</div>';
    }
}

$(document).ready(function() {
    calcBookInit();

    $(".radio-tool").on('click', function(){
        var id = $(this).attr('for');
        var name = $(this).data('name');
        var value = $("#" + id).val();
        $("[name=" + name + "]").val(value);
        var cont = $(this).closest(".calc-container");
        calcBook(cont);
    });

    $(".calc-format a, .calc-cover a").on('click', function() {
        var name = $(this).data('name');
        var value = $(this).data('value');
        $(this).closest('ul').find('li').removeClass('active');
        $(this).parent().addClass('active');

        $("[name=" + name + "]").val(value);
        var cont = $(this).closest(".calc-container");
        $(cont).find("#"+name+"_label .label-value").html($(this).html());
        calcBook(cont);
    });

    $(".num-iterator .btn").on("click", function() {

        if (!$(this).hasClass("less") && !$(this).hasClass("more")) {
            return;
        }

        var cont = $(this).closest('.calc-container');
        var parent = $(this).closest(".num-iterator");
        var input = $(parent).find(".num-iterator-num");
        var label = $(parent).find(".num-iterator-label");
        var value = input.val();
        if ($(this).hasClass("less")) {
            value--;
            if ($(this).data('minimum') > value) {
                value = $(this).data('minimum');
            }
        }

        if ($(this).hasClass("more")) {
            value++;
            if ($(this).data('maximum') < value) {
                value = $(this).data('maximum');
            }
        }

        if (value < 0) {
            value = 0;
        }

        if (value > 80) {
            value = 80;
        }

        input.val(value);
        label.html(value);
        calcBook(cont);
    });
});



