<?php
/**
 * Created by PhpStorm.
 * User: nikolay
 * Date: 06.02.16
 * Time: 14:59
 */

class w_photobook_price extends widget {

    public $js = [
        ['_module/photobook/w/js/w_photobook_price.js', 'dynamic'],
        ['/_th/assets/bootstrap/3.2.0/bootstrap.min.js', 'dynamic']
    ];

    public $type = null;

    public $format = null;

    public $samples = [
        m_photobook::TYPE_VIPUSK_BUTTERFLY => [2,5,7,10],
        m_photobook::TYPE_VIPUSK_PREMIUM   => [2,5,7,10],
        m_photobook::TYPE_VIPUSK_STANDART  => [7,10,12],
    ];

    public $cover_type = m_photobook::COVER_HARD;

    public $has_corners = false;

    public $has_cut_decor = false;

    public $has_photo = false;

    /**
     * @var m_photobook
     */
    public $photobook = null;

    public function run()
    {
        $this->photobook = new m_photobook();
    }

    public function getCovers()
    {
        return $this->photobook->getCovers($this->type, $this->format);
    }

    public function getCoverTitle()
    {
        return $this->photobook->getCoverTitle($this->cover_type);
    }

    public function getFormats()
    {
        return $this->photobook->getFormats($this->type);
    }

    public function getFormatPagePrice($format)
    {
        $params = $this->photobook->getParams($this->type, $format);
        return $params['price_page'];
    }

    public function getPagePrice()
    {
        $params = $this->photobook->getParams($this->type, $this->format);
        return $params['price_page'];
    }

    public function getCoverTypePrice($cover_type)
    {
        return $this->photobook->getCoverPrice($this->type, $this->format, $cover_type);
    }

    public function getCoverPrice()
    {
        return $this->photobook->getCoverPrice($this->type, $this->format, $this->cover_type);
    }

    public function getFormatTitle()
    {
        return $this->format;
    }

    /**
     * Стоимость отделки уголками
     *
     * @return float
     */
    public function getCornerPrice()
    {
        return $this->photobook->getCornerPrice();
    }

    /**
     * Стоимость фотовставки
     *
     * @return float
     */
    public function getPhotoPrice()
    {
        return $this->photobook->getPhotoPrice();
    }

    /**
     * Стоимость фотовставки
     *
     * @return float
     */
    public function getCutPrice()
    {
        return $this->photobook->getCutPrice();
    }
}