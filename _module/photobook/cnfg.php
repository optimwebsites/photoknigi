<?php

return [
    "default" => [
        "editor_api_url" => [
            "value" => "http://e.funfotobook.ru",
            "title" => "Api",
            "editable" => true
        ],
        "editor_api_token" => [
            "value" => "MN0mUzW9YEyaT5Q6",
            "title" => "Токен",
            "editable" => true
        ],
        "editor_url" => [
            "value" => "http://e.funfotobook.ru/products/photobooks/constructor/edit",
            "title" => "Редактор фотокниг",
            "editable" => true
        ],
        "cookie_domain" => [
            "value" => ".funfotobook.ru",
            "title" => "Домен редактора",
            "editable" => true
        ],
        "error_page" => [
            "value" => "/fotoknigi/konstruktor_fotoknig/",
            "title" => "Редактор фотокниг",
            "editable" => true
        ],
        "editor_types" => [
            "value" => 1,
            "title" => "Форматы фотокниг",
            "editable" => true
        ],
        "formats" => [
            "value" => [
                408 => ["title" => "20 x 20", "id_format" => "20x20" ],
                412 => ["title" => "21 x 30", "id_format" => "21x30"],
                409 => ["title" => "30 x 21", "id_format" => "30x21"],
                411 => ["title" => "25 x 25", "id_format" => "25x25"],
                413 => ["title" => "30 x 30", "id_format" => "30x30"],
                414 => ["title" => "40 x 30", "id_format" => "40x30"],
            ],
            "title" => "Форматы фотокниг",
            "editable" => true
        ],
        "templates" => [
            "value" => [
                //296 => ['title' => "Инстабук", 'formats' => ["20x20"]],
                282 => ['title' => "Незабываемый отдых", 'formats' => [408 => "20x20"]],
                288 => ['title' => "Зимние грезы", 'formats' => [408 => "20x20"]],
                284 => ['title' => "Наш малыш", 'formats' => [408 => "20x20"]],
                285 => ['title' => "Наша малышка", 'formats' => [408 => "20x20"]],
                286 => ['title' => "Морские приключения", 'formats' => [408 => "20x20"]],
                287 => ['title' => "Love IS", 'formats' => [408 => "20x20"]],
                ],
            "title" => "Форматы фотокниг",
            "editable" => true
        ],

    ],
    "dev1" => [
        "editor_api_url" => [
            "value" => "http://nz7zia.srv001.optimweb.ru/_c/photobook/test/s/?api_path=",
            "title" => "Страница создания проекта",
            "editable" => true
        ],
        "cookie_domain" => [
            "value" => ".nz7zia.srv001.optimweb.ru",
            "title" => "Домен редактора",
            "editable" => true
        ],
        "editor_url" => [
            "value" => "http://e.nz7zia.srv001.optimweb.ru/products/photobooks/constructor/edit",
            "title" => "Редактор фотокниг",
            "editable" => true
        ],
    ]
];
