<?php
/**
 * Created by PhpStorm.
 * User: nikolay
 * Date: 20.01.16
 * Time: 9:40
 */

class s_photobook extends controller {

    protected $accessRules=["*" => ["*" => 1]];

    public function createPhotobook()
    {
        $order = m_order::model()->findByPk(Yii::app()->request->getQuery('order'));
        if (!$order) {
            throw new CHttpException(404);
        }

        $photobook_client = m_photobook_client::model()->byIdClient($order->id_client)->find();
        if (!$photobook_client) {
            $photobook_client = new m_photobook_client();
            $photobook_client->createFromOrder($order);
        }

        try {
            $photobook_project = new m_photobook_project();
            $photobook_project->create($photobook_client, json_decode($order->param, 1));
            $editor_project = $photobook_project->getProject();
            $this->redirectUrl = Yii::app()->params['photobook']['editor_url'] . "/" . $editor_project->getId();
        } catch (Exception $e) {
            $mail = new m_mail();
            $mail->subject = "Ошибка в API редактора";
            $mail->to = Yii::app()->params['cms']['admin_email'];
            $mail->msg = $e->getMessage();
            $mail->save();
            throw $e;
        }
    }

    public function test($m_page_module)
    {
        $path = Yii::app()->request->getQuery("api_path");

        switch ($path) {
            case "/companies/tokens/get/abcd":
                echo '{"access_token":"LO1mDTgsMXYPzNBfhGe3SqF9WZ7JlyIV","expires":"Mon, 08 Feb 2016 16:24:42 +0300"}';
                break;
            case "/api/photobooks/projects/add":
                echo '{"project": {
                        "id": "806",
                        "hash": "uIkiV2Ce",
                        "name": "08.02.2016 12:52",
                        "user_id": "257",
                        "user_hash": "7bff0b01e5ce25269fa1a14dd8a805b5",
                        "company_id": "2",
                        "template_group_id": "0",
                        "type_id": "3",
                        "format_id": "2",
                        "calculator": {},
                        "is_editable": "1",
                        "is_public": false,
                        "update_date": "2016-02-08 16:41:08",
                        "description": "",
                        "constructor": {}
                    }
                }';
                break;
        }
        exit;
    }

    private function getResponseCookie($curl)
    {
        $rows = explode("\n", $curl->getHead());

        $cookie = [];
        foreach ($rows as $row) {
            $params = explode(":", $row);
            if ($params[0] == "Set-Cookie") {
                $args = explode(";", $params[1]);
                $name = null;
                foreach ($args as $a) {
                    $p = explode("=", $a);
                    if ($name === null) {
                        $name = trim($p[0]);
                        $cookie[$name]['value'] = $p[1];
                    } else {
                        $cookie[$name][trim($p[0])] = $p[1];
                    }
                }


            }

        }

        return $cookie;
    }

    /**
     * @param m_page_module $m_page_module
     */
    public function loadPrice(m_page_module $m_page_module)
    {
        $type = Yii::app()->request->getQuery('type');
        $format = Yii::app()->request->getQuery('format');
        $price = new w_photobook_price();
        $price->type = $type;
        $price->format = $format;
        $price->tpl = 'vipusk';
        return $price;
    }
}