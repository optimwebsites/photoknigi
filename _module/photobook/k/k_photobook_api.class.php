<?php
/**
 * Created by PhpStorm.
 * User: nikolay
 * Date: 20.01.16
 * Time: 9:40
 */

class k_photobook_api {

    private $token = null;

    private $secret_key = 'abcd';

    public function __construct()
    {
        $this->secret_key = Yii::app()->params['photobook']['editor_api_token'];
    }

    public function call($service, $params, $method = "POST")
    {
        $curl = new Curl();
        if ($method == "POST") {
            $curl->setPostData($params);
        }
        $p = Yii::app()->params;
        $url = $p['photobook']['editor_api_url'] . $service;

        Log::_log("Вызов url^ " . $url, 'photobook');
        Log::_log("Параметры: " . print_r($params, 1), 'photobook');

        $curl->setUrl($url);
        if ($curl->isOk()) {
            Log::_log("Успешно", 'photobook');
            Log::_log($curl->getBody(), 'photobook');
            return (new k_photobook_api_response($curl->getBody()))->getResponse();
        } else {
            Log::_log("Ошибка " . $curl->errno . " " . $curl->error, 'photobook');
            Log::_log(print_r($curl->getHead()), 'photobook');
            throw new Exception("Ошибка " . $curl->errno . " " . $curl->error);
        }
    }

    private function getToken()
    {
        if ($this->token) {
            return $this->token;
        }

        $response = $this->call("/api/companies/tokens/get/".$this->secret_key, []);
        $this->token = $response->access_token;
        return $this->token;
    }

    /**
     * Параметры
     * access_token - ключ доступа к API
    book_types - ID типа фотокниг
    formats - ID формата фотокниги
    name - название проекта, по умолчанию берется текущая дата в формате d.m.Y H:i.
    group_id - ID шаблона фотокниг
    cover - ID обложки фотокниги
    cover_material - ID материала обложки фотокниги
    decor - ID декора фотокниги
    decor_locations - ID варианта размещения декора фотокниги
    decor_forms - ID варианта нанесения декора фотокниги
    paper - ID бумаги фотокниги
    flyleaf - ID форзаца фотокниги
    flyleaf_material - ID материала форзаца фотокниги
    cuts - ID обреза фотокниги
    cases - ID футляра фотокниги
    design - ID дизайна фотокниги
    supercovers - ID суперобложки
    count - Количество фотокниг
    pages - Количество страниц в фотокниге
    user_id - ID зарегистрированного пользователя
     *
     * @link https://bitbucket.org/FotisRF/fotisopendoc/wiki/API/Photobooks/Projects
     *
     * @param array $params
     *
     * @return k_photobook_api_project
     */
    public function addProject($params)
    {
        $p['access_token'] = $this->getToken();
        Log::_log("Создаем новый проект k_photobook_api::addProject", 'photobook');
        $p['book_types'] = $params['type_id'];
        $p['formats'] = $params['format_id'];
        $p['group_id'] = $params['group_id'];

        $response = $this->call("/api/photobooks/projects/add", $p);

        $project = new k_photobook_api_project();
        $project->fromJson($response->project);
        $project->is_public = 1;
        $this->updateProject($project, ['public' => 1]);

        return $project;
    }

    public function updateProject(k_photobook_api_project $project, $props)
    {
        Log::_log("Обновляем проект", 'photobook');
        $props['access_token'] = $this->getToken();
        //$params['access_token'] = $this->getToken();
        $this->call("/api/photobooks/projects/update/" . $project->getId(), $props);
    }
}

class k_photobook_api_response {

    /**
     * @var array
     */
    private $response = null;

    public function __construct($json)
    {
        $this->response = json_decode($json);

        if ($this->response === null) {
            throw new Exception('Невалидный ответ от API');
        }

        if (isset($this->response->error)) {
            throw new Exception($this->response->error->descritpion);
        }
    }


    public function getResponse()
    {
        return $this->response;
    }


}
