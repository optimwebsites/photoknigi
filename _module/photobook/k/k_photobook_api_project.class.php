<?php
/**
 * Created by PhpStorm.
 * User: nikolay
 * Date: 11.02.16
 * Time: 9:30
 */

class k_photobook_api_project {

    public $id;

    public $hash;

    public $name;

    public $company_id;

    public $user_id;

    public $user_hash;

    public $template_group_id;

    public $type_id;

    public $format_id;

    public $calculator;

    public $is_editable;

    public $is_public;

    public $update_date;

    public $description;

    public $constructor;

    public $company_name;

    public $book_type_name;

    public $book_type_hardcover;

    public $book_type_print;

    public $count;

    public $format_name;

    public $image;

    public $created_date;

    /**
     * @param $json
     * @throws Exception
     */
    public function fromJson($response)
    {
        $this->setAttributes($response);

        return $this;
    }

    /**
     * @param $attrs
     * @throws Exception
     */
    public function setAttributes($attrs) {
        foreach (get_object_vars($attrs) as $k => $v) {
            if (property_exists($this, $k)) {
                $this->$k = $v;
            } else {
                throw new Exception("Не известное свойство {$k}");
            }
        }
    }

    public function getId()
    {
        if (empty($this->id) || empty($this->hash)) {
            return null;
        }

        return $this->id."_".$this->hash;
    }
}