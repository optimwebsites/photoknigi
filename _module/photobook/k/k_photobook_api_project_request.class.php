<?php
/**
 * Created by PhpStorm.
 * User: nikolay
 * Date: 11.02.16
 * Time: 9:30
 */

class k_photobook_api_project_request {

    /**
     * ID типа фотокниг
     * @var int
     */
    public $book_types;

    /**
     * ID формата фотокниги
     * @var int
     */
    public $formats;

    /**
     *  - название проекта, по умолчанию берется текущая дата в формате d.m.Y H:i.
     * @var string
     */
    public $name;

    /**
     *  - ID шаблона фотокниг
     * @var int
     */
    public $group_id;

    /**
     *  - ID обложки фотокниги
     * @var int
     */
    public $cover;

    /**
     *  - ID материала обложки фотокниги
     * @var int
     */
    public $cover_material;

    /**
     *  - ID декора фотокниги
     * @var int
     */
    public $decor;

    /**
     *  - ID варианта размещения декора фотокниги
     * @var
     */
    public $decor_locations;

    /**
     *  - ID варианта нанесения декора фотокниги
     * @var
     */
    public $decor_forms;

    /**
     *  - ID бумаги фотокниги
     * @var int
     */
    public $paper;

    /**
     *  - ID форзаца фотокниги
     * @var
     */
    public $flyleaf;

    /**
     *  - ID материала форзаца фотокниги
     *
     * @var
     */
    public $flyleaf_material;

    /**
     *  - ID обреза фотокниги
     * @var
     */
    public $cuts;

    /**
     *  - ID футляра фотокниги
     * @var
     */
    public $cases;

    /**
     *  - ID дизайна фотокниги
     * @var
     */
    public $design;

    /**
     *  - ID суперобложки
     * @var
     */
    public $supercovers;

    /**
     *  - Количество фотокниг
     * @var
     */
    public $count;

    /**
     *  - Количество страниц в фотокниге
     * @var
     */
    public $pages;

    /**
     *  - ID зарегистрированного пользователя
     * @var
     */
    public $user_id;


    public function setAttributes($attrs) {
        foreach ($attrs as $k => $v) {
            if (property_exists($this, $k)) {
                $this->$k = $v;
            } else {
                throw new Exception("Не известное свойство {$k}");
            }
        }
    }

    public function getRequest()
    {
        return [
            'book_types' => $this->book_types,
            'formats' => $this->formats,
            'user_id' => $this->user_id,
            'name' => $this->name,
            'group_id' => $this->group_id,
        ];
    }

}