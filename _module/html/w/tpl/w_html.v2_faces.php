<div class="content-block">
	<div class="title">
		<h2>Мы делаем фотокниги для Вас</h2>
	</div>
	<div class="row faces">
		<div class="col-xs-6 col-sm-3">
			<img src="/_site/i/v2/f2.jpg" />
			<p class="h4">Дунаев Николай</p>
			<p class="faces_position">руководитель <br><?=$name = Yii::app()->site->getName();?></p>
		</div>
		<div class="col-xs-6 col-sm-3">
			<img src="/_site/i/v2/f1.jpg" />
			<p class="h4">Дашевская Анна</p>
			<p class="faces_position">руководитель отдела <br>по работе с клиентами</p>
		</div>
		<div class="col-xs-6 col-sm-3">
			<img src="/_site/i/v2/f4.jpg" />
			<p class="h4">Елена Кудинова</p>
			<p class="faces_position">руководитель отдела <br>дизайна</p>
		</div>
		<div class="col-xs-6 col-sm-3">
			<img src="/_site/i/v2/f3.jpg" />
			<p class="h4">Иванов Петр</p>
			<p class="faces_position">руководитель отдела <br>печати</p>
		</div>
	</div>
</div>