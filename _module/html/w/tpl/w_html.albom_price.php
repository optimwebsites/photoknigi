<div class="container bottom-padding-mini" id="price-panel">
    <div class="row">
        <div class="col-sm-12 col-md-12">
            <div class="title-box text-center">
                <div class="h1 title">Наши цены</div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-4 text-center bottom-padding-mini">
            <h3>Выпускной альбом - "бабочка"</h3>
            <img src="/_site/i/vip/a-but.jpg" class="bottom-padding-mini"/>
            <p>«Бабочка» – промежуточный вариант, совмещающий качество и долговечность сборки с проклейкой картоном,
                а также демократичную цену, обусловленную цифровой печатью.
                Бумага 160 гр./м.кв., ламинация, проклейка картоном, разворот на 180 градусов. Минимальный объем - 5 разворотов</p>
            <button class="btn btn-warning btn-lg" data-toggle="modal" data-target="#orderBook">Оставить заявку</button>
        </div>
        <div class="col-sm-12 col-md-8">
            <?php
            $price = new w_photobook_price();
            $price->type = m_photobook::TYPE_VIPUSK_BUTTERFLY;
            $price->format = '21x30';
            $price->tpl = 'vipusk';
            echo $price->getHtml();
            ?>
            <div class="row">
                <div class="col-sm-8">
                    <div class="h6  text-danger">Примерная стоимость дизайна<small><br/>6 страниц / 3 разворота, класс 22 человека, 10 преподавателей, персональная фотообложка</small></div>
                </div>
                <div class="col-sm-4">
                    <div class="h2 text-danger">2 900 руб</div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <hr class="shadow" />
    </div>

    <div class="row">
        <div class="col-sm-12 col-md-4 text-center bottom-padding-mini">
            <h3>Выпускной альбом СТАНДАРТ</h3>
            <img src="/_site/i/vip/a-standart.jpg" class="bottom-padding-mini"/>
            <p>Печать цифровая. Предлагаем следующие варианты выполнения внутреннего блока: из бумаги плотностью 200 гр./м.кв.;
                из бумаги плотностью 200 гр./м.кв. + ламинация; из бумаги плотностью 200 гр./м.кв. + ламинация +
                разворот на 180 градусов. Минимальный объем - 14 страниц</p>
            <button class="btn btn-warning btn-lg" data-toggle="modal" data-target="#orderBook">Оставить заявку</button>
        </div>
        <div class="col-sm-12 col-md-8 bottom-padding-mini">
            <?php
            $price = new w_photobook_price();
            $price->type = m_photobook::TYPE_VIPUSK_STANDART;
            $price->format = '21x30';
            $price->tpl = 'vipusk';
            echo $price->getHtml();
            ?>
            <div class="row">
                <div class="col-sm-8">
                    <div class="h6 text-danger">Стоимость дизайна <br><small>14 страниц, класс 22 человека, <br/>10 преподавателей, персональная фотообложка</small></div>
                </div>
                <div class="col-sm-4">
                    <div class="h2 text-danger">3 915 руб</div>
                </div>
            </div>

        </div>
        <div class="clearfix"></div>
        <hr class="shadow" />
    </div>
    <div class="row">
        <div class="col-sm-12 col-md-4 text-center bottom-padding-mini">
            <h3>Выпускной альбом ПРЕМИУМ</h3>
            <img src="/_site/i/vip/a-premium.jpg" class="bottom-padding-mini"/>
            <p>Выпускной фотоальбом премиум-класса. Фотопечать.
                Печать на фотобумаге, проклейка пластиком, разворот на 180 градусов.
                Яркий, долговечный, запоминающийся фотоальбом. минимальный объем - 2 разворота</p>
            <button class="btn btn-warning btn-lg" data-toggle="modal" data-target="#orderBook">Оставить заявку</button>
        </div>
        <div class="col-sm-12 col-md-8 bottom-padding-mini">
            <?php
            $price = new w_photobook_price();
            $price->type = m_photobook::TYPE_VIPUSK_PREMIUM;
            $price->format = '21x30';
            $price->tpl = 'vipusk';
            echo $price->getHtml();
            ?>
            <div class="row">
                <div class="col-sm-8">
                    <div class="h6  text-danger">Примерная стоимость дизайна<small><br/>6 страниц / 3 разворота, класс 22 человека, 10 преподавателей, персональная фотообложка</small></div>
                </div>
                <div class="col-sm-4">
                    <div class="h2 text-danger">2 900 руб</div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <hr class="shadow" />
    </div>

</div>