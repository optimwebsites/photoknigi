<div class="full-width-box  bottom-padding-mini service-desk text-white" id="photobooks">
    <div class="fwb-bg fwb-paralax band-6" data-speed="2"></div>

    <div class="container" id="how-to-panel">

        <div class="row services">
            <div class="col-sm-12 col-md-12">
                <div class="title-box text-center">
                    <h1 class="title text-white">Мы не возьмем деньги, если наша работа Вам не
                        понравится</h1>
                </div>
            </div>
            <div class="col-sm-12 col-md-12">
                <p>Вы оплачиваете нашу работу только после согласования полностью готового к печати макета
                    выпускного фотоальбома. Если Вам не понравится наша работа, мы не возьмем за нее оплату. </p><br/>
            </div>


            <div class="col-sm-3 col-md-3 service">
                <div class="icon bg"><i class="fa">1</i></div>
                <h6 class="title">Заявка</h6>

                <div class="text-small"> <button class="btn btn-warning"  data-toggle="modal" data-target="#orderBook">Оставьте заявку</button><br/>
                    Мы свяжемся с Вами в течение нескольких минут и предложим каталог шаблонов выпускных альбомов</div>

            </div>
            <div class="col-sm-3 col-md-3 service">
                <div class="icon bg"><i class="fa">2</i></div>
                <h6 class="title">Шаблон и макет</h6>

                <div class="text-small">После выбора шаблона мы подробно обговорим все детали макета.
                    <br/>
                    В течение 5-7 дней после получения фото мы сделаем макет и согласуем его с Вами.
                </div>

            </div>
            <div class="col-sm-3 col-md-3 service">
                <div class="icon bg"><i class="fa">3</i></div>
                <h6 class="title">Оплата</h6>

                <div class="text-small">Вы оплачиваете изготовление фотоальбомов.
                    При необходимости мы напечатаем и доставим сигнальный экземпляр.</div>

            </div>
            <div class="col-sm-3 col-md-3 service">
                <div class="icon bg"><i class="fa">4</i></div>
                <h6 class="title">Доставка</h6>

                <div class="text-small">Мы доставим тираж по всей России от 1 до 7 дней в зависимости от выбранного способа доставки.</div>

            </div>
        </div>


    </div>
</div>