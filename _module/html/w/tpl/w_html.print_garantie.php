<div class="full-width-box  bottom-padding-mini service-desk text-white">
    <div class="fwb-bg fwb-paralax band-6" data-speed="2"></div>

    <div class="container" id="how-to-panel">

        <div class="row services">
            <div class="col-sm-12 col-md-12">
                <div class="title-box text-center">
                    <h1 class="title text-white">Вы фотограф?</h1>
                </div>
            </div>
            <div class="col-sm-12 col-md-12 text-center">
                <p>Для фотографов мы предлагаем специальные цены на печать фотокниг, а также печать первой фотокниги совершенно бесплатно.
                    <br/>Убедитесь в качестве нашей продукции!
                </p><br/>
                <a href="" class="btn btn-warning" data-toggle="modal" data-target="#orderBook">Оставьте заявку</a>
            </div>
        </div>
    </div>
</div>