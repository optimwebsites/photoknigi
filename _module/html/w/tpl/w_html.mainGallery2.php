<div class="bottom-padding-mini frame-shadow-lifted">
<div class="slider rs-slider load">
    <div class="tp-banner-container">
        <div class="tp-banner">
            <ul>
                <li data-delay="7000" data-transition="fade" data-slotamount="7"
                    data-masterspeed="2000">
                    <div class="elements">
                        <h1 class="tp-caption lft skewtotop title c-bg-caption"
                            data-x="30"
                            data-y="31"
                            data-speed="1000"
                            data-start="700"
                            data-easing="Power4.easeOut"
                            data-endspeed="500"
                            data-endeasing="Power1.easeIn">
                            <span>Самые лучшие фотокниги</span>
                        </h1>

                        <h2 class="tp-caption lft skewtotop title c-bg-caption c-bg-caption-light"
                            data-x="30"
                            data-y="111"
                            data-speed="1000"
                            data-start="1000"
                            data-easing="Power4.easeOut"
                            data-endspeed="500"
                            data-endeasing="Power1.easeIn">
                            <span class="text-danger">для самых ценных моментов</span>
                        </h2>


                        <button class="tp-caption lft skewtotop btn btn-lg btn-warning"
                                data-toggle="modal" data-target="#orderBook"
                                data-x="30"
                                data-y="421"
                                data-speed="1000"
                                data-start="1500"
                                data-easing="Power4.easeOut"
                                data-endspeed="500"
                                data-endeasing="Power1.easeIn">
                            Заказать фотокнигу</span>
                        </button>

                        <!--<a href="/fotoknigi/konstruktor_fotoknig/" class="tp-caption lft skewtotop btn btn-lg btn-warning"
                                data-x="280"
                                data-y="421"
                                data-speed="1000"
                                data-start="1500"
                                data-easing="Power4.easeOut"
                                data-endspeed="500"
                                data-endeasing="Power1.easeIn">
                            Конструктор фотокниг</span>
                        </a>-->

                        <div class="tp-caption lft skewtotop h4 text-white c-bg-caption"
                             data-x="950"
                             data-y="51"
                             data-speed="1000"
                             data-start="300"
                             data-easing="Power4.easeOut"
                             data-endspeed="1500"
                             data-endeasing="Power1.easeIn">
                            <span>Готовый макет</span>
                        </div>

                        <div class="tp-caption lft skewtotop h4 c-bg-caption c-bg-caption-light"
                             data-x="980"
                             data-y="111"
                             data-speed="1000"
                             data-start="800"
                             data-easing="Power4.easeOut"
                             data-endspeed="800"
                             data-endeasing="Power1.easeIn">
                            <span class="text-danger">через 1 день</span>
                        </div>

                        <div class="tp-caption lft skewtotop h4 text-white c-bg-caption"
                            data-x="890"
                            data-y="351"
                            data-speed="1000"
                            data-start="300"
                            data-easing="Power4.easeOut"
                            data-endspeed="1500"
                            data-endeasing="Power1.easeIn">
                            <span>Доставка по России</span>
                        </div>

                        <div class="tp-caption lft skewtotop h4 c-bg-caption c-bg-caption-light"
                            data-x="1050"
                            data-y="411"
                            data-speed="1000"
                            data-start="800"
                            data-easing="Power4.easeOut"
                            data-endspeed="800"
                            data-endeasing="Power1.easeIn">
                            <span class="text-danger">340 руб</span>
                        </div>


                    </div>

                    <img src="/_site/i/3/fb_1.jpg" alt=""
                         data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat">
                </li>


            </ul>
            <div class="tp-bannertimer"></div>
        </div>
    </div>
</div>
    </div>