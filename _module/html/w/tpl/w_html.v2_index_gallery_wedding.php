<div id="gallery_wedding">
	<div class="row content-block">
		<div class="col-xs-12">
			<h2>Свадебные фотокниги</h2>
			<p>
				Первая и самая важная фотокнига для молодой семьи, чтобы праздник не заканчивался никогда. Настоящая семейная драгоценность может быть выполнена в любом стиле. Также можно заказать вместе со свадебной фотокнигой минибуки для родных и близких. Поделитесь своей радостью!
			</p>
			<button class="btn btn-danger scroll-to" data-target="#order-form">Заказать фотокнигу</button>
		</div>
	</div>
	<div class="row igallery">
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/wedding/w1.jpg">
				<img src="/_site/i/v2/wedding/w1.jpg"
					 data-title="<div class='igallery-caption'>Тканевая обложка, вышивка, отстрочка по периметру. Цена 30x21 - <span class='igallery-price'>3114</span> <span class='igallery-price-rub'>р.</span></div>"
					 title="<div class='igallery-caption'>Тканевая обложка, вышивка, отстрочка по периметру. Цена 30x21 - <span class='igallery-price'>3114</span> <span class='igallery-price-rub'>р.</span></div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/wedding/w2.jpg">
				<img src="/_site/i/v2/wedding/w2.jpg"
					 data-title="<div class='igallery-caption'>Комбинированная экокожа, фотовставка, персональная шильда. Цена 30x30 - <span class='igallery-price'>6879</span> <span class='igallery-price-rub'>р.</span></div>"
					 title="<div class='igallery-caption'>Комбинированная экокожа, фотовставка, персональная шильда. Цена 30x30 - <span class='igallery-price'>6879</span> <span class='igallery-price-rub'>р.</span></div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/wedding/w3.jpg">
				<img src="/_site/i/v2/wedding/w3.jpg"
					 data-title="<div class='igallery-caption'>Экокожа, фотовставка, стандартная шильда, уголки, отстрочка. Цена 30x30 - <span class='igallery-price'>6663</span> <span class='igallery-price-rub'>р.</span></div>"
					 title="<div class='igallery-caption'>Экокожа, фотовставка, стандартная шильда, уголки, отстрочка. Цена 30x30 - <span class='igallery-price'>6663</span> <span class='igallery-price-rub'>р.</span></div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/wedding/w4.jpg">
				<img src="/_site/i/v2/wedding/w4.jpg"
					 data-title="<div class='igallery-caption'>Персональная фотообложка, цифровая печать. Цена 21x30 - <span class='igallery-price'>975</span> <span class='igallery-price-rub'>р.</span></div>"
					 title="<div class='igallery-caption'>Персональная фотообложка, цифровая печать. Цена 21x30 - <span class='igallery-price'>975</span> <span class='igallery-price-rub'>р.</span></div>"
					/>
			</a>
		</div>
	</div>
</div>
