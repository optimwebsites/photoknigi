<div class="col-xs-offset-0 col-sm-offset-2 col-md-offset-1 h1-container">
	<div class="clearfix"></div>
	<h1>Фотокниги на заказ</h1>
	<ul class="menu-level2">
		<li><a href="#gallery_wedding" class="scroll-to" data-target="#gallery_wedding">Свадебные фотокниги</a></li>
		<li><a href="#gallery_family" class="scroll-to" data-target="#gallery_family">Семейные фотокниги</a></li>
		<li><a href="#gallery_child" class="scroll-to" data-target="#gallery_child">Детские фотокниги</a></li>
		<li><a href="#gallery_travel" class="scroll-to" data-target="#gallery_travel">Фотокниги путешествий</a></li>
		<li><a href="#gallery_lovestory" class="scroll-to" data-target="#gallery_lovestory">Love Story</a></li>
	</ul>
	<div class="clearfix"></div>
	<ul class="simple-list" id="head-list">
		<li class="text-white hide"><span class="h2 text-white">Индивидуальный дизайн</span><br>
			<span class="h3 text-white">&nbsp; &nbsp; + высококачественная печать</span><br/>
			&nbsp; &nbsp; <span class="text-red">от</span> <span class="text-red h1">2300</span> <small class="text-red">руб</small>
		</li>
		<li class="hide"><span class="h2 text-white">Доставка по России </span><br/>
			&nbsp; &nbsp; &nbsp; <span class="text-red h1">340</span> <span class="text-red">руб</span></li>
	</ul>
	<style>
		.main-content {
			margin-top:300px;
			opacity:0;
		}
	</style>
</div>