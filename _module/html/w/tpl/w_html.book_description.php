<div class="container">
    <div class="row text-center">
        <div class="col-sm-12">
            <div class="title-box text-center">
                <div class="h1 title">Фотокнига Премиум и Стандарт</div>
            </div>
        </div>
        <div class="col-sm-12 text-center">
            <img src="/_site/i/3/fb4.jpg" />
        </div>
        <div class="row our-services">
            <div class="col-sm-6 text-center bottom-padding-mini">

                <div class="h5">Фотокнига Премиум</div>
                <p>Фотокнига ПРЕМИУМ поражает своей красотой. Она идеально подходит для свадебного фотоальбома, детской или семейной фотокниги.
                    Разворот этой фотокниги - целая фотография, напечатанная на фотобумаге премиум качества.
                    И красота ее не померкнет через десятки лет.  </p>
            </div>

            <div class="col-sm-6 bottom-padding-mini">
                <div class="h5">Фотокнига Стандарт</div>
                <p>Более бюджетный вариант, отпечатанный типографским способом на качественной мелованной бумаге высокой плотности.
                    Такая фотокнига идеально подойдет для выпускных альбомов, каталогов продукции, эффектных рекламных материалов или книг с кулинарными рецептами.</p>
            </div>
        </div>
    </div>
    <?php
    $w_description = new w_html();
    $w_description->tpl = "book_description_tools";
    echo $w_description->getHtml();
    ?>
</div>