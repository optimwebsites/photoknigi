<div class="container" id="aCalculator">
    <div class="row">
        <div class="col-sm-6 bottom-padding-mini">
            <!--<div class="title-box text-center">
                <h2 class="title ">Калькулятор</h2>
            </div>
            <div class="text-center">
                <form class="form-horizontal form-box register-form contact-form" role="form">
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Способ печати</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="cType">
                                <option value="photo">Фотопечать</option>
                                <option value="poly">Полиграфическая печать</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-4 control-label">Формат</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="cFormat">
                                <option value="x1521">15x21</option>
                                <option value="x2130">21x30</option>
                                <option value="x3021">30x21</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Макет</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="cDesign">
                                <option value="0">Только печать. Макет уже есть</option>
                                <option value="1">Дизайн + печать</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group hide" id="childFotoRow">
                        <label class="col-sm-4 control-label">Фотографий учеников</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="cNumChilds" placeholder="22" value="22">
                        </div>
                    </div>
                    <div class="form-group hide" id="teacherFotoRow">
                        <label class="col-sm-4 control-label">Фотографий преподавателей</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="cNumTeachers" placeholder="10" value="10">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label" id="cPageTilte">Количество разворотов</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="cNumPage" placeholder="1" value="2">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Количество экземпляров</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="cNumBook" placeholder="1" value="1">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-4 control-label">Доставка</label>
                        <div class="col-sm-8">
                            <select class="form-control" id="cDelivery">
                                <option value="340">Почтой России (5-7 дней по России, 1-2 дня по Москве)</option>
                                <option value="890">Срочная (2-3 дня по России)</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-6">
                            <div class="text-right">
                                Печать:
                            <span class="text-primary">
                                <b>
                                    <span id="print-cost"></span>
                                </b>
                            </span> руб.
                            </div>
                            <div class="text-right">
                                Изготовление макета:
                            <span class="text-primary">
                                <b>
                                    <span id="design-cost"></span>
                                </b>
                            </span> руб.
                            </div>
                            <div class="text-right">Доставка по России:
                            <span class="text-primary">
                                <b>
                                    <span id="delivery-cost"></span>
                                </b>
                            </span> руб.
                            </div>
                            <div class="text-right">
                                Итого:
                            <span class="text-danger">
                                <b>
                                    <span id="total-cost"></span>
                                </b>
                            </span> руб.
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <button type="button" class="btn btn-warning btn-lg" style="margin-top:20px;"
                                    id="order-book-btn" data-toggle="modal" data-target="#orderBook">Заказать</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-xs-12 col-sm-12">
                            <small>Расчет цены ориентировочный. Для получения окончательной стоимости заказа обращайтесь к менеджерам.</small>
                        </div>
                    </div>
                </form>
                <script type="text/javascript">

                    $("#calc-btn").click(function() {
                        albumCalculator();
                    })

                    $("#cDesign").change(function() {
                        if ($(this).val() == 0) {
                            $("#childFotoRow, #teacherFotoRow").addClass("hide");
                        } else {
                            $("#childFotoRow, #teacherFotoRow").removeClass("hide");
                        }

                        albumCalculator();
                    });

                    $("#cType, #cFormat, #cDesign, #cDelivery").change(
                        function() {
                            albumCalculator();
                        }
                    );

                    $("#cNumPage, #cNumBook, #cNumTeachers, #cNumChilds").keyup(
                        function() {
                            setTimeout(function() { albumCalculator();}, 500);
                        }
                    );

                    function albumCalculator()
                    {
                        var cType = $("#cType").val();
                        var cDesign = $("#cDesign").val();
                        var cFormat = $("#cFormat").val();
                        var cNumChilds = $("#cNumChilds").val();
                        var cNumTeachers = $("#cNumTeachers").val();
                        var cNumPage = $("#cNumPage").val();
                        var cNumBook = $("#cNumBook").val();
                        var cDelivery = $("#cDelivery").val();

                        var price = {photo: {x1521:75, x2130:119, x3021:119}, poly: {x1521:14, x2130:23, x3021:23   }};
                        var photoPrice = {child: 100, teacher:70};
                        var deliveryPrice = {simple: 340, fast: 890};
                        var defaultPageNum = {photo:2, poly:7};

                        if (cType=="poly") {
                            if (cNumPage < 14) {
                                cNumPage = 14;
                                $("#cNumPage").val(cNumPage);
                                $("#cPageTilte").text("Количество страниц");
                            }
                        } else {
                            $("#cPageTilte").text("Количество разворотов");
                        }

                        var totalPrint   = (price[cType][cFormat] * cNumPage + 135) * cNumBook;
                        var totalDesign  = cDesign * (cNumChilds * photoPrice.child + cNumTeachers * photoPrice.teacher);
                        totalDesign += totalDesign * 0.05 * (cNumPage - defaultPageNum[cType]);

                        totalDesign = Math.round(totalDesign);
                        var deliveryCost = cDelivery;

                        $("#calc-result").removeClass('hide');
                        $("#print-cost").text(totalPrint);
                        $("#design-cost").text(totalDesign);
                        $("#total-cost").text(totalPrint - (-1 * totalDesign) - (-1 * deliveryCost));
                        $("#delivery-cost").text(deliveryCost);
                    }

                    albumCalculator();
                </script>
            </div>-->
            <div class="title-box text-center">
                <h2 class="title ">Доставка по всей России</h2>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <?php
                    $delivery = new w_html();
                    $delivery->tpl = "delivery";
                    echo $delivery->getHtml();
                    ?>
                </div>
            </div>
        </div>

        <div class="col-sm-6 col-md-6">

            <div class="title-box text-center">
                <h2 class="title ">Шаблоны</h2>
            </div>

            <?php
            $gallery = new w_gallery_slider();
            $gallery->setProperties([
                    'photoTpl' => "default",
                    'photo_width' => 600,
                    'photo_height' => 390,
                    "gallery" => [
                        'id_gallery' => 1,
                        'preview_image' => null,
                        'gallery_title' => 'Шаблоны',
                        'gallery_description' => '',
                        'rewrite_name' => 'galereya-1',

                    ],
                    "photos" => [
                        [
                            'id_gallery_photo' => 1,
                            'photo' => '/_site/i/albom/tpls/3.jpg',
                            'photo_title' => '',
                            'photo_text' => '',
                            'photo_type' => w_widget_media::TYPE_IMG,
                            "extAttr" => []
                        ],
                        [
                            'id_gallery_photo' => 2,
                            'photo' => '/_site/i/albom/tpls/2.jpg',
                            'photo_title' => '',
                            'photo_text' => '',
                            'photo_type' => w_widget_media::TYPE_IMG,
                            "extAttr" => []
                        ],
                        [
                            'id_gallery_photo' => 2,
                            'photo' => '/_site/i/albom/tpls/1.jpg',
                            'photo_title' => '',
                            'photo_text' => '',
                            'photo_type' => w_widget_media::TYPE_IMG,
                            "extAttr" => []
                        ],
                        [
                            'id_gallery_photo' => 2,
                            'photo' => '/_site/i/albom/tpls/4.jpg',
                            'photo_title' => '',
                            'photo_text' => '',
                            'photo_type' => w_widget_media::TYPE_IMG,
                            "extAttr" => []
                        ],
                    ]]
            );
            echo $gallery->getHtml();

            ?>

            <div>
                <br/>
                <h5 class="text-center">Ищете шаблон для выпускного фотоальбома?</h5>

                <p class="text-center">
                    <button class="btn btn-warning btn-large"  data-toggle="modal" data-target="#orderBook">Оставьте заявку</button>
                </p>
                <p class="text-center">Мы пришлем каталог шаблонов.
                    Вы выберете тот, который подходит больше всего.
                    В течение 5-7 дней мы сделаем оригинал-макет с Вашими фотографиями и пришлем его на согласование.
                </p>
            </div>
        </div>
    </div>
</div>
