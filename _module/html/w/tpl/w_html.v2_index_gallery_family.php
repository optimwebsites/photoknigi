<div id="gallery_family">
	<div class="row content-block">
		<div class="col-xs-12">
			<h2>Семейные фотокниги</h2>
			<p>
				Юбилеи, дни рождения, памятные встречи и события – есть множество моментов, которые хочется запечатлеть внутри семьи. Как правило, такие фотокниги печатаются в 2-3 экземплярах, поэтому на них действует специальное предложение со скидкой.
			</p>
			<button class="btn btn-danger scroll-to" data-target="#order-form">Заказать фотокнигу</button>
		</div>
	</div>
	<div class="row igallery">
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/family/1.jpg">
				<img src="/_site/i/v2/family/1.jpg"
					 data-title="<div class='igallery-caption'>Тканевая обложка, фотовставка, уголки. Цена 25x25 - <span class='igallery-price'>2702</span> <span class='igallery-price-rub'>р.</span></div>"

					 title="<div class='igallery-caption'>Тканевая обложка, фотовставка, уголки. Цена 25x25 - <span class='igallery-price'>2702</span> <span class='igallery-price-rub'>р.</span></div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/family/2.jpg">
				<img src="/_site/i/v2/family/2.jpg"
					 data-title="<div class='igallery-caption'>Экокожа, фотовставка, тиснение серебром, прострочка. Цена 25x25 - <span class='igallery-price'>4872</span> <span class='igallery-price-rub'>р.</span></div>"
					 title="<div class='igallery-caption'>Экокожа, фотовставка, тиснение серебром, прострочка. Цена 25x25 - <span class='igallery-price'>4872</span> <span class='igallery-price-rub'>р.</span></div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/family/3.jpg">
				<img src="/_site/i/v2/family/3.jpg"
					 data-title="<div class='igallery-caption'>Персональная фотообложка, цифровая печать. Цена 30x30 - <span class='igallery-price'>1483</span> <span class='igallery-price-rub'>р.</span></div>"
					 title="<div class='igallery-caption'>Персональная фотообложка, цифровая печать. Цена 30x30 - <span class='igallery-price'>1483</span> <span class='igallery-price-rub'>р.</span></div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/family/4.jpg">
				<img src="/_site/i/v2/family/4.jpg"
					 data-title="<div class='igallery-caption'>Персональная фотообложка, фотопечать. Цена 30x30 - <span class='igallery-price'>2514</span> <span class='igallery-price-rub'>р.</span></div>"
					 title="<div class='igallery-caption'>Персональная фотообложка, фотопечать. Цена 30x30 - <span class='igallery-price'>2514</span> <span class='igallery-price-rub'>р.</span></div>"
					/>
			</a>
		</div>
	</div>
</div>
