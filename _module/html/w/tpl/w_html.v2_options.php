<div>
	<div class="title content-block">
		<h2>Оформление фотокниги</h2>
		<div class="row">
			<div class="col-sm-6">
				<ul class="simple-list">
					<li>Обложки из <span class="h3 text-red">58</span> видов кожзама (гладкие и текстурированные)</li>
					<li><span class="h3 text-red">7</span> видов тканей для обложки</li>
					<li><span class="h3 text-red">9</span> видов бумвинила и <span class="h3 text-red">3</span> варианта полимера с тканевой структурой для обложки</li>
					<li><span class="h3 text-red">16</span> вариантов клише для тиснения, возможность изготовления персонального</li>
					<li><span class="h3 text-red">4</span> вида фотобумаги: глянцевая, матовая, с текстурой «шелк» и «металлик»</li>
					<li>вышивка на обложке текста, вензеля, памятных дат</li>
					<li><span class="h3 text-red">8</span> стандартных шильд и возможность изготовления персональных</li>
					<li><span class="h3 text-red">21</span> вид дизайнерской бумаги для изготовления форзаца</li>
				</ul>
			</div>
			<div class="col-sm-6">
				<ul class="simple-list">
					<li>от <span class="h3 text-red">5</span> до <span class="h3 text-red">35</span> разворотов в фотокниге</li>
					<li><span class="h3 text-red">10</span> типоразмеров (от 20х20 до 40х30)</li>
					<li>мини-версии фотокниг (минибуки) размером 15х15 см</li>
					<li>боксы для хранения фотокниг из картона, ткани, кожзама</li>
					<li>блинтовое тиснение, припрессовка серебряной или золотой фольгой</li>
					<li><span class="h3 text-red">4</span> способа отделки среза: белый, черный, золото или серебро</li>
					<li>обложки с фотопечатью и защитным глянцевым покрытием</li>
					<li>фотокниги с отстрочкой обложки по периметру и без</li>
					<li>… и бесконечное множество вариантов комбинации этих параметров!</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="row igallery">
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/options/1.jpg">
				<img src="/_site/i/v2/options/1.jpg"
					 data-title="<div class='igallery-caption'>Вышивка на обложках, отстрочка по периметру</div>"
					 title="<div class='igallery-caption'>Вышивка на обложках, отстрочка по периметру</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/options/2.jpg">
				<img src="/_site/i/v2/options/2.jpg"
					data-title="<div class='igallery-caption'>Серебрение, золочение и чернение срезов</div>"
					 title="<div class='igallery-caption'>Серебрение, золочение и чернение срезов</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/options/3.jpg">
				<img src="/_site/i/v2/options/3.jpg"
					 data-title="<div class='igallery-caption'>Проклейка пластиком, разворот 180 градусов, фотовставки</div>"
					 title="<div class='igallery-caption'>Проклейка пластиком, разворот 180 градусов, фотовставки</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/options/4.jpg">
				<img src="/_site/i/v2/options/4.jpg"
					 data-title="<div class='igallery-caption'>Боксы, обложка из ткани, отделка уголками</div>"
					 title="<div class='igallery-caption'>Боксы, обложка из ткани, отделка уголками</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/options/5.jpg">
				<img src="/_site/i/v2/options/5.jpg"
					 data-title="<div class='igallery-caption'>Минибуки, обложки из экокожи</div>"
					 title="<div class='igallery-caption'>Минибуки, обложки из экокожи</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/options/6.jpg">
				<img src="/_site/i/v2/options/6.jpg"
					 data-title="<div class='igallery-caption'>Комбинированные обложки, сумки для фотокниг</div>"
					 title="<div class='igallery-caption'>Комбинированные обложки, сумки для фотокниг</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/options/7.jpg">
				<img src="/_site/i/v2/options/7.jpg"
					 data-title="<div class='igallery-caption'>Шильды с надписями, обложки из дизайнерской ткани</div>"
					 title="<div class='igallery-caption'>Шильды с надписями, обложки из дизайнерской ткани</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/options/8.jpg">
				<img src="/_site/i/v2/options/8.jpg"
					 data-title="<div class='igallery-caption'>Рельефные вставки, рельефный корешок, просточка обложки</div>"
					 title="<div class='igallery-caption'>Рельефные вставки, рельефный корешок, просточка обложки</div>"
					/>
			</a>
		</div>
	</div>
</div>
