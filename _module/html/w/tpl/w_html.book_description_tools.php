<div class="row text-center bottom-padding-mini">
    <div class="col-sm-4">
        <div class="title-box text-center no-margin no-padding">
            <div class="h3 title">Обложки</div>
        </div>
        <img src="/_site/i/3/fb3.jpg" />
        <p><br><br>Мы делаем самые разнообразные обложки для наших фотокниг: глянцевые фотообложки, обложки из кожи, ткани, с тиснением бронзой, с металлическими уголками.</p>
    </div>
    <div class="col-sm-4">
        <div class="title-box text-center  no-margin no-padding">
            <div class="h3 title">Проклейка пластиком</div>
        </div>
        <img src="/_site/i/3/f6.jpg" />
        <p><br><br>Между листами фотобумаги можно вклеить пластиковые вставки. Это сделает страницы Вашей фотокниги более плотными. долговечными и эффектными.</p>
    </div>
    <div class="col-sm-4">
        <div class="title-box text-center  no-margin no-padding">
            <div class="h3 title">Минибуки</div>
        </div>
        <img src="/_site/i/3/f5.jpg" />
        <p><br/><br>Минибук - революция в мире фотокниг. Эффектная фотокнига, исполненная в миниатюре. Несмотря на размер, качественная фотобумага передает всю яркость и четкость изображений.</p>
    </div>
</div>
