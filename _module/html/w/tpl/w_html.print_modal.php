<div>
<div id="orderBook" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть">
                    <span aria-hidden="true">X</span>
                </button>
                <div id="saveCallbackForm_container" class="show">
                    <form class="register-form contact-form" method="post" action="/_j/callback/save/s/" id="saveCallbackForm">
                        <input type="hidden" class="form-control" name="m_callback[msg]"/>
                        <br/><h6 id="register-form-title" class="text-center">
                            Мы свяжемся с Вами в течение 15 минут
                        </h6>

                        <div class="form-group">
                            <label>Ваше имя:</label>
                            <input type="text" class="form-control" name="m_callback[name]"/>
                        </div>
                        <div class="form-group">
                            <label>Телефон:</label>
                            <input type="text" class="form-control" name="m_callback[phone]"/>
                        </div>
                        <div class="form-group">
                            <label>E-mail:</label>
                            <input type="text" class="form-control" name="m_callback[email]"/>
                        </div>
                        <div class="clearfix"></div>
                        <div class="buttons-box clearfix">
                            <div class="alert hide" id="formMsg_saveCallbackForm"></div>
                            <button type="button" class="btn btn-warning form-submit" data-form="saveCallbackForm">
                                Отправить заявку
                            </button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    //подписываемся на событие успешного сабмита
    $(document).on('form_submit_success', function(e, data) {
        $("button.form-submit").hide();
    });

    var reachedGoals = {startPrintOrder:false, orderPrint:false, orderPrintButton:false};

    $('#orderBook').on('shown.bs.modal', function () {
        $(document).trigger("reachGoal", {"name": "orderPrintButton"});
    });

    $("[name='m_callback[name]']").click(function () {
        $(document).trigger("reachGoal", {name: "startPrintOrder"});
    });

    $("button.form-submit").click(function () {
        $(document).trigger("reachGoal", {name: "orderPrint"});
    });

    $(document).on("reachGoal", function (e, data) {
        if (window['yaCounter30888196'] && !reachedGoals[data.name]) {
            yaCounter30888196.reachGoal(data.name);
        }

        reachedGoals[data.name] = true;
    });
</script>
</div>
