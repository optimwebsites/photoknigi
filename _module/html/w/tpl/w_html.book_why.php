<div class="container">
    <div class="row text-center">
        <div class="col-sm-12">

            <div class="title-box text-center">
                <div class="h1 title">Почему фотокнигу нужно заказать у нас?</div>
            </div>
        </div>
        <div class="col-sm-12 text-center">
            <img src="/_site/i/3/fb2.jpg" />
        </div>
        <div class="clearfix"><br/></div>
        <div class="row our-services"  id="photobooks">
            <div class="col-sm-4 text-center bottom-padding-mini">

                <div class="h5">Индивидуальный дизайн</div>
                <p>Мы разработаем индивидуальный макет для Вашей фотокниги с учетом пожеланий и исходных данных,
                    внесем корректировки и согласуем с Вами.</p>
            </div>

            <div class="col-sm-4 bottom-padding-mini">
                <div class="h5">Подготовка макета</div>
                <p>Мы подготовим фотографии к печати: повысим или изменим яркость, контрастность, цветовую
                    насыщенность, приведем к единому стилю.</p>
            </div>


            <div class="col-sm-4 text-center bottom-padding-mini">

                <div class="h5">Высочайшее качество</div>
                <p>Напечатаем Вашу фотокнигу методом химической фотопечати на самом современном оборудовании. Качество бумаги, печати и сборки
                    гарантируем.</p>
            </div>
        </div>
    </div>
</div>