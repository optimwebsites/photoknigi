<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="title-box text-center">
            <div class="h1 title">У нас действительно низкие цены!</div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<div class="row">
    <div class="col-sm-12 bottom-padding-mini">
        <p>
            Большая клиентская база и большие объемы производства позволяют максимально уменьшить наценку,
            а взаимодействие с производителями расходных материалов, минуя посредников, позволяет минимизировать стоимость готовой продукции.
            В конечном итоге выигрываете вы, получая высококачественный продукт по низкой цене.
        </p>
    </div>
</div>
<?php
$discount = new w_html();
$discount->tpl = "discount_warning";
echo $discount->getHtml();
?>
<div class="row">
    <div class="col-md-12 text-center bottom-padding-mini">
        <div class="content-block frame-shadow-raised">
            <div class="row text-center book-price">
                <div class="col-sm-12">
                    <div class="h4">Фотокнига ПРЕМИУМ</div>
                </div>
            </div>
            <div class="row text-center book-price">
                <div class="col-sm-12">
                    <div class="h6 text-danger">Химическая фотопечать<br><small>(Минимальный объем - 2 разворота. Цены указаны для твердой персональной фотообложки)</small></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-condensed table-striped">
                        <thead>
                        <tr>
                            <th  class="text-center">Формат, см</th>
                            <th class="text-center">Фотокнига 2 разворта</th>
                            <th class="text-center">Цена доп. разворота</th>
                            <th class="text-center">Фотокнига 10 разворотов</th>
                            <!--<th>Скачать</th>-->
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        $books = new m_photobook();
                        foreach ($books->getFormats(m_photobook::TYPE_PHOTOBOOK_PREMIUM) as $format) {
                            $params = $books->getParams(m_photobook::TYPE_PHOTOBOOK_PREMIUM, $format);
                            ?>
                            <tr>
                                <td><?=$format?></td>
                                <td>
                                    <?=$books->getPrice(m_photobook::TYPE_PHOTOBOOK_PREMIUM, $format, 2, m_photobook::COVER_HARD, false, true)?>
                                    <!--<span class="bk-price-old">
                                        <?=$books->getPrice(m_photobook::TYPE_PHOTOBOOK_PREMIUM, $format, 2)?>
                                    </span>-->
                                </td>
                                <td><?=ceil($params['price_page'] * (1-$params['discount']/100))?>
                                    <!--<span class="bk-price-old">
                                        <?=$params['price_page']?>
                                    </span>-->
                                </td>
                                <td><?=$books->getPrice(m_photobook::TYPE_PHOTOBOOK_PREMIUM, $format, 10, m_photobook::COVER_HARD, false, true)?>
                                    <!--<span class="bk-price-old">
                                        <?=$books->getPrice(m_photobook::TYPE_PHOTOBOOK_PREMIUM, $format, 10)?>
                                    </span>-->
                                </td>
                                <!--<td><a href="">Photoshop (PSD)</a></td>-->
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center bottom-padding-mini">
                    <a href=""  class="btn btn-warning" data-toggle="modal" data-target="#orderBook">Заказать печать фотокниги</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 text-center bottom-padding-mini">
        <div class="content-block frame-shadow-raised">
            <div class="row text-center book-price">
                <div class="col-sm-12">
                    <div class="h4">Фотокнига СТАНДАРТ</div>
                </div>
            </div>
            <div class="row text-center book-price">
                <div class="col-sm-12">
                    <div class="h6 text-danger">Лазерная фотопечать <br><small>(Минимальный объем - 14 страниц. Цены указаны для твердой персональной фотообложки)</small></div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-condensed table-striped">
                        <thead>
                        <tr>
                            <th>Формат, см</th>
                            <th class="text-center">Фотокнига 14 страниц</th>
                            <th class="text-center">Цена доп. страницы</th>
                            <th class="text-center">Фотокнига 20 страниц</th>
                            <!--<th>Скачать</th>-->
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        $books = new m_photobook();
                        foreach ($books->getFormats(m_photobook::TYPE_PHOTOBOOK_STANDART) as $format) {
                            $params = $books->getParams(m_photobook::TYPE_PHOTOBOOK_STANDART, $format);
                            ?>
                            <tr>
                                <td><?=$format?></td>
                                <td>
                                    <?=$books->getPrice(m_photobook::TYPE_PHOTOBOOK_STANDART, $format, 2, m_photobook::COVER_HARD, false, true)?>
                                    <!--<span class="bk-price-old">
                                        <?=$books->getPrice(m_photobook::TYPE_PHOTOBOOK_STANDART, $format, 2)?>
                                    </span>-->
                                </td>
                                <td><?=ceil($params['price_page'] * (1-$params['discount']/100))?>
                                    <!--<span class="bk-price-old">
                                        <?=$params['price_page']?>
                                    </span>-->
                                </td>
                                <td><?=$books->getPrice(m_photobook::TYPE_PHOTOBOOK_STANDART, $format, 10, m_photobook::COVER_HARD, false, true)?>
                                    <!--<span class="bk-price-old">
                                        <?=$books->getPrice(m_photobook::TYPE_PHOTOBOOK_STANDART, $format, 10)?>
                                    </span>-->
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center bottom-padding-mini">
                    <a href=""  class="btn btn-warning" data-toggle="modal" data-target="#orderBook">Заказать печать фотокниги</a>
                </div>
            </div>
        </div>
    </div>
</div>

