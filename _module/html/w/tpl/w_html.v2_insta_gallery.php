<div class="slider rs-slider">
	<div class="tp-banner-container">
		<div class="tp-banner"  data-height="650">
			<ul>
				<li data-delay="7000" data-transition="fade" data-slotamount="7"
					data-masterspeed="5000">
					<div class="elements">
						<img src="/_site/i/v2/insta/insta_bg.jpg" alt="" class="tp-caption " style="width:100%"
							 data-x="0"
							 data-y="0"
							 data-speed="2000"
							 data-start="3000"
							 data-easing="Power4.easeOut"
							 data-endspeed="1500"
							 data-endeasing="Power1.easeIn" />


						<div class="tp-caption lft skewtotop h1 text-white"
							data-x="100"
							data-y="0"
							data-speed="1000"
							data-start="100"
							data-easing="Power4.easeOut"
							data-endspeed="500"
							data-endeasing="Power1.easeIn">
							<span>Индивидуальный дизайн</span>
						</div>

						<div class="tp-caption lft skewtotop h3 text-white"
							data-x="100"
							data-y="35"
							data-speed="1000"
							data-start="300"
							data-easing="Power4.easeOut"
							data-endspeed="500"
							data-endeasing="Power1.easeIn">
							<span>+ высококачественная печать</span>
						</div>

						<div class="tp-caption lft skewtotop text-red simple-text"
							 data-x="100"
							 data-y="62"
							 data-speed="1000"
							 data-start="500"
							 data-easing="Power4.easeOut"
							 data-endspeed="800"
							 data-endeasing="Power1.easeIn">
							 <span class="h1 text-red">3 500</span> руб
						</div>


						<div class="tp-caption lft skewtotop h2 text-white"
							 data-x="600"
							 data-y="10"
							 data-speed="1000"
							 data-start="1200"
							 data-easing="Power4.easeOut"
							 data-endspeed="1500"
							 data-endeasing="Power1.easeIn">
							<span>Доставка по России</span>
						</div>

						<div class="tp-caption lft skewtotop text-red"
							 data-x="600"
							 data-y="42"
							 data-speed="1000"
							 data-start="1500"
							 data-easing="Power4.easeOut"
							 data-endspeed="800"
							 data-endeasing="Power1.easeIn">
							<span class="text-red h1">340</span> руб
						</div>
					</div>
				</li>

			</ul>
			<div class="tp-bannertimer"></div>
		</div>
	</div>
</div>
