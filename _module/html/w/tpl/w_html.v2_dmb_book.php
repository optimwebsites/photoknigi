<div id="gallery_family">
	<div class="content-block">
	<div class="row">
		<div class="col-xs-12">
			<h2>Дембельский альбом - фотокнига</h2>
			<p>Этот альбом сочетает в себе красочное оформление и невысокую цену и сжатые сроки изготовления.
				Для дембельского альбома-фотокниги страницы печатаются на фотобумаге и склеиваются на пластике таким образом,
				чтобы получилась книга с разворотом страниц на 180 градусов. После этого из камуфляжной ткани (или материала формы для любого рода войск)
				изготавливается тканевая обложка, на ней вышивается эмблема, года службы, имя, любой текст или пожелание и обложка отделывается уголками.
				Так получается незабываемый и неповторимый подарок.
			</p>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<h3>Уникальный ДМБ-альбом</h3>
			<p>Правда! Такие дембельские альбомы в России делаем только мы</p>
		</div>
		<div class="col-sm-6">
			<h3>Высочайшее качество</h3>
			<p>Фотопечать, проклейка пластиком, развороты страниц на 180 градусов </p>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<h3>Уникальный дизайн</h3>
			<p>Материалы для обложки, вышивка эмблем, надписей, дизайн альбома для любого рода войск</p>
		</div>
		<div class="col-sm-6">
			<h3>Отличная цена</h3>
			<p>Просто пришлите фотографии и получите готовый дембельский альбом всего за 3500 руб.</p>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<h3>Изготовление за 7 дней</h3>
			<p>Уже через 1 день мы пришлем готовый макет, и согласуем его с Вами</p>
		</div>
		<div class="col-sm-6">
			<h3>Доставка по России</h3>
			<p>От Калининграда до Камчатки, в любую точку России. Автобусом, почтой, курьером, самолетом!</p>
		</div>

	</div>
		<button class="btn btn-danger scroll-to" data-target="#order-form">Заказать ДМБ-альбом</button>
	</div>
	<div class="row igallery">
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/dmb/1.jpg">
				<img src="/_site/i/v2/dmb/1.jpg"
					 title="<div class='igallery-caption'>Формат альбома 30х21 см</div>"/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/dmb/2.jpg">
				<img src="/_site/i/v2/dmb/2.jpg"
					 title="<div class='igallery-caption'>Тканевая обложка из камуфляжной ткани, вышивка, отделка уголками</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/dmb/3.jpg">
				<img src="/_site/i/v2/dmb/3.jpg"
					 title="<div class='igallery-caption'>Страницы проклеены пластиком</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/dmb/4.jpg">
				<img src="/_site/i/v2/dmb/4.jpg"
					 title="<div class='igallery-caption'>Корешок из кожзама</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/dmb/5.jpg">
				<img src="/_site/i/v2/dmb/5.jpg"
					 title="<div class='igallery-caption'>Фотопечать на глянцевой фотобумаге</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/dmb/6.jpg">
				<img src="/_site/i/v2/dmb/6.jpg"
					 title="<div class='igallery-caption'>Разворот страниц на 180 градусов</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/dmb/7.jpg">
				<img src="/_site/i/v2/dmb/7.jpg"
					 title="<div class='igallery-caption'>Вышивка на обложке любой сложности: эмблемы, девизы, имя, года службы</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/dmb/8.jpg">
				<img src="/_site/i/v2/dmb/8.jpg"
					 title="<div class='igallery-caption'>Такой альбом - незабываемый подарок</div>"
					/>
			</a>
		</div>
	</div>
</div>
