<div class="container">
    <div class="row">
        <div class="col-sm-12 bottom-padding-mini">
            <div class="title-box text-center">
                <h2 class="title ">Доставка по всей России</h2>
            </div>
            <?php
            $delivery = new w_html();
            $delivery->tpl = 'delivery';
            echo $delivery->getHtml();
            ?>
        </div>
    </div>
</div>
