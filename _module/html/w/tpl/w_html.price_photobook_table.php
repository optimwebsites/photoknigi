   <div class="row">
    <div class="col-xs-12">
        <h3>Технические требования к макетам</h3>

        <ul>
            <li>макет предоставляется в виде JPEG файлов</li>
            <li>разрешение файлов - 300 dpi</li>
            <li>цветовая модель файлов - RGB</li>
        </ul>

        <?php
        $discount = new w_html();
        $discount->tpl = "discount_warning";
        echo $discount->getHtml();
        ?>

        <h3>Размеры фотокниг ПРЕМИУМ при печати фотоспособом</h3>
        <p>Минимальный объем - 2 разворота</p>
        <table class="table table-condensed table-striped">
           <thead>
           <tr>
               <th>Формат, см</th>
               <th>Разворот до обреза, мм</th>
               <th>Обложка до загиба, мм</th>
               <th>Цена, 10 развортов</th>
               <th>Цена доп. разворота</th>
               <!--<th>Скачать</th>-->
           </tr>
           </thead>
           <tbody>
           <?php

           $books = new m_photobook();
           foreach ($books->getFormats(m_photobook::TYPE_PHOTOBOOK_PREMIUM) as $format) {
                $params = $books->getParams(m_photobook::TYPE_PHOTOBOOK_PREMIUM, $format);
                $cover_size = $books->getCoverSize(m_photobook::TYPE_PHOTOBOOK_PREMIUM, $format, m_photobook::COVER_HARD);
           ?>
           <tr>
               <td><?=$format?></td>
               <td><?=$params['width']."х".$params['height']?></td>
               <td><?=$cover_size['width']."х".$cover_size['height']?></td>
               <td><?=$books->getPrice(m_photobook::TYPE_PHOTOBOOK_PREMIUM, $format, 10, m_photobook::COVER_HARD, false, true)?>
                   <span class="bk-price-old">
                        <?=$books->getPrice(m_photobook::TYPE_PHOTOBOOK_PREMIUM, $format, 10)?>
                    </span>
               </td>
               <td><?=ceil($params['price_page'] * (1-$params['discount']/100))?>
                   <span class="bk-price-old">
                        <?=$params['price_page']?>
                    </span>
               </td>
               <!--<td><a href="">Photoshop (PSD)</a></td>-->
           </tr>
           <?php } ?>
           </tbody>
       </table>
       </div>
   </div>
    <div class="col-xs-12">
        <h3>Размеры фотокниг СТАНДАРТ при печати цифровым способом</h3>
        <p>Минимальный объем - 14 страниц</p>
        <table class="table table-condensed">
            <thead>
            <tr>
                <th>Формат, см</th>
                <th>Разворот до обреза, мм</th>
                <th>Обложка до загиба, мм</th>
                <th>Цена, 20 страниц</th>
                <th>Цена доп. страницы</th>
                <!--<th>Скачать</th>-->
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ($books->getFormats(m_photobook::TYPE_PHOTOBOOK_STANDART) as $format) {
            $params = $books->getParams(m_photobook::TYPE_PHOTOBOOK_STANDART, $format);
                $cover_size = $books->getCoverSize(m_photobook::TYPE_PHOTOBOOK_STANDART, $format, m_photobook::COVER_HARD);
            ?>
            <tr>
                <td><?=$format?></td>
                <td><?=$params['width']."х".$params['height']?></td>
                <td><?=$cover_size['width']."х".$cover_size['height']?></td>
                <td>
                    <?=$books->getPrice(m_photobook::TYPE_PHOTOBOOK_STANDART, $format, 10, m_photobook::COVER_HARD, false, true)?>
                    <span class="bk-price-old">
                        <?=$books->getPrice(m_photobook::TYPE_PHOTOBOOK_STANDART, $format, 10)?>
                    </span>
                </td>
                <td><?=ceil($params['price_page'] * (1-$params['discount']/100))?>
                    <span class="bk-price-old">
                        <?=$params['price_page']?>
                    </span>
                </td>
                <!--<td><a href="">Photoshop (PSD)</a></td>-->
            </tr>
            <?php } ?>
            </tbody>
        </table>
    </div>
   </div>