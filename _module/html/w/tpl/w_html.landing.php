<div class="full-width-box sample-book wedding">
    <div class="container ">
        <div class="row">
            <div class="col-sm-12 overflow">
                <div class="wedding">
                    <div class="row">
                        <div class="col-sm-12 col-md-4">
                            <h2>Свадебные фотокниги</h2>

                            <div class="h2">История любви <br> Свадьба <br> Венчание</div>
                            <div class="img-mobile"><img src="/_site/i/wedding3.jpg"/></div>
                            <div class="book-text">
                                <p>Самые чистые и прекрасные чувства, потрясающие своей искренностью.
                                    Личный праздник остается таким даже сквозь года,
                                    оформленный и запечатленный так, как хотите этого вы.</p>
                            </div>
                            <a href="/#orderbook" class="btn btn-primary orderbook">Заказать фотокнигу</a>
                        </div>
                        <div class="col-md-8  img-desktop">
                            <img src="/_site/i/wedding3.jpg"/>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>


<div class="full-width-box sample-book travel" id="photobooks">
    <div class="container ">
        <div class="row">
            <div class="col-sm-12 overflow">
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <h2>Детские фотокниги</h2>

                        <div class="h2">Первые шаги <br> Десткий сад <br> Школьник</div>
                        <div class="img-mobile"><img src="/_site/i/child2.jpg"/></div>
                        <div class="book-text">
                            <p>Ценнейшие и неуловимые моменты взросления, детские проделки и радостные ощущения будут
                                свежи в памяти всегда благодаря этим фотокнигам.
                                Мы поможем запечатлеть детство.</p>
                        </div>
                        <a href="/#orderbook" class="btn btn-success orderbook">Заказать фотокнигу</a>
                    </div>
                    <div class="col-md-8  img-desktop">
                        <img src="/_site/i/child2.jpg"/>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>



<!--<div class="full-width-box sample-book kids">
    <div class="container ">
        <div class="row">
            <div class="col-sm-12 overflow">
                <div class="row">
                    <div class="col-sm-12 col-md-4">
                        <h2>Семейные фотокниги</h2>

                        <div class="h2">Семейные архивы и <br> современные съемки</div>
                        <div class="book-text">
                            <p>Настоящая семейная фотолетопись. Многочисленные родственники,
                                близкие и родные – им всем найдется достойное место на страницах наших фотокниг.
                                Сохраните память о родителях и передайте ее детям.</p>
                        </div>
                        <a href="/#orderbook" class="btn btn-primary orderbook">Заказать фотокнигу</a>
                    </div>
                    <div class="col-md-8">
                        <img src="/_site/i/family.jpg"/>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>
-->

<div class="full-width-box sample-book family">
    <div class="container ">
        <div class="row">
            <div class="col-sm-12 col-md-4">
                <h2>Фотокниги путешествий</h2>

                <div class="h2">Отпуск <br> Поездка <br> Приключения</div>
                <div class="img-mobile"><img src="/_site/i/travel.jpg"/></div>
                <div class="book-text">
                    <p>Зафиксируйте каждое ваше путешествие так, чтобы его захотелось проживать снова и снова.
                        Яркая фотокнига напомнит вам о незабываемых моментах даже в череде будней.</p>
                </div>
                <a href="/#orderbook" class="btn btn-danger orderbook">Заказать фотокнигу</a>
            </div>
            <div class="col-md-8 img-desktop">
                <img src="/_site/i/travel.jpg"/>
            </div>

        </div>

    </div>
</div>
<div class="clearfix"></div>

<div class="full-width-box sample-book slides">
    <div class="container ">
        <div class="row">
            <div class="col-md-12">

                <?php
                $carousel = new w_gallery_carousel();
                $carousel->addResourcesToPage();
                ?>
                <div id="slider-with-blocks-1" class="royalSlider rsMinW  ">
                    <div class="rsContent slide1">
                        <div class="bContainer">
                            <div class="row">
                                <div class="col-sm-4 rsABlock" data-move-effect="left">
                                    <img src="/_site/i/handling.jpg" />
                                    <div class="h2">Подготовка макета</div>
                                    <p>Подготовим фотографии к работе: повысим или изменим яркость, контрастность, цветовую насыщенность, приведем к единому стилю.</p>
                                </div>

                                <div class="col-sm-4 rsABlock text-center" data-move-effect="none">
                                    <img src="/_site/i/design.jpg" />
                                    <div class="h2">Индивидуальный макет</div>
                                    <p>Разработаем индивидуальный макет фотокниги на основе ваших пожеланий и исходных данных, внесем корректировки и согласуем с вами.</p>
                                </div>
                                <div class="col-sm-4 rsABlock text-center" data-move-effect="right">
                                    <img src="/_site/i/quality2.jpg" />
                                    <div class="h2">Высочайшее качество</div>
                                    <p>Напечатаем вашу фотокнигу в нашей собственной типографии. Качество бумаги, печати и сборки гарантируем.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--<div class="rsContent slide2">
                        <div class="bContainer">
                            <div class="row">
                                <div class="col-sm-6 rsABlock" data-move-effect="left" data-delay="0" data-move-offset="500" data-easing="easeOutBack"  data-fade-effect="none">
                                    <img src="/_c/placeholder/show/s/?w=400&h=300" />
                                </div>
                                <div class="col-sm-6 rsABlock" data-move-effect="right" data-delay="0" data-move-offset="500">
                                    <div class="h2">Подготовка фотографий</div>

                                    <p>Каждую фотографию вручную обрабатывает дизайнер в специальной программе по ряду параметров:</p>
                                    <ul class="textlist">
                                        <li>Оптимальная экспозиция</li>
                                        <li>Повышает яркость снимка, выделяет слишком затемненные объекты.</li>
                                        <li>Контрастность.</li>
                                        <li>Повышает глубину цвета и четкость изображения.</li>
                                        <li>Оптимальная экспозиция</li>
                                        <li>Повышает яркость снимка, выделяет слишком затемненные объекты.</li>
                                        <li>Контрастность.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="rsContent slide3">
                        <div class="bContainer">
                            <div class="row">
                                <div class="col-sm-6 rsABlock" data-move-effect="left" data-delay="0" data-move-offset="500" data-easing="easeOutBack"  data-fade-effect="none">
                                    <img src="/_c/placeholder/show/s/?w=400&h=300" />
                                </div>
                                <div class="col-sm-6 rsABlock" data-move-effect="right" data-delay="0" data-move-offset="500">
                                    <div class="h2">Индивидуальный макет</div>

                                    <p>Каждую фотографию вручную обрабатывает дизайнер в специальной программе по ряду параметров:</p>
                                    <ul class="textlist">
                                        <li>Оптимальная экспозиция</li>
                                        <li>Повышает яркость снимка, выделяет слишком затемненные объекты.</li>
                                        <li>Контрастность.</li>
                                        <li>Повышает глубину цвета и четкость изображения.</li>
                                        <li>Оптимальная экспозиция</li>
                                        <li>Повышает яркость снимка, выделяет слишком затемненные объекты.</li>
                                        <li>Контрастность.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="rsContent slide4">
                        <div class="bContainer">
                            <div class="row">
                                <div class="col-sm-6 rsABlock" data-move-effect="left" data-delay="0" data-move-offset="500" data-easing="easeOutBack"  data-fade-effect="none">
                                    <img src="/_c/placeholder/show/s/?w=400&h=300" />
                                </div>
                                <div class="col-sm-6 rsABlock" data-move-effect="right" data-delay="0" data-move-offset="500">
                                    <div class="h2">Высочайшее качество</div>

                                    <p>Каждую фотографию вручную обрабатывает дизайнер в специальной программе по ряду параметров:</p>
                                    <ul class="textlist">
                                        <li>Оптимальная экспозиция</li>
                                        <li>Повышает яркость снимка, выделяет слишком затемненные объекты.</li>
                                        <li>Контрастность.</li>
                                        <li>Повышает глубину цвета и четкость изображения.</li>
                                        <li>Оптимальная экспозиция</li>
                                        <li>Повышает яркость снимка, выделяет слишком затемненные объекты.</li>
                                        <li>Контрастность.</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>-->
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="full-width-box sample-book benefits"  id="prices">
        <div class="container ">
            <div class="row">
                <div class="col-sm-12">
                    <h2>Стоимость</h2>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 super-price">
                   <em><span class="price-text">Суперцена!</span> Минибук 20х20см с индивидуальным дизайном, до 7 разворотов, мягкой обложкой всего за <span class="price-text">3 190</span> р.!</em>
                </div>
            </div>
            <div class="row price-icons">
                <div class="col-xs-4 col-sm-2">
                    <img src="/_site/i/booksize2.png" style="width:66px; height:66px; margin-top:24px;"/>
                    <div class="size-text">20x20см</div>
                    <div class="price-text">4 390</div>

                </div>
                <div class="col-xs-4 col-sm-2">
                    <img src="/_site/i/booksize2.png" style="width:66px; height:90px;""/>
                    <div class="size-text">20x30см</div>
                    <div class="price-text">5 390</div>
                </div>
                <div class="col-xs-4 col-sm-2">
                    <img src="/_site/i/booksize2.png" style="width:90px; height:66px;margin-top:24px;""/>
                    <div class="size-text">30x20см</div>
                    <div class="price-text">5 390</div>
                </div>
                <div class="col-xs-4 col-sm-2">
                    <img src="/_site/i/booksize2.png" style="width:80px; height:80px;margin-top:10px;""/>
                    <div class="size-text">25x25см</div>
                    <div class="price-text">5 690</div>
                </div>
                <div class="col-xs-4 col-sm-2">
                    <img src="/_site/i/booksize2.png" style="width:90px; height:90px"/>
                    <div class="size-text">30x30см</div>
                    <div class="price-text">6 290</div>
                </div>
                <div class="col-xs-4 col-sm-2">
                    <img src="/_site/i/booksize2.png" style="width:105px; height:90px"/>
                    <div class="size-text">40x30см</div>
                    <div class="price-text">8 190</div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 super-price"></div>
                <div class="col-sm-12 ">
                    <b>СКИДКА!!!</b> При заказе нескольких экземпляров скидка на каждый последующий - <b>15%</b>
                </div>
                <div class="col-sm-6">
                    <div class="h2">ВСЕ ВКЛЮЧЕНО!</div>
                    <ul>
                        <li>10 разворотов / 20 страниц;</li>
                        <li>индивидуальный дизайн до 50 фото;</li>
                        <li>предпечатная подготовка всех фотографий;</li>
                        <li>печать на фотобумаге премиум качества;</li>
                        <li>твердая ламинированная обложка c индивидуальным дизайном.</li>
                    </ul>
                </div>
                <div class="col-sm-6">
                    <div class="h2">Дополнительно:</div>

                    <ul>
                        <li>больше фотографий для макета;</li>
                        <li>до 100 страниц / 50 разворотов;</li>
                        <li>обложки из натуральной кожи, кожзама или "паспарту";</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="full-width-box sample-book prices"  id="howto">
        <div class="container ">
            <div class="row">
                <div class="col-sm-1 text-center">
                    &nbsp;<br/></div>
                <div class="col-sm-2 text-center">
                    <i class="fa fa-photo"></i>
                    <i class="fa fa-long-arrow-right pull-right"></i>
                    <p>Вы высылаете <br>фотографии</p>
                </div>
                <div class="col-sm-2 text-center">
                    <i class="fa fa-file-pdf-o"></i>
                    <i class="fa fa-long-arrow-right pull-right"></i>
                    <p>Мы делаем <br>макет</p>
                </div>
                <div class="col-sm-2 text-center">
                    <i class="fa fa-check"></i>
                    <i class="fa fa-long-arrow-right pull-right"></i>

                    <p>Согласовываем <br>с Вами</p>
                </div>
                <div class="col-sm-2 text-center">
                    <i class="fa fa-credit-card"></i>
                    <i class="fa fa-long-arrow-right pull-right"></i>

                    <p>Вы оплачиваете <br>услугу</p>
                </div>
                <div class="col-sm-2 text-center">
                    <i class="fa fa-truck"></i>
                    <p>Мы доставляем <br>Вам книгу</p>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="full-width-box sample-book terms">
        <div class="container ">
            <div class="row">

                <div class="col-sm-4">
                    <div class="h2">Сроки</div>
                    <p>Мы пришлем готовый макет Вам на согласование через 1 день после предоставления снимков</p>
                </div>
                <div class="col-sm-4">
                    <div class="h2">Оплата</div>
                    <ul class="textlist no-list-style">
                        <li>Более 50 способов оплаты через Интернет</li>
                        <li>Банковскими картами</li>
                        <li>Банковский перевод</li>
                    </ul>
                </div>
                <div class="col-sm-4">
                    <div class="h2">Доставка</div>
                    <ul class="textlist no-list-style">
                        <li>В любой регион России - <b>340 руб.</b></li>
                        <li>По Москве - в течение 1 дня</li>
                        <li>По России - от 5 до 10 дней</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>



    <script type="text/javascript" src="/_th/assets/galleries/royalslider/jquery.royalslider.min.js"></script>
    <script type="text/javascript">

        $('#slider-with-blocks-1').royalSlider({
            arrowsNav: true,
            arrowsNavAutoHide: false,
            fadeinLoadedSlide: false,
            controlNavigationSpacing: 0,
            controlNavigation: 'bullets',
            imageScaleMode: 'none',
            imageAlignCenter:false,
            blockLoop: true,
            loop: true,
            numImagesToPreload: 6,
            transitionType: 'fade',
            keyboardNavEnabled: true,
            block: {
                delay: 400
            }
        });
    </script>

