<div id="gallery_child">
	<div class="row content-block">
		<div class="col-xs-12">
			<h2>Детские фотокниги</h2>
			<p>
				Мы создаем любые детские фотокниги – от рождения и до совершеннолетия. Встреча из роддома, крещение, детский сад, первый звонок, отдых на море, день рождения, выпускной, а также любое важное событие из жизни вашего ребенка может быть оформлено в детскую фотокнигу.
			</p>
			<button class="btn btn-danger scroll-to" data-target="#order-form">Заказать фотокнигу</button>
		</div>
	</div>
	<div class="row igallery">
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/child/1.jpg">
				<img src="/_site/i/v2/child/1.jpg"
					 data-title="<div class='igallery-caption'>Персональная фотообложка, фотопечать. Цена 20x20 - <span class='igallery-price'>1354</span> <span class='igallery-price-rub'>р.</span></div>"
					 title="<div class='igallery-caption'>Персональная фотообложка, фотопечать. Цена 20x20 - <span class='igallery-price'>1354</span> <span class='igallery-price-rub'>р.</span></div>"
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/child/2.jpg">
				<img src="/_site/i/v2/child/2.jpg"
					 data-title="<div class='igallery-caption'>Комбинированная экокожа, отстрочка, шильда, чернение срезов. Цена 30x21 - <span class='igallery-price'>3969</span> <span class='igallery-price-rub'>р.</span></div>"
					 title="<div class='igallery-caption'>Комбинированная экокожа, отстрочка, шильда, чернение срезов. Цена 30x21 - <span class='igallery-price'>3969</span> <span class='igallery-price-rub'>р.</span></div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/child/3.jpg">
				<img src="/_site/i/v2/child/3.jpg"
					 data-title="<div class='igallery-caption'>Экокожа, фотовставка, тиснение серебром. Цена 25x25 - <span class='igallery-price'>4197</span> <span class='igallery-price-rub'>р.</span></div>"
					 title="<div class='igallery-caption'>Экокожа, фотовставка, тиснение серебром. Цена 25x25 - <span class='igallery-price'>4197</span> <span class='igallery-price-rub'>р.</span></div>"
				/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/child/4.jpg">
				<img src="/_site/i/v2/child/4.jpg"
					 data-title="<div class='igallery-caption'>Экокожа, фотовставка, тиснение золотом, отстрочка. Цена 21x30 - <span class='igallery-price'>3996</span> <span class='igallery-price-rub'>р.</span></div>"
					 title="<div class='igallery-caption'>Экокожа, фотовставка, тиснение золотом, отстрочка. Цена 21x30 - <span class='igallery-price'>3996</span> <span class='igallery-price-rub'>р.</span></div>"
					/>
			</a>
		</div>
	</div>
</div>
