<div class="row">
    <div class="col-xs-12">
    <?php
    $w = new w_html();
    $w->tpl = "book_price_inner";
    echo $w->getHtml();
    ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
        <?php
        $w_description = new w_seo();
        $w_description->tpl = "text";
        echo $w_description->getHtml();
        ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-12 bottom-padding-mini">
        <?php
        $w = new w_gallery_list();
        $w->setProperties([
            'id_gallery_list' => 12,
            'widget' => "gallery_carousel",
            'base_path' => '/fotoknigi/portfolio',
            'title' => 'Примеры фотокниг',
        ]);
        echo $w->getHtml();
        ?>
    </div>
</div>
