<div class="content-block content-block__bg_green print-booktype">
	<style>
		.print-booktype p {
			margin-top:20px;
			padding:10px;
		}

		.print-booktype h3 {
			margin-bottom: 20px;
		}
	</style>
	<div class="row">
		<div class="col-sm-12 col-md-6 text-center">
			<h3>Фотопечать мокрым (химическим) способом</h3>
			<div class="img-container">
				<img src="/_site/i/print/1.jpg" />
			</div>
			<p>На сегодняшний день это самый качественный способ печати изображения, который пока никто не может превзойти.
				Печать осуществляется на оборудовании Noritsy 3702 HD на фотобумаге FujicolorCrystalArchivePaper, обеспечивающем
				высокое качество печати с беспрецендентным разрешением 640dpi. Наше обрудование позволяет легко отобразить
				ту естественную красоту места съемки, что видел фотограф своими глазами при съемке фотографий.
			</p>
		</div>
		<div class="col-sm-12 col-md-6 text-center">
			<h3>Печать типографским (лазерным) способом</h3>
			<div class="img-container">
				<img src="/_site/i/print/konica.jpg" />
			</div>
			<p>
				Этот способ, пока немного уступает в качестве, мокрой печати, но зато себестоимость печати значительно ниже.
				Лазерная печать возможна на разных типах и толщинах бумаг и на картоне. В этом варианте возможна двусторонняя печать.
				Мы печатаем  на оборудовании KonicaMinoltaBizHub PRO C6000.
			</p>
		</div>
	</div>
</div>

