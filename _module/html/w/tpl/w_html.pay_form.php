<div class="container">
    <style>
        .delivery-items {
            list-style: none;
            margin:0;
            padding:0;
        }

        .delivery-items li {
            float: left;
            margin: 20px;
            margin-right: 3px;
            border: 1px solid transparent;
        }

        .delivery-items li:hover,
        .delivery-items li.active
        {
            background:#f0d59a;
            border: 1px solid #d1b886;
        }

        .delivery-item-price {
            font-weight: bold;
            font-size:17px;
        }

    </style>
    <div class="row">
        <div class="title-box text-center col-sm-12 col-md-8 col-md-offset-2">
            <div class="title h2">Оплата заказа</div>
        </div>
        <div class="col-sm-12 col-md-8 col-md-offset-2">

            <form class="form-box register-form contact-form" method="post" action="https://money.yandex.ru/eshop.xml">
                <!-- Обязательные поля -->
                <input name="shopId" value="105948" type="hidden"/>
                <input name="scid" value="38075" type="hidden"/>
                <?php
                $delivery = (int)Yii::app()->request->getQuery('delivery', 1);
                $sum = (int)Yii::app()->request->getQuery('sum');
                echo CHtml::hiddenField("sum", $sum);
                ?>
                <input name="customerNumber" value="1298" type="hidden"/>

                <div class="form-group">
                    <h4>Оплата изготовления фотокниги</h4>
                </div>
                <div class="form-group">
                    <label>Выберите способ оплаты:</label>
                    <select class="form-control" name="paymentType">
                        <option value="AC">Оплата с банковской карты</option>
                        <option value="SB">Сбербанк: оплата по SMS или Сбербанк Онлайн</option>
                        <option value="PC">Яндекс.Деньги</option>
                        <option value="WM">WebMoney</option>
                        <option value="QW">QIWI Wallet</option>
                        <option value="AB">Альфа-Клик</option>
                    </select>
                </div>
                <?php

                if ($delivery) {
                    ?>
                    <div class="form-group">
                        <label>Адрес доставки <?= $delivery == 2 ? "" : "(обязательно укажите почтовый индекс):" ?></label>
                        <input type="text" class="form-control" name="custAddr"/>
                    </div>
                    <div class="form-group">
                        <label>Получатель:</label>
                        <input type="text" class="form-control" name="custName"/>
                    </div>
                    <div class="form-group">
                        <label><?= $delivery == 2 ? "Доставка по Москве" : "Доставка по России:" ?></label>
                        <ul class="delivery-items text-center">
                            <?php if ($delivery == 1) { ?>
                            <li data-cost="340" class="active"><img style="height:60px;" src="/_site/i/postrf.png" /><br/>5-9 дней <br> <span
                                    class="delivery-item-price">340</span> руб
                            </li>
                            <li data-cost="580"><img style="height:60px;" src="/_site/i/dpd.png" /><br/>2-3 дня <br> <span class="delivery-item-price">580</span>
                                руб
                            </li>
                            <li data-cost="780"><img src="/_site/i/ems.png" style="height:60px; background-color: #fff;" />  <br/>2-3 дня <br> <span class="delivery-item-price">780</span> руб
                            </li>
                            <?php } else { ?>
                                <li data-cost="150"  class="active"><img style="height:60px;" src="/_site/i/dpd.png" /><br/>2-3 дня <br> <span class="delivery-item-price">150</span>
                                    руб
                                </li>
                            <?php } ?>
                        </ul>
                        <input type="hidden" class="form-control" name="orderDetails"/>

                        <div class="clearfix"></div>
                    </div>
                    <div class="form-group">
                        <input type="checkbox" class="" name="" checked="checked"/> <b>Я понимаю, что <?=$name = Yii::app()->site->getName();?> не
                            несет ответственности за сроки доставки выбранным мной перевозчиком</b>
                    </div>

                <div>
                    <div class="h5 pull-left">Сумма заказа</div>
                    <div class="h5 pull-right"><span id="sumOrder"><?=$sum?></span><small> руб</small></div>
                    <div class="clearfix"></div>
                </div>
                <div>
                    <div class="h5 pull-left">Стоимость доставки</div>
                    <div class="h5 pull-right"><span id="sumDelivery"><?= $delivery == 2 ? 150 : 340 ?></span><small> руб</small></div>
                    <div class="clearfix"></div>
                </div>
                <?php
                }
                ?>
                <div>
                    <div class="h5 pull-left">Сумма к оплате</div>
                    <div class="h5 pull-right"><span id="sumTotal"><?=$sum?></span><small> руб</small></div>
                    <div class="clearfix"></div>
                </div>

                <div class="clearfix"></div>
                <div class="buttons-box clearfix">
                    <div class="alert hide" id="formMsg_PayForm"></div>
                    <button type="submit" class="btn btn-warning btn-lg">Оплатить</button>
                </div>
            </form>
            <script>
                function calcOrder()
                {
                    var sumOrder = $("#sumOrder");

                    if (sumOrder.length != 1) {
                        return;
                    }

                    var sum = parseInt($("#sumOrder").text()) - (-1) * parseInt($("#sumDelivery").text());
                    $("#sumTotal").html(sum);
                    $("[name=sum]").val(sum);
                }

                $(document).ready(function() {
                    $(".delivery-items li").click(function() {
                        $(".delivery-items li").removeClass("active");
                        $(this).addClass("active");
                        $("#sumDelivery").html($(this).data("cost"));
                        calcOrder();
                    });
                });

                calcOrder();
            </script>
        </div>
    </div>
</div>