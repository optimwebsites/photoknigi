<div class="container">
    <?php
    //костыль. Нужто только для того, чтобы подтянуть JS и CSS для карусели
    //@todo перевести на галерею
    $g = new w_gallery_carousel();
    $g->getHtml();

    $name = Yii::app()->site->getName();
    ?>
    <div class="row">
        <div class="bottom-padding-mini col-sm-12 col-md-12 carousel-box"
             data-carousel-pagination="true" data-carousel-one="true" data-carousel-nav="true"
             data-carousel-autoplay="false" data-carousel-interval="20000"
             data-autoplay-disable="true">
            <div class="title-box">
                <a class="next" href="#">
                    <svg x="0" y="0" width="9px" height="16px" viewBox="0 0 9 16"
                         enable-background="new 0 0 9 16" xml:space="preserve">
				  <polygon fill-rule="evenodd" clip-rule="evenodd" fill="#fcfcfc"
                           points="1,0.001 0,1.001 7,8 0,14.999 1,15.999 9,8 "></polygon>
				</svg>
                </a>
                <a class="prev" href="#">
                    <svg x="0" y="0" width="9px" height="16px" viewBox="0 0 9 16"
                         enable-background="new 0 0 9 16" xml:space="preserve">
				  <polygon fill-rule="evenodd" clip-rule="evenodd" fill="#fcfcfc"
                           points="8,15.999 9,14.999 2,8 9,1.001 8,0.001 0,8 "></polygon>
				</svg>
                </a>

                <h2 class="title">Отзывы</h2>
            </div>
            <div class="respond-carousel">
                <div class="caroufredsel_wrapper">
                    <div class="carousel">
                        <div class="respond respond-blockquote border">
                            <div class="description border-success">
                                <blockquote>
                                    <p>В поисках компании по созданию фотокниги перерыла много
                                        сайтов по интернету. Со многими хотела установить контакт,
                                        сделать заказ, но
                                        должного внимания к своему заказу не нашла.</p>

                                    <p>Отправила заявку <?=$name?> по интернету, через
                                        несколько минут мне перезвонили, уточнив, что я хочу
                                        получить в результате.</p>

                                    <p>Далее с менеджером Аней обсуждали все
                                        детали: свет, стиль, размеры, ориентиры.
                                        Дважды пересылались варианты макета. Сколько же нужно было
                                        проявить Ане терпения.</p>

                                    <p>В результате получился замечательный подарок, который можно с
                                        удовольствием показывать родным, друзьям, близким.</p>

                                    <p><b>Спасибо вам огромное!</b></p>
                                </blockquote>
                            </div>
                            <div class="name">
                                <strong>Сария Рахимова</strong>

                                <div>Санкт-Петербург</div>
                            </div>
                        </div>

                        <div class="respond respond-blockquote border">
                            <div class="description border-success">
                                <blockquote>
                                    <p>Добрый день! Большая благодарность всем сотрудникам
                                        <?=$name?> за внимательное отношение и хорошо
                                        выполненную работу. Я живу в
                                        Южно-Сахалинске и качественных типографий здесь нет. Когда
                                        наши близкие друзья решили переехать жить в другой город, мы
                                        задумались о подарке на память. Решили подарить фотокнигу,
                                        начали искать, кто ее сделает. Предложений было много,
                                        многие фирмы писали, что работают в воскресенье. Но когда я
                                        стал звонить в воскресенье (сроки поджимали!) по контактным
                                        номерам, дозвониться не мог. В <?=$name?> мне
                                        ответили, приняли заказ. Меня очень внимательно выслушали,
                                        учли все мои пожелания.
                                        Потом прислали мне макет на утверждение,
                                        мне понравилось. Договорились о том, как я получу свой
                                        заказ, пошли мне на встречу. Очень понравилось то, что заказ
                                        был хорошо сделан и доставлен в срок. И отправили мне заказ
                                        именно тем
                                        перевозчиком, каким я просил. Друзьям подарок очень
                                        понравился, и они с радостью будут вспоминать нас и то
                                        время, когда они жили в Южно-Сахалинске. Желаю
                                        <?=$name?> и дальше так работать!</p>
                                </blockquote>
                            </div>
                            <div class="name">
                                <strong>Иван Рыбников</strong>

                                <div>Южно-Сахалинск</div>
                            </div>
                        </div>
                        <div class="respond respond-blockquote border">
                            <div class="description border-success">
                                <blockquote>
                                    <p>Меня зовут Ольга. Не так давно у меня была прекрасная
                                        свадьба. С фотографом нам явно не повезло:
                                        фотографии некрасивые, без фокуса, пересвеченные, в кадрах
                                        много лишнего. Не фотографии — сплошное разочарование, я
                                        даже плакала. Такой счастливый день — и такие ужасные
                                        фотографии. Я обратилась на сайт
                                        <?=$name?>. Меня там внимательно выслушали и
                                        успокоили, сказали, что и не такое видели. А еще заверили,
                                        что с нашими фотографиями будет работать самый лучший
                                        опытный дизайнер. Когда увидела макет своей книги, просто не
                                        узнала
                                        свои жуткие фото. Наши фотографии удивительным образом
                                        изменились в лучшую сторону: дизайнер исправил цвет
                                        фотографий, убрал лишнее из кадра, кое-где даже изменил фон.
                                        В итоге со страниц фотокниги смотрим счастливые мы, такие,
                                        какими были на свадьбе. Большое спасибо дизайнеру Лене и
                                        всему коллективу <?=$name?>.</p>
                                </blockquote>
                            </div>
                            <div class="name">
                                <strong>Ольга Мухина</strong>

                                <div>Рязань</div>
                            </div>
                        </div>

                    </div>
                    <div class="clearfix"></div>
                    <div class="pagination switches" style="display: inline-block;"><a
                            href="#"><span>1</span></a><a href="#"
                                                          class="selected"><span>2</span></a><a
                            href="#"><span>3</span></a></div>

                </div>
            </div>
        </div>

    </div>
</div>