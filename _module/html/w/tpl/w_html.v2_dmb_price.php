<div class="content-block content-block">
	<div class="title">
		<h2>Цена</h2>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-6">
			<h3>Дембельский альбом - фотокнига</h3>
			<div class="img-container  col-sm-6">
				<img src="/_site/i/v2/dmb/price.jpg" />
			</div>
			<h3>В стоимость включено</h3>
			<ul class="simple-list">
				<li>высококачественная фотопечать 10 страниц (5 разворотов)</li>
				<li>до 30 фотографий</li>
				<li>обложка из камуфляжной ткани</li>
				<li>вышивка на обложке эмблемы, рода войск</li>
				<li>корешок из кожзама</li>
				<li>отделка уголками</li>
			</ul>
			<div class="clearfix"></div>
			<div class="text-red">Цена <span class="h2 text-red">3 500</span> руб</div>

			<h3>Дополнительно</h3>
			<ul class="simple-list">
				<li>больше разворотов / фотографий</li>
				<li>обложка из материала формы любого рода войск</li>
				<li>разработка индивидуального макета альбома</li>
				<li>прострочка обложки, бокс для альбома, отделка срезов</li>
			</ul>
			<button class="btn btn-danger btn-lg scroll-to" data-target="#order-form">Заказать ДМБ-альбом</button>
		</div>
		<div class="col-xs-12 col-md-6">
			<h3>Дембельский альбом - скрапбукинг</h3>
			<div class="img-container col-sm-6">
				<img src="/_site/i/v2/dmb/p2.jpg" />
			</div>
			<h3>В стоимость включено</h3>
			<ul class="simple-list">
				<li>ДМБ-альбом 10 страниц (5 разворотов)</li>
				<li>ручная работа</li>
				<li>индивидуальный дизайн</li>
				<li>до 30 фотографий</li>
				<li>пошив формы любого рода войск для альбома</li>
				<li>отделка альбома</li>
			</ul>
			<div class="clearfix"></div>
			<div class="text-red">Цена <span class="h2 text-red">7 200</span> руб</div>

			<h3>Дополнительно</h3>
			<ul class="simple-list">
				<li>больше разворотов / фотографий</li>
				<li>короба, чемоданчики для ДМБ-альбома</li>
				<li>разработка уникальных элементов оформления</li>
			</ul>
		</div>
	</div>
</div>
