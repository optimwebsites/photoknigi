<div>
<div class="content-block">
	<div class="title">
		<h2>Заказать инстабук</h2>
	</div>
	<div class="row">
		<div class="col-xs-12 col-md-4">
			<div class="img-container">
				<img src="/_site/i/v2/dmb/price.jpg" />
			</div>
		</div>
		<div class="col-xs-12 col-md-8">
			<ul class="simple-list">
				<li>высококачественная фотопечать 16 страниц (8 разворотов)</li>
				<li>30-50 фотографий</li>
				<li>твердая ламинированная фотообложка</li>
				<li>проклейка страниц пластиком</li>
			</ul>
			<div class="row">
				<div class="col-md-6 text-center">
					<div class="text-red">Цена <span class="h2 text-red">1 200</span> руб</div>
					<br/>
					<button class="btn btn-danger btn-lg" id="openEditor">Создать инстабук самостоятельно</button>
				</div>
				<div class="col-md-6 text-center">
					<div class="text-red">Цена <span class="h2 text-red">2 200</span> руб</div>
					<br/>
					<button class="btn btn-danger btn-lg scroll-to" data-target="#order-form">Заказать у дизайнера</button>
				</div>
			</div>
		</div>
	</div>
</div>
	<div id="ffbEditor">
		редактор
	</div>
	<script type="text/javascript" src="/_site/_js/ffbEditor.js"></script>
	<script type="text/javascript">

	</script>
</div>