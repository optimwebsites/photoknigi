<div class="content-block  content-block__bg_green">
	<div class="title text-center">
		<h2 class="text-red">Убедитесь в качестве наших фотокниг прямо сейчас</h2>
	</div>
	<div class="row how-to-work">
		<div class="col-xs-12">
			<p>Вы фотограф? Вышлем бесплатный демо образец, дополнительные образцы со скидкой 30%.</p>
			<div class="text-center"><button class="btn btn-danger scroll-to" data-target="#order-form">Заказать образцы</button></div>
		</div>
	</div>
</div>
