<?php
$phone = new Phone(Yii::app()->params['cms']['phone']);

?>
<div class="container">
	<div class="row">
		<div class="col-xs-4 col-sm-2 col-md-2">
			<div class="logo">
				<div><a href="/"><?=$name = Yii::app()->site->getName();?></a></div>
			</div>
		</div>
		<div class="hidden-xs col-sm-6 col-md-5">
			<nav>
				<ul class="main-menu">
					<li class="active"><a href="/">Фотокниги <br>на заказ</a></li>
					<li><a href="/vypusknye_albomi/">Выпускные<br> альбомы</a></li>
					<li><a href="/fotoknigi/pechat/">Печать<br> фотокниг</a></li>
					<!--<li><a href="/fotoknigi/portfolio/">Наши<br> работы</a></li>-->
				</ul>
			</nav>
		</div>
		<div class="hidden-xs hidden-sm col-md-2 text-center">
			<p class="h5 text-white">Доставка по РФ</p>
			<p class="h4 text-red">340 руб.</p>
		</div>
		<div class="col-xs-8 col-sm-4 col-md-3 text-center">
			<div class="phone"><a href="tel:<?=$phone->getNumber()?>"><?=$phone->prettyFormat()?></a></div>
			<div class="phone-desc"><a href="mailto:<?=Yii::app()->site->getEmail()?>"><?=Yii::app()->site->getEmail()?></a></div>
				<div class="work-always">без выходных</div>
		</div>
	</div>
</div>