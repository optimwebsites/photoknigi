<div class="content-block content-block__bg_green">
	<div class="title">
		<h2>Оформление фотокниги</h2>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="slider rs-slider">
				<div class="tp-banner-container">
					<div class="tp-banner" data-height="480">
						<ul>
							<li data-delay="5000" data-transition="fade" data-slotamount="7"
								data-masterspeed="2000">
								<div class="elements">

									<img src="/_site/i/v2/1.jpg" class="tp-caption lfb skewtotop column3"
										 data-x="0"
										 data-y="60"
										 data-speed="1000"
										 data-start="0"
										 data-easing="Power4.easeOut"
										 data-endstart="0"
										 data-endspeed="1000"
										 data-endeasing="Power1.easeIn" />


									<img src="/_site/i/v2/2.jpg" class="tp-caption lfb skewtotop column3"
										 data-x="400"
										 data-y="60"
										 data-speed="2000"
										 data-start="200"
										 data-easing="Power4.easeOut"
										 data-endstart="300"
										 data-endspeed="5000"
										 data-endeasing="Power1.easeIn" />

									<img src="/_site/i/v2/3.jpg" class="tp-caption lfb skewtotop column3"
										 data-x="800"
										 data-y="60"
										 data-speed="3000"
										 data-start="500"
										 data-easing="Power4.easeOut"
										 data-endspeed="10000"
										 data-endeasing="Power1.easeIn" />


									<div class="tp-caption lft skewtotop center-caption text-red"
										 data-x="0"
										 data-y="0"
										 data-speed="1000"
										 data-start="1000"
										 data-easing="Power4.easeOut"
										 data-endspeed="1000"
										 data-endeasing="Power1.easeIn">
										<span>Вышивка на обложке, <br/>прострочка по периметру</span>
									</div>


									<div class="tp-caption lft skewtotop center-caption text-red"
										 data-x="400"
										 data-y="0"
										 data-speed="1000"
										 data-start="1000"
										 data-easing="Power4.easeOut"
										 data-endspeed="500"
										 data-endeasing="Power1.easeIn">
										<span>Золочение <br/>и серебрение срезов</span>
									</div>

									<div class="tp-caption lft skewtotop center-caption text-red"
										 data-x="800"
										 data-y="0"
										 data-speed="1000"
										 data-start="1000"
										 data-easing="Power4.easeOut"
										 data-endspeed="500"
										 data-endeasing="Power1.easeIn">
										<span>Отделка <br/>уголками</span>
									</div>


								</div>
							</li>


							<li data-delay="5000" data-transition="fade" data-slotamount="7"
								data-masterspeed="2000">
								<div class="elements">

									<img src="/_site/i/v2/4.jpg" class="tp-caption lft skewtotop column3"
										 data-x="0"
										 data-y="60"
										 data-speed="1000"
										 data-start="0"
										 data-easing="Power4.easeOut"
										 data-endspeed="1000"
										 data-endeasing="Power1.easeIn" />


									<img src="/_site/i/v2/5.jpg" class="tp-caption lft skewtotop column3"
										 data-x="400"
										 data-y="60"
										 data-speed="2000"
										 data-start="200"
										 data-easing="Power4.easeOut"
										 data-endspeed="5000"
										 data-endeasing="Power1.easeIn" />

									<img src="/_site/i/v2/6.jpg" class="tp-caption lft skewtotop column3"
										 data-x="800"
										 data-y="60"
										 data-speed="3000"
										 data-start="500"
										 data-easing="Power4.easeOut"
										 data-endspeed="10000"
										 data-endeasing="Power1.easeIn" />


									<div class="tp-caption lft skewtotop center-caption text-red"
										 data-x="0"
										 data-y="0"
										 data-speed="1000"
										 data-start="1000"
										 data-easing="Power4.easeOut"
										 data-endspeed="500"
										 data-endeasing="Power1.easeIn">
										<span>Ламинированные <br/>фотообложки</span>
									</div>


									<div class="tp-caption lft skewtotop center-caption text-red"
										 data-x="400"
										 data-y="0"
										 data-speed="1000"
										 data-start="1000"
										 data-easing="Power4.easeOut"
										 data-endspeed="500"
										 data-endeasing="Power1.easeIn">
										<span>Обложки из ткани <br/>(более 30 видов ткани)</span>
									</div>

									<div class="tp-caption lft skewtotop center-caption text-red"
										 data-x="800"
										 data-y="0"
										 data-speed="1000"
										 data-start="1000"
										 data-easing="Power4.easeOut"
										 data-endspeed="500"
										 data-endeasing="Power1.easeIn">
										<span>Обложки из экокожи <br/>(более 40 видов экокожи)</span>
									</div>
								</div>
							</li>
							<li data-delay="5000" data-transition="fade" data-slotamount="7"
								data-masterspeed="2000">
								<div class="elements">

									<img src="/_site/i/v2/7.jpg" class="tp-caption lfb skewtotop column3"
										 data-x="0"
										 data-y="60"
										 data-speed="1000"
										 data-start="0"
										 data-easing="Power4.easeOut"
										 data-endspeed="1000"
										 data-endeasing="Power1.easeIn" />


									<img src="/_site/i/v2/8.jpg" class="tp-caption lfb skewtotop column3"
										 data-x="400"
										 data-y="60"
										 data-speed="2000"
										 data-start="200"
										 data-easing="Power4.easeOut"
										 data-endspeed="5000"
										 data-endeasing="Power1.easeIn" />

									<img src="/_site/i/v2/9.jpg" class="tp-caption lfb skewtotop column3"
										 data-x="800"
										 data-y="60"
										 data-speed="3000"
										 data-start="500"
										 data-easing="Power4.easeOut"
										 data-endspeed="1000"
										 data-endeasing="Power1.easeIn" />


									<div class="tp-caption lft skewtotop center-caption text-red"
										 data-x="0"
										 data-y="0"
										 data-speed="1000"
										 data-start="1000"
										 data-easing="Power4.easeOut"
										 data-endspeed="500"
										 data-endeasing="Power1.easeIn">
										<span>Фотовставки <br/>на обложках</span>
									</div>


									<div class="tp-caption lft skewtotop center-caption text-red"
										 data-x="400"
										 data-y="0"
										 data-speed="1000"
										 data-start="1000"
										 data-easing="Power4.easeOut"
										 data-endspeed="500"
										 data-endeasing="Power1.easeIn">
										<span>Шильды <br/>с именами</span>
									</div>

									<div class="tp-caption lft skewtotop center-caption text-red"
										 data-x="800"
										 data-y="0"
										 data-speed="1000"
										 data-start="1000"
										 data-easing="Power4.easeOut"
										 data-endspeed="500"
										 data-endeasing="Power1.easeIn">
										<span>Комбинированные <br/>обложки</span>
									</div>
								</div>
							</li>

							<li data-delay="5000" data-transition="fade" data-slotamount="7"
								data-masterspeed="2000">
								<div class="elements">

									<img src="/_site/i/v2/10.jpg" class="tp-caption lft skewtotop column3"
										 data-x="0"
										 data-y="60"
										 data-speed="1000"
										 data-start="0"
										 data-easing="Power4.easeOut"
										 data-endspeed="1000"
										 data-endeasing="Power1.easeIn" />


									<img src="/_site/i/v2/11.jpg" class="tp-caption lft skewtotop column3"
										 data-x="400"
										 data-y="60"
										 data-speed="2000"
										 data-start="200"
										 data-easing="Power4.easeOut"
										 data-endspeed="5000"
										 data-endeasing="Power1.easeIn" />

									<img src="/_site/i/v2/12.jpg" class="tp-caption lft skewtotop column3"
										 data-x="800"
										 data-y="60"
										 data-speed="3000"
										 data-start="500"
										 data-easing="Power4.easeOut"
										 data-endspeed="10000"
										 data-endeasing="Power1.easeIn" />


									<div class="tp-caption lft skewtotop center-caption text-red"
										 data-x="0"
										 data-y="0"
										 data-speed="1000"
										 data-start="1000"
										 data-easing="Power4.easeOut"
										 data-endspeed="500"
										 data-endeasing="Power1.easeIn">
										<span>Проклейка страниц <br/>пластиком</span>
									</div>


									<div class="tp-caption lft skewtotop center-caption text-red"
										 data-x="400"
										 data-y="0"
										 data-speed="1000"
										 data-start="1000"
										 data-easing="Power4.easeOut"
										 data-endspeed="500"
										 data-endeasing="Power1.easeIn">
										<span>Боксы <br/>для фотокниг</span>
									</div>

									<div class="tp-caption lft skewtotop center-caption text-red"
										 data-x="800"
										 data-y="0"
										 data-speed="1000"
										 data-start="1000"
										 data-easing="Power4.easeOut"
										 data-endspeed="500"
										 data-endeasing="Power1.easeIn">
										<span><br>Минибуки</span>
									</div>
								</div>
							</li>



						</ul>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
