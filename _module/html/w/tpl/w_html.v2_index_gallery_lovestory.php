<div id="gallery_lovestory">
	<div class="row content-block">
		<div class="col-xs-12">
			<h2>Love Story</h2>
			<p>
				Да здравствует любовь! Романтичные и нежные фотокниги Love Story обновляют чувства и становятся лучшим подарком для пары влюбленных. Для подобной фотокниги не обязательно использовать профессиональные фотографии. Главное – передать чувства и настроение.
			</p>
			<button class="btn btn-danger scroll-to" data-target="#order-form">Заказать фотокнигу</button>
		</div>
	</div>
	<div class="row igallery">
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/lovestory/1.jpg">
				<img src="/_site/i/v2/lovestory/1.jpg"
					 data-title="<div class='igallery-caption'>Экокожа, фотовставка, золочение срезов, окантовка, бокс. Цена 30x30 - <span class='igallery-price'>7891</span> <span class='igallery-price-rub'>р.</span></div>"
					 title="<div class='igallery-caption'>Экокожа, фотовставка, золочение срезов, окантовка, бокс. Цена 30x30 - <span class='igallery-price'>7891</span> <span class='igallery-price-rub'>р.</span></div>"
					/>
			</a>
			<div class="igallery-hover">
				<div class='igallery-caption'>Экокожа, фотовставка, золочение срезов, окантовка, бокс. Цена 30x30 - <span class='igallery-price'>7891</span> <span class='igallery-price-rub'>р.</span></div>
			</div>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/lovestory/2.jpg">
				<img src="/_site/i/v2/lovestory/2.jpg"
					 data-title="<div class='igallery-caption'>Экокожа, вышивка золотом, золочение срезов, отстрочка. Цена 30x21 - <span class='igallery-price'>3894</span> <span class='igallery-price-rub'>р.</span></div>"
					 title="<div class='igallery-caption'>Экокожа, вышивка золотом, золочение срезов, отстрочка. Цена 30x21 - <span class='igallery-price'>3894</span> <span class='igallery-price-rub'>р.</span></div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/lovestory/3.jpg">
				<img src="/_site/i/v2/lovestory/3.jpg"
					 data-title="<div class='igallery-caption'>Экокожа, фотовставка, тиснение золотом, индивидуальное клише. Цена 30x25 - <span class='igallery-price'>5460</span> <span class='igallery-price-rub'>р.</span></div>"
					 title="<div class='igallery-caption'>Экокожа, фотовставка, тиснение золотом, индивидуальное клише. Цена 30x25 - <span class='igallery-price'>5460</span> <span class='igallery-price-rub'>р.</span></div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/lovestory/4.jpg">
				<img src="/_site/i/v2/lovestory/4.jpg"
					 data-title="<div class='igallery-caption'>Фотообложка, фотопечать. Цена 30x30 - <span class='igallery-price'>1463</span> <span class='igallery-price-rub'>р.</span></div>"
					 title="<div class='igallery-caption'>Фотообложка, фотопечать. Цена 30x30 - <span class='igallery-price'>1463</span> <span class='igallery-price-rub'>р.</span></div>"
					/>
			</a>
		</div>
	</div>
</div>
