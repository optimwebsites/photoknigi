<?php
$phone = new Phone(Yii::app()->params['cms']['phone']);
?>
<div class="content-block content-block__bg_green2 ">
	<div class="row order-row">
		<div class="col-xs-12 col-sm-6 text-center">
			<button class="btn btn-danger btn-lg scroll-to" type="button" data-target="#order-form">Заказать фотокнигу</button>
		</div>
		<div class="col-xs-12 col-sm-6  text-center">
			<div class="phone"><a href="tel:<?=$phone->getNumber()?>"><?=$phone->prettyFormat()?></a></div>
			<!--<div class="order-row__phone-desc">бесплатный звонок с мобильного телефона</div>-->
		</div>
	</div>
</div>
