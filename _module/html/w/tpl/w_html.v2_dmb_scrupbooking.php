<div id="gallery_family">
	<div class="row content-block">
		<div class="col-xs-12">
			<h2>Дембельские альбомы скрапбукинг</h2>
			<p>
				Дембельский альбом в технике срапбукинг - это настоящий шедевр!
				Только ручная работа и индивидуальный дизайн.
				Наши мастера осуществят любую фантазию и идею дембельского альбома.
				Вы можете заказать дембельский альбом с оформлением фотографиями
				или купить дембельский альбом и вставить фотографии самостоятельно.
				Самое разнообразное оформление, любой род войск, размер, цвет, конфигурация – скрапбукингу все под силу.
			</p>
			<p>Мы даже можем сшить настоящую форму для дембельского альбома на заказ!
				А еще мы изготавливаем дембельские коробки или чемоданчики, открытки с армейской тематикой, стикеры,
				наклейки и многое другое!</p>
			<button class="btn btn-danger scroll-to" data-target="#order-form">Заказать ДМБ-альбом</button>
		</div>
	</div>
	<div class="row igallery">
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/dmb/s1.jpg">
				<img src="/_site/i/v2/dmb/s1.jpg"
					 title="<div class='igallery-caption'></div>"/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/dmb/s3.jpg">
				<img src="/_site/i/v2/dmb/s3.jpg" />
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/dmb/s4.jpg">
				<img src="/_site/i/v2/dmb/s4.jpg" />
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/dmb/s7.jpg">
				<img src="/_site/i/v2/dmb/s7.jpg" />
			</a>
		</div>

		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/dmb/s5.jpg">
				<img src="/_site/i/v2/dmb/s5.jpg"
					 title="<div class='igallery-caption'></div>"/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/dmb/s6.jpg">
				<img src="/_site/i/v2/dmb/s6.jpg" />
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/dmb/s2.jpg">
				<img src="/_site/i/v2/dmb/s2.jpg" />
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/dmb/s8.jpg">
				<img src="/_site/i/v2/dmb/s8.jpg" />
			</a>
		</div>
	</div>
</div>
