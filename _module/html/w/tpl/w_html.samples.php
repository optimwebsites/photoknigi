<div class="container">
    <div class="row">
        <div class="title-box text-center col-sm-12 col-md-12">
            <div class="title h2">Примеры работ</div>
        </div>
        <div class="col-sm-12 col-md-12">
            <?php
            $gallery = new w_gallery();
            $gallery->name = "samples";
            echo $gallery->getHtml();
            ?>
        </div>
    </div>
</div>