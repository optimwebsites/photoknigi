<div class="container">
     <div class="row text-center">
        <div class="col-sm-12">

            <div class="title-box text-center">
                <div class="h1 title">Почему печать фотокниг нужно заказать у нас?</div>
            </div>
        </div>

        <div class="clearfix"><br/></div>
        <div class="row our-services"  id="photobooks">
            <div class="col-sm-4 text-center bottom-padding-mini">

                <div class="h5">Индивидуальный подход</div>
                <p>
                    Мы не делим заказчиков на крупных и остальных. Мы следим за своей репутацией и всегда идем навстречу пожеланиям наших клиентов.

                </p>
            </div>

            <div class="col-sm-4 bottom-padding-mini">
                <div class="h5">Высокое качество</div>
                <p>
                    Cовременное высокотехнологичное оборудование, только оригинальные расходные материалы и опытный персонал с опытом работы более 10 лет
                - рецепт высокого качества нашей продукции.
                </p>
            </div>


            <div class="col-sm-4 text-center bottom-padding-mini">

                <div class="h5">Скорость изготовления</div>
                <p>Стандартный срок изготовления фотокниги - 2 дня.
                    Даже в самый пик работы (май, июнь) сроки изготовления не превышают 7 дней. Для этого мы налаживаем круглосуточное производство.
                </p>
            </div>
        </div>
    </div>
</div>