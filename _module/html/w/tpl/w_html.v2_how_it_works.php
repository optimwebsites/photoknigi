<div class="content-block  content-block__bg_green">
	<div class="title">
		<h2>Как мы работаем</h2>
	</div>
	<div class="row how-to-work">
		<div class="col-xs-6 col-sm-2">
			<div class="how-to-icon"><img src="/_site/i/v2/photo.svg" /></div>
			<p class="h4">Фотографии</p>
			<p>Вы присылаете фотографии</p>
		</div>
		<div class="col-xs-6 col-sm-2">
			<div class="how-to-icon"><img src="/_site/i/v2/design.svg" /></div>
			<p class="h4">Макет</p>
			<p>Мы разрабатываем макет</p>
		</div>
		<div class="col-xs-6 col-sm-2">
			<div class="how-to-icon how-to-photo"><img src="/_site/i/v2/check.svg" /></div>
			<p class="h4">Согласование</p>
			<p>Согласовываем макет с Вами</p>
		</div>
		<div class="col-xs-6 col-sm-2">
			<div class="how-to-icon how-to-photo"><img src="/_site/i/v2/card.svg" /></div>
			<p class="h4">Оплата</p>
			<p>Вы оплачиваете фотокнигу</p>
		</div>
		<div class="col-xs-6 col-sm-2">
			<div class="how-to-icon how-to-photo"><img src="/_site/i/v2/book.svg" /></div>
			<p class="h4">Изготовление</p>
			<p>Мы изготавливаем фотокнигу</p>
		</div>
		<div class="col-xs-6 col-sm-2">
			<div class="how-to-icon how-to-photo"><img src="/_site/i/v2/delivery.svg" /></div>
			<p class="h4">Доставка</p>
			<p>Доставляем фотокнигу по всей России</p>
		</div>
	</div>
</div>
