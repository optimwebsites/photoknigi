<div>
	<div class="content-block">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<h2>Выпускной альбом ПРЕМИУМ</h2>
				<div class="row">
					<div class="col-sm-6">
						<ul class="simple-list">
							<li>Фотопечать на фотобумаге FujiFilm</li>
							<li>4 вида фотобумаги: матовая, глянцевая, металлик или шелк</li>
							<li>разворот на 180 градусов</li>
							<li>Минимальный объем - 2 разворота</li>
						</ul>
					</div>
					<div class="col-sm-6">
						<ul class="simple-list">
							<li>Проклейка страниц пластиком</li>
							<li>Любой вариант обложки: фотообложка, ткань, экокожа, бумвинил</li>
							<li>Оформление любой сложности: тиснение, вышивка, отстрочка, уголки, шильды</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row igallery">
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/vipusk/premium_1.jpg">
				<img src="/_site/i/v2/vipusk/premium_1.jpg"
					 data-title="<div class='igallery-caption'>Печать на фотобумаге</div>"
					 title="<div class='igallery-caption'>Печать на фотобумаге</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/vipusk/premium_2.jpg">
				<img src="/_site/i/v2/vipusk/premium_2.jpg"
					 data-title="<div class='igallery-caption'>Проклейка страниц пластиком</div>"
					 title="<div class='igallery-caption'>Проклейка страниц пластиком</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/vipusk/premium_3.jpg">
				<img src="/_site/i/v2/vipusk/premium_3.jpg"
					 data-title="<div class='igallery-caption'>Разворот на 180 градусов</div>"
					 title="<div class='igallery-caption'>Разворот на 180 градусов</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/vipusk/premium_4.jpg">
				<img src="/_site/i/v2/vipusk/premium_4.jpg"
					 data-title="<div class='igallery-caption'>Любые виды обложек: мягкая, твердая, экокожа, комбинированная</div>"
					 title="<div class='igallery-caption'>Любые виды обложек: мягкая, твердая, экокожа, комбинированная</div>"
					/>
			</a>
		</div>
	</div>

	<div class="content-block">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="title">
					<h3>Цены выпускного альбома ПРЕМИУМ</h3>
				</div>

				<?php
				$price = new w_photobook_price();
				$price->type = m_photobook::TYPE_VIPUSK_PREMIUM;
				$price->format = '15x21';
				$price->tpl = 'vipusk';
				echo $price->getHtml();
				?>
			</div>
		</div>
	</div>

	<div class="content-block  content-block__bg_green2">
		<div class="row">
			<div class="col-sm-3"><br/><button class="btn btn-lg btn-danger scroll-to" type="button" data-target="#order-form">Заказать печать</button></div>
			<div class="col-sm-9">
				<p class="text-right">Примерная стоимость дизайна<br/>6 страниц / 3 разворота, класс 22 человека, 10 преподавателей, персональная фотообложка
					<br>
					<span class="h3">2900</span> <span class="text-red">р.</span></p>
			</div>
		</div>
	</div>

	<div class="content-block">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<h2>Выпускной альбом "БАБОЧКА"</h2>
				<div class="row">
					<div class="col-sm-6">
						<ul class="simple-list">
							<li>Цифровая печать на мелованной бумаге плотностью 160 г/м.кв.</li>
							<li>Разворот страниц на 180 градусов</li>
							<li>Ламинация страниц, проклейка картоном</li>
							<li>Минимальный объем - 2 разворота</li>
						</ul>
					</div>
					<div class="col-sm-6">
						<ul class="simple-list">
							<li>Низкая цена при высоком качестве</li>
							<li>Любой вариант обложки: фотообложка, ткань, экокожа, бумвинил</li>
							<li>Оформление любой сложности: тиснение, вышивка, отстрочка, уголки, шильды</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row igallery">
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/vipusk/bat_1.jpg">
				<img src="/_site/i/v2/vipusk/bat_1.jpg"
					 data-title="<div class='igallery-caption'>Цифровая печать на фотобумаге 160 г/м.кв. с проклейкой пластиком</div>"
					 title="<div class='igallery-caption'>Цифровая печать на фотобумаге 160 г/м.кв. с проклейкой пластиком</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/vipusk/bat_2.jpg">
				<img src="/_site/i/v2/vipusk/bat_2.jpg"
					 data-title="<div class='igallery-caption'>Ламинирование разворотов</div>"
					 title="<div class='igallery-caption'>Ламинирование разворотов</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/vipusk/bat_3.jpg">
				<img src="/_site/i/v2/vipusk/bat_3.jpg"
					 data-title="<div class='igallery-caption'>Проклейка пластиком, разворот на 180 градусов</div>"
					 title="<div class='igallery-caption'>Проклейка пластиком, разворот на 180 градусов</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/vipusk/bat_4_1.jpg">
				<img src="/_site/i/v2/vipusk/bat_4_1.jpg"
					 data-title="<div class='igallery-caption'>Любые виды обложек: мягкая, твердая, экокожа, комбинированная</div>"
					 title="<div class='igallery-caption'>Любые виды обложек: мягкая, твердая, экокожа, комбинированная</div>"
					/>
			</a>
		</div>
	</div>

	<div class="content-block">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="title">
					<h2 class="text-red">Цена выпускного альбома "Бабочка"</h2>
				</div>

				<?php
				$price = new w_photobook_price();
				$price->type = m_photobook::TYPE_VIPUSK_BUTTERFLY;
				$price->format = '21x30';
				$price->tpl = 'vipusk';
				echo $price->getHtml();
				?>
			</div>
		</div>
	</div>

	<div class="content-block  content-block__bg_green2">
		<div class="row">
			<div class="col-sm-3"><br/><button class="btn btn-lg btn-danger scroll-to" type="button" data-target="#order-form">Заказать печать</button></div>
			<div class="col-sm-9">
				<p class="text-right">Примерная стоимость дизайна<br/>6 страниц / 3 разворота, класс 22 человека, 10 преподавателей, персональная фотообложка
					<br>
					<span class="h3">2900</span> <span class="text-red">р.</span></p>
			</div>
		</div>
	</div>

	<div class="content-block">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<h2>Выпускной альбом СТАНДАРТ</h2>
				<div class="row">
					<div class="col-sm-6">
						<ul class="simple-list">
							<li>Цифровая печать</li>
							<li>Мелованная бумага плотностью 220 г/м.кв. или 200 г/м.кв. + ламинация </li>
							<li>Минимальный объем - 14 страниц</li>
						</ul>
					</div>
					<div class="col-sm-6">
						<ul class="simple-list">
							<li>Любой вариант обложки: фотообложка, ткань, экокожа, бумвинил</li>
							<li>Оформление любой сложности: тиснение, вышивка, отстрочка, уголки, шильды</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row igallery">
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/vipusk/std_1.jpg">
				<img src="/_site/i/v2/vipusk/std_1.jpg"
					 data-title="<div class='igallery-caption'>Цифровая печать на бумаге плотностью 220 г/м.кв</div>"
					 title="<div class='igallery-caption'>Цифровая печать на бумаге плотностью 220 г/м.кв</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/vipusk/std_2.jpg">
				<img src="/_site/i/v2/vipusk/std_2.jpg"
					 data-title="<div class='igallery-caption'>Журнальная сшивка от 14 страниц</div>"
					 title="<div class='igallery-caption'>Журнальная сшивка от 14 страниц</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/vipusk/std_3.jpg">
				<img src="/_site/i/v2/vipusk/std_3.jpg"
					 data-title="<div class='igallery-caption'>Ламинация страниц</div>"
					 title="<div class='igallery-caption'>Ламинация страниц</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/vipusk/std_4.jpg">
				<img src="/_site/i/v2/vipusk/std_4.jpg"
					 data-title="<div class='igallery-caption'>Любые виды обложек: мягкая, твердая, экокожа, комбинированная</div>"
					 title="<div class='igallery-caption'>Любые виды обложек: мягкая, твердая, экокожа, комбинированная</div>"
					/>
			</a>
		</div>
	</div>

	<div class="content-block">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="title">
					<h3>Цена выпускного альбома СТАНДАРТ</h3>
				</div>

				<?php
				$price = new w_photobook_price();
				$price->type = m_photobook::TYPE_VIPUSK_STANDART;
				$price->format = '15x21';
				$price->tpl = 'vipusk';
				echo $price->getHtml();
				?>
			</div>
		</div>
	</div>

	<div class="content-block  content-block__bg_green2">
		<div class="row">
			<div class="col-sm-3"><br/><button class="btn btn-lg btn-danger scroll-to" type="button" data-target="#order-form">Заказать печать</button></div>
			<div class="col-sm-9">
				<p class="text-right">Примерная стоимость дизайна<br/>14 страниц, класс 22 человека, 10 преподавателей, персональная фотообложка
					<br>
					<span class="h3">3900</span> <span class="text-red">р.</span></p>
			</div>
		</div>
	</div>
</div>
