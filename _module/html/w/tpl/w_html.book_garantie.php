<div class="full-width-box  bottom-padding-mini service-desk text-white">
    <div class="fwb-bg fwb-paralax band-6" data-speed="2"></div>

    <div class="container" id="how-to-panel">

        <div class="row services">
            <div class="col-sm-12 col-md-12">
                <div class="title-box text-center">
                    <h1 class="title text-white">Мы не возьмем деньги, если наша работа Вам не
                        понравится</h1>
                </div>
            </div>
            <div class="col-sm-12 col-md-12">
                <p>Вы оплачиваете нашу работу только после согласования полностью готового к печати макета
                    фотокниги. Если Вам не понравится наша работа, мы не возьмем за нее оплату. </p><br/>
            </div>


            <div class="col-sm-3 col-md-3 service">
                <div class="icon bg"><i class="fa fa-picture-o"></i></div>
                <h6 class="title">Фото</h6>

                <div class="text-small">Вы присылаете фотографии</div>

            </div>
            <div class="col-sm-3 col-md-3 service">
                <div class="icon bg"><i class="fa fa-check"></i></div>
                <h6 class="title">Макет</h6>

                <div class="text-small">Мы делаем макет и согласуем его с Вами</div>

            </div>
            <div class="col-sm-3 col-md-3 service">
                <div class="icon bg"><i class="fa fa-credit-card"></i></div>
                <h6 class="title">Оплата</h6>

                <div class="text-small">Вы оплачиваете изготовление книги</div>

            </div>
            <div class="col-sm-3 col-md-3 service">
                <div class="icon bg"><i class="fa fa-truck"></i></div>
                <h6 class="title">Доставка</h6>

                <div class="text-small">Мы доставляем Вам книгу</div>

            </div>
        </div>


    </div>
</div>