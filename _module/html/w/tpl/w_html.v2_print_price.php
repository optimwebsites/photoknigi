<div>
	<div class="content-block">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<h2>Фотокниги ПРЕМИУМ</h2>
				<div class="row">
					<div class="col-sm-6">
						<ul class="simple-list">
							<li>Фотопечать на фотобумаге FujiFilm</li>
							<li>4 вида фотобумаги: матовая, глянцевая, металлик или шелк</li>
							<li>разворот на 180 градусов</li>
						</ul>
					</div>
					<div class="col-sm-6">
						<ul class="simple-list">
							<li>Проклейка страниц пластиком</li>
							<li>Любой вариант обложки: фотообложка, ткань, экокожа, бумвинил</li>
							<li>Оформление любой сложности: тиснение, вышивка, отстрочка, уголки, шильды</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>


	<div class="row igallery">
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/print/pr1.jpg">
				<img src="/_site/i/v2/print/pr1.jpg"
					 data-title="<div class='igallery-caption'>Любые виды обложек, отделка обложек любой сложности</div>"
					 title="<div class='igallery-caption'>Любые виды обложек, отделка обложек любой сложности</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/print/pr3.jpg">
				<img src="/_site/i/v2/print/pr3.jpg"
					 data-title="<div class='igallery-caption'>Печать на фотобумаге. Яркие цвета, на ломается на сгибах</div>"
					 title="<div class='igallery-caption'>Печать на фотобумаге. Яркие цвета, на ломается на сгибах</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/print/pr4.jpg">
				<img src="/_site/i/v2/print/pr4.jpg"
					 data-title="<div class='igallery-caption'>Разворот страниц на 180 градусов</div>"
					 title="<div class='igallery-caption'>Разворот страниц на 180 градусов</div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/print/pr2.jpg">
				<img src="/_site/i/v2/print/pr2.jpg"
					 data-title="<div class='igallery-caption'>Проклейка страниц пластиком</div>"
					 title="<div class='igallery-caption'>Проклейка страниц пластиком</div>"
					/>
			</a>
		</div>
	</div>

	<div class="content-block">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="title">
					<h3>Фотокниги ПРЕМИУМ. Калькулятор</h3>
				</div>

				<?php
				$price = new w_photobook_price();
				$price->type = m_photobook::TYPE_PHOTOBOOK_PREMIUM;
				$price->format = '21x30';
				$price->tpl = 'print';
				echo $price->getHtml();
				?>
			</div>
		</div>
	</div>


	<div class="content-block">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<h2>Фотокниги СТАНДАРТ</h2>
				<div class="row">
					<div class="col-sm-6">
						<ul class="simple-list">
							<li>Цифровая печать на мелованной бумаге плотностью 220 г/м.кв.</li>
							<li>Низкая цена при высоком качестве</li>
						</ul>
					</div>
					<div class="col-sm-6">
						<ul class="simple-list">
							<li>Любой вариант обложки: фотообложка, ткань, экокожа, бумвинил</li>
							<li>Оформление любой сложности: тиснение, вышивка, отстрочка, уголки, шильды</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row igallery">
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/print/st1.jpg">
				<img src="/_site/i/v2/print/st1.jpg"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/print/st2.jpg">
				<img src="/_site/i/v2/print/st2.jpg"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/print/st3.jpg">
				<img src="/_site/i/v2/print/st3.jpg"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/print/st4.jpg">
				<img src="/_site/i/v2/print/st4.jpg"
					/>
			</a>
		</div>
	</div>

	<div class="content-block">
		<div class="row">
			<div class="col-xs-12 col-sm-12">
				<div class="title">
					<h3>Фотокниги СТАНДАРТ. Калькулятор</h3>
				</div>

				<?php
				$price = new w_photobook_price();
				$price->type = m_photobook::TYPE_PHOTOBOOK_STANDART;
				$price->format = '21x30';
				$price->tpl = 'print';
				echo $price->getHtml();
				?>
			</div>
		</div>
	</div>
</div>
