<div class="container">
    <div class="row">
        <div class="title-box text-center col-sm-12 col-md-12">
            <div class="title h2">Контакты</div>
        </div>
        <div class="map-box col-sm-12 col-md-12">
            <div class="contact-info col-sm-6 col-md-6" data-appear-animation="fadeInLeftBig">
                <address>
                    <div class="title">ООО "Издательство Патриот"</div>
                    <div class="title">Адрес</div>
                    Москва, Олимпийский пр-т, д. 22.
                </address>
                <div class="row">
                    <address class="col-sm-6 col-md-6">
                        <div class="title">Звоните</div>
                        <div>+7 (495) 151-28-95</div>
                    </address>
                    <address class="col-sm-6 col-md-6">
                        <div class="title">Пишите</div>
                        <div><a href="mailto:<?=Yii::app()->site->getEmail()?>"><?=Yii::app()->site->getEmail()?></a></div>
                    </address>
                </div>
            </div>

            <div
                style="height: 500px;"
                class="map-canvas"
                data-zoom="16"
                data-lat="55.7870047"
                data-lng="37.620"
                data-title="Москва"
                data-content="Телефон: +7 (495) 151-28-95"></div>
        </div>
    </div>
</div>