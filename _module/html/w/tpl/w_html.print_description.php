<div class="container" id="equipments">
    <div class="row text-center">
        <div class="col-sm-12">
            <div class="title-box text-center">
                <div class="h1 title">Мы гарантируем качество наших фотокниг</div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 text-center bottom-padding-mini">
                <div class="text-center">
                    <img src="/_site/i/print/1.jpg" />
                </div>
                <div class="h5">Фотопечать мокрым (химическим) способом</div>
                <p>На сегодняшний день это самый качественный способ печати изображения, который пока никто не может превзойти.
                    Печать осуществляется на оборудовании Noritsy 3702 HD на фотобумаге FujicolorCrystalArchivePaper, обеспечивающем
                    высокое качество печати с беспрецендентным разрешением 640dpi. Наше обрудование позволяет легко отобразить
                    ту естественную красоту места съемки, что видел фотограф своими глазами при съемке фотографий.
                </p>
            </div>

            <div class="col-sm-6 bottom-padding-mini">
                <div class="text-center">
                    <img src="/_site/i/print/konica.jpg" />
                </div>
                <div class="h5">Печать типографским (лазерным) способом</div>
                <p>
                    Этот способ, пока немного уступает в качестве, мокрой печати, но зато себестоимость печати значительно ниже.
                    Лазерная печать возможна на разных типах и толщинах бумаг и на картоне. В этом варианте возможна двусторонняя печать.
                    Мы печатаем  на оборудовании KonicaMinoltaBizHub PRO C6000.
                </p>
            </div>
        </div>
    </div>
    <?php
    $w_description = new w_html();
    $w_description->tpl = "book_description_tools";
    echo $w_description->getHtml();
    ?>
</div>