<?php
$books = new m_photobook();
?>
<div class="row">
    <div class="col-sm-12 col-md-12">
        <div class="title-box text-center">
            <div class="h1 title">Наши цены</div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php
$discount = new w_html();
$discount->tpl = "discount_warning";
echo $discount->getHtml();
?>
<div class="row">
    <div class="col-md-12 text-center bottom-padding-mini">
        <div class="content-block frame-shadow-raised">
            <div class="row text-center book-price">
                <div class="col-sm-12">
                    <div class="h4">Фотокнига ПРЕМИУМ</div>
                </div>
            </div>
            <div class="row text-center book-price">
                <div class="col-sm-12">
                    <div class="h6  text-danger">Индивидуальный дизайн + печать <br><small>(8 разворотов / 16 страниц + персональная обложка)</small></div>
                </div>
            </div>
            <div class="row bk-price-list bottom-padding-mini">
                <div class="col-xs-3 col-sm-2">
                    <div class="bk-format">20х20</div>
                    <div class="bk-price"><?php
                        echo number_format($books->getPrice(
                            m_photobook::TYPE_PHOTOBOOK_PREMIUM,
                            '20x20',
                            8,
                            m_photobook::COVER_HARD,
                            true,
                            true
                        ), 0, ".", " ");
                        ?>
                    </div>
                    <!--<div class="bk-price-old"><?php
                    echo $books->getPrice(
                        m_photobook::TYPE_PHOTOBOOK_PREMIUM,
                        '20x20',
                        8,
                        m_photobook::COVER_HARD,
                        true
                    );
                    ?>
                    </div>-->
                </div>
                <div class="col-xs-3 col-sm-2">
                    <div class="bk-format">21х30</div>
                    <div class="bk-price"><?php
                        echo number_format($books->getPrice(
                            m_photobook::TYPE_PHOTOBOOK_PREMIUM,
                            '21x30',
                            8,
                            m_photobook::COVER_HARD,
                            true,
                            true
                        ), 0, ".", " ");
                        ?>
                    </div>
                    <!--<div class="bk-price-old"><?php
                        echo $books->getPrice(
                            m_photobook::TYPE_PHOTOBOOK_PREMIUM,
                            '21x30',
                            8,
                            m_photobook::COVER_HARD,
                            true
                        );
                        ?>
                    </div>-->
                </div>
                <div class="col-xs-3 col-sm-2">
                    <div class="bk-format">30х21</div>
                    <div class="bk-price"><?php
                        echo number_format($books->getPrice(
                            m_photobook::TYPE_PHOTOBOOK_PREMIUM,
                            '30x21',
                            8,
                            m_photobook::COVER_HARD,
                            true,
                            true
                        ), 0, ".", " ");
                        ?>
                    </div>
                    <!--<div class="bk-price-old"><?php
                        echo $books->getPrice(
                            m_photobook::TYPE_PHOTOBOOK_PREMIUM,
                            '30x21',
                            8,
                            m_photobook::COVER_HARD,
                            true
                        );
                        ?>
                    </div>-->
                </div>
                <div class="col-xs-3 col-sm-2">
                    <div class="bk-format">25х25</div>
                    <div class="bk-price"><?php
                        echo number_format($books->getPrice(
                            m_photobook::TYPE_PHOTOBOOK_PREMIUM,
                            '25x25',
                            8,
                            m_photobook::COVER_HARD,
                            true,
                            true
                        ), 0, ".", " ");
                        ?>
                    </div>
                    <!--<div class="bk-price-old"><?php
                    echo $books->getPrice(
                        m_photobook::TYPE_PHOTOBOOK_PREMIUM,
                        '25x25',
                        8,
                        m_photobook::COVER_HARD,
                        true
                    );
                    ?>
                    </div>-->
                </div>
                <div class="col-xs-3 col-sm-2">
                    <div class="bk-format">30х30</div>
                    <div class="bk-price"><?php
                        echo number_format($books->getPrice(
                            m_photobook::TYPE_PHOTOBOOK_PREMIUM,
                            '30x30',
                            8,
                            m_photobook::COVER_HARD,
                            true,
                            true
                        ), 0, ".", " ");
                        ?>
                    </div>
                   <!-- <div class="bk-price-old"><?php
                        echo $books->getPrice(
                            m_photobook::TYPE_PHOTOBOOK_PREMIUM,
                            '30x30',
                            8,
                            m_photobook::COVER_HARD,
                            true
                        );
                        ?>
                    </div>-->
                </div>
                <div class="col-xs-3 col-sm-2">
                    <div class="bk-format">40х30</div>
                    <div class="bk-price"><?php
                        echo number_format($books->getPrice(
                            m_photobook::TYPE_PHOTOBOOK_PREMIUM,
                            '40x30',
                            8,
                            m_photobook::COVER_HARD,
                            true,
                            true
                        ), 0, ".", " ");
                        ?>
                    </div>
                    <!--<div class="bk-price-old"><?php
                        echo $books->getPrice(
                            m_photobook::TYPE_PHOTOBOOK_PREMIUM,
                            '40x30',
                            8,
                            m_photobook::COVER_HARD,
                            true
                        );
                        ?>
                    </div>-->
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center bottom-padding-mini">
                    <a href=""  class="btn btn-warning" data-toggle="modal" data-target="#orderBook">Заказать фотокнигу</a>
                    <!--<a href="/fotoknigi/konstruktor_fotoknig/" class="btn btn-warning">Конструктор</a>-->
                </div>
            </div>
        </div>
    </div>
    <!--
    <div class="col-md-6 text-center bottom-padding-mini">
        <div class="content-block frame-shadow-raised">
            <div class="row text-center book-price">
                <div class="col-sm-12">
                    <div class="h4">Создайте фотокнигу сами!</div>
                </div>
            </div>

            <div class="row text-center">
                <div class="col-sm-6">
                    <a href="/fotoknigi/konstruktor_fotoknig/"><img src="/_site/i/l.jpg" title="Констуктор фотокниг" class="round-corner-60"/></a>
                </div>
                <div class="col-sm-6">
                    <div class="h6 text-danger">За 15 минут<br> в удобном конструкторе</small></div>
                    <div><br><br>Всего <span class="text-danger" style="font-size:200%">1900</span> рублей</div>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 text-center bottom-padding-mini">
                    <a href="/fotoknigi/konstruktor_fotoknig/" class="btn btn-warning">Создать фотокнигу</a>
                </div>
            </div>
        </div>
    </div>-->
</div>

