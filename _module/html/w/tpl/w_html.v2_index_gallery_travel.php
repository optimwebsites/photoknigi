<div id="gallery_travel">
	<div class="row content-block">
		<div class="col-xs-12">
			<h2>Фотокниги путешествий</h2>
			<p>
				Что может быть лучше отдыха, новых впечатлений, приключений и путешествий! Предлагаем сохранить все это на страницах ваших фотокниг. А, может, у вас уже накопился архив на целую серию фотокниг путешествий? Давайте оформим ваши фотовоспоминания в едином стиле.
			</p>
			<button class="btn btn-danger scroll-to" data-target="#order-form">Заказать фотокнигу</button>
		</div>
	</div>
	<div class="row igallery">
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/travel/1.jpg">
				<img src="/_site/i/v2/travel/1.jpg"
					 data-title="<div class='igallery-caption'>Фотообложка, комбинированный корешок. Цена 30x25 - <span class='igallery-price'>2115</span> <span class='igallery-price-rub'>р.</span></div>"
					 title="<div class='igallery-caption'>Фотообложка, комбинированный корешок. Цена 30x25 - <span class='igallery-price'>2115</span> <span class='igallery-price-rub'>р.</span></div>"
				/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/travel/2.jpg">
				<img src="/_site/i/v2/travel/2.jpg"
					 data-title="<div class='igallery-caption'>Фотообложка, фотопечать. Цена 21x30 - <span class='igallery-price'>2115</span> <span class='igallery-price-rub'>р.</span></div>"
					 title="<div class='igallery-caption'>Фотообложка, фотопечать. Цена 21x30 - <span class='igallery-price'>2115</span> <span class='igallery-price-rub'>р.</span></div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/travel/3.jpg">
				<img src="/_site/i/v2/travel/3.jpg"
					 data-title="<div class='igallery-caption'>Фотообложка, фотопечать, проклейка пластиком. Цена 25x25 - <span class='igallery-price'>1268</span> <span class='igallery-price-rub'>р.</span></div>"
					 title="<div class='igallery-caption'>Фотообложка, фотопечать, проклейка пластиком. Цена 25x25 - <span class='igallery-price'>1268</span> <span class='igallery-price-rub'>р.</span></div>"
					/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/travel/4.jpg">
				<img src="/_site/i/v2/travel/4.jpg"
					 data-title="<div class='igallery-caption'>Фотообложка, фотопечать, проклейка пластиком. Цена 30x30 - <span class='igallery-price'>2516</span> <span class='igallery-price-rub'>р.</span></div>"
					 title="<div class='igallery-caption'>Фотообложка, фотопечать, проклейка пластиком. Цена 30x30 - <span class='igallery-price'>2516</span> <span class='igallery-price-rub'>р.</span></div>"
					/>
			</a>
		</div>
	</div>
</div>
