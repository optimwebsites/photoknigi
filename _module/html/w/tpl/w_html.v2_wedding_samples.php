<div id="gallery_wedding">
	<div class="row content-block">
		<div class="col-xs-12">
			<h2>Свадебные фотокниги</h2>
			<p>
				Первая и самая важная фотокнига для молодой семьи, чтобы праздник не заканчивался никогда. Настоящая семейная драгоценность может быть выполнена в любом стиле. Также можно заказать вместе со свадебной фотокнигой минибуки для родных и близких. Поделитесь своей радостью!
			</p>
			<button class="btn btn-danger scroll-to" data-target="#order-form">Заказать фотокнигу</button>
		</div>
	</div>
	<div class="row igallery">
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/wedding/w1.jpg">
				<img src="/_site/i/v2/wedding/w1.jpg"
					 title="<div class='igallery-caption'></div>"/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/wedding/w2.jpg">
				<img src="/_site/i/v2/wedding/w2.jpg" />
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/wedding/w3.jpg">
				<img src="/_site/i/v2/wedding/w3.jpg" />
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/wedding/w4.jpg">
				<img src="/_site/i/v2/wedding/w4.jpg" />
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/wedding/5.jpg">
				<img src="/_site/i/v2/wedding/5.jpg"
					 title="<div class='igallery-caption'></div>"/>
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/wedding/6.jpg">
				<img src="/_site/i/v2/wedding/6.jpg" />
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/wedding/7.jpg">
				<img src="/_site/i/v2/wedding/7.jpg" />
			</a>
		</div>
		<div class="col-xs-6 col-sm-3 igallery-item">
			<a href="/_site/i/v2/wedding/8.jpg">
				<img src="/_site/i/v2/wedding/8.jpg" />
			</a>
		</div>
	</div>
</div>
