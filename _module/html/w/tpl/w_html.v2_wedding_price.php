<div class="content-block content-block">
	<div class="title">
		<h2>Цены</h2>
	</div>
	<div class="row price-row">
		<div class="col-xs-4 col-sm-2">
			<div class="book-size">20x20</div>
			<img src="/_site/i/v2/book_sizes_20x20.png" />
			<div class="book-price">4 600</div>
		</div>
		<div class="col-xs-4 col-sm-2">
			<div class="book-size">30x21</div>
			<img src="/_site/i/v2/book_sizes_30x21.png" />
			<div class="book-price">4 600</div>
		</div>
		<div class="col-xs-4 col-sm-2">
			<div class="book-size">21x30</div>
			<img src="/_site/i/v2/book_sizes_21x30.png" />
			<div class="book-price">4 600</div>
		</div>
		<div class="col-xs-4 col-sm-2">
			<div class="book-size">25x25</div>
			<img src="/_site/i/v2/book_sizes_25x25.png" />
			<div class="book-price">5 000</div>
		</div>
		<div class="col-xs-4 col-sm-2">
			<div class="book-size">30x30</div>
			<img src="/_site/i/v2/book_sizes_30x30.png" />
			<div class="book-price">5 300</div>
		</div>
		<div class="col-xs-4 col-sm-2">
			<div class="book-size">40x30</div>
			<img src="/_site/i/v2/book_sizes_40x30.png" />
			<div class="book-price">5 800</div>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-6">
			<h3>В стоимость включено</h3>
			<ul class="simple-list">
				<li>индивидуальный дизайн фотокниги 16 страниц (8 разворотов)</li>
				<li>высококачественная печать на фотобумаге</li>
				<li>проклейка страниц пластиком</li>
				<li>персональная ламинированная фотообложка</li>
				<li>обработка и верстка 50 фотографий</li>
			</ul>
		</div>
		<div class="col-sm-6">
			<h3>Дополнительно</h3>
			<ul class="simple-list">
				<li>до 40 разворотов</li>
				<li>больше фотографий</li>
				<li>вышивка, обложки из ткани, кожзама, бумвинила, паспарту</li>
				<li>отделка срезов фотокниги</li>
				<li>боксы, минибук</li>
			</ul>
		</div>
	</div>
</div>
