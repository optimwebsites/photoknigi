<div class="container">
    <div class="row">
        <div class="col-sm-6 bottom-padding-mini">
            <div class="title-box text-center">
                <h2 class="title ">Ищете лучшие цены на печать фотокниг?</h2>
            </div>
            <div class="text-center">
                <a href="/fotoknigi/pechat/" class="btn btn-warning"><big>Они здесь</big></a>
                <br>
                <br>
                <p>Если у вас есть собственный макет, вы дизайнер или фотограф, то мы вам предложим лучшую цену на печать фотокниг</p>
            </div>
        </div>
        <div class="col-sm-6 col-md-6">
            <div class="manufactures carousel-box load overflow bottom-padding-mini" data-autoplay-disable="true">
                <div class="title-box text-center">
                    <a class="next" href="#">
                        <svg x="0" y="0" width="9px" height="16px" viewBox="0 0 9 16" enable-background="new 0 0 9 16" xml:space="preserve">
				  <polygon fill-rule="evenodd" clip-rule="evenodd" fill="#fcfcfc" points="1,0.001 0,1.001 7,8 0,14.999 1,15.999 9,8 "></polygon>
				</svg>
                    </a>
                    <a class="prev" href="#">
                        <svg x="0" y="0" width="9px" height="16px" viewBox="0 0 9 16" enable-background="new 0 0 9 16" xml:space="preserve">
				  <polygon fill-rule="evenodd" clip-rule="evenodd" fill="#fcfcfc" points="8,15.999 9,14.999 2,8 9,1.001 8,0.001 0,8 "></polygon>
				</svg>
                    </a>
                    <h2 class="title ">Мы изготавливаем фотокниги и фотокаталоги для</h2>
                </div>

                <div class="clearfix"></div>
                <div class="row">
                    <div class="carousel">
                        <div class="make-wrapper">
                            <img class="replace-2x" src="/_site/i/logo-rgo.png" width="128" height="128" alt="">
                        </div>
                        <div class="make-wrapper">
                            <img class="replace-2x" src="/_site/i/Metro_logo.png" width="128" height="128" alt="">
                        </div>
                        <div class="make-wrapper">
                            <img class="replace-2x" src="/_site/i/shl-logo.jpg" width="128" height="128" alt="">
                        </div>
                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
