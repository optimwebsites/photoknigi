<?php

return [
    "default" => [
        "order_page" => [
            "value" => "/_c/photobook/createPhotobook/s/",
            "title" => "Страница оформления заказа",
            "editable" => true
        ],
        "order_info_page" => [
            "value" => '/_c/photobook/createPhotobook/s/',
            "title" => "Страница с информацией о заказе",
            "editable" => true
        ],
        "admin_email" => [
            "value" => 'info@funfotobook.ru',
            "title" => "E-mail администратора магазина",
            "editable" => true
        ],
    ]
];
