<?php
$formats = [];

?>
<div>
    <div class="container">
        <style>
            .product {
                cursor:pointer;
            }

            .product img {
                border-left: 1px solid #f0f0f0;
                border-right: 1px solid #f0f0f0;
                border-top: 1px solid #f0f0f0;
            }
            .product.active .product-description {
                background-color: #ff2f30;
                color:#fff;
            }

            .constructor-book__title {
                text-align:center;
                padding-bottom: 10px;
                color: #ff2f30;
            }

            .constructor-book__format {
                margin:10px 0;
                float:left;
            }

            .constructor-book__price {
                float:right;
                margin:10px 0;
                color: #ff2f30;
            }
        </style>

        <div class="row">
            <div class="col-sm-12">
                <div class="title-box">
                    <h1 class="title">Конструктор фотокниг</h1>
                </div>
                <div class="row bottom-padding-mini">
                    <div class="col-sm-4">
                        <img src="/_site/i/l.jpg" />

                    </div>
                    <div class="col-sm-8">
                        <h3 class="text-danger">Создайте фотокнигу в удобном конструкторе за 15 минут</h3>

                        <p>Чтобы создать фотокнигу в нашем конструкторе фотокниг выполните несколько простых действий</p>
                        <ul>
                            <li>Выберите шаблон фотокниги</li>
                            <li>Загрузите фотографии в конструктор</li>
                            <li>Расставьте фотографии в фотокниге</li>
                            <li>Оформите заказ</li>
                        </ul>
                    </div>
                </div>
                </div>
            </div>
        </div>
        <div class="full-width-box  bottom-padding-mini text-white" id="photobooks" style="padding:20px 0">
            <div class="fwb-bg fwb-paralax band-6" data-speed="2"></div>

            <div class="container" id="how-to-panel">

                <div class="row text-center">
                    <div class="col-sm-3 col-md-3">
                        <div style="font-size:60px;"><i class="fa fa-money"></i></div>
                        <div>Цена</div>
                        <div class="h2">Всего 1900 рублей</div>
                    </div>
                    <div class="col-sm-3 col-md-3">
                        <div style="font-size:60px;"><i class="fa fa-clock-o"></i></div>
                        <div>Срок изготовления</div>
                        <div class="h2">3 дня</div>
                    </div>
                    <div class="col-sm-3 col-md-3">
                        <div style="font-size:60px;"><i class="fa fa-credit-card"></i></div>
                        <div>Оплата</div>
                        <div class="h2">на сайте</div>

                    </div>
                    <div class="col-sm-3 col-md-3">
                        <div style="font-size:60px;"><i class="fa fa-truck"></i></div>
                        <div>Доставка по России</div>
                        <div class="h2">от 1 до 7 дней</div>
                    </div>
                </div>


            </div>
        </div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                <div class="title-box text-center">
                    <h2 class="text-danger text-center">Создайте фотокнигу прямо сейчас</h2>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="panel-group" id="accordion">
                    <!--<div class="panel panel-default" id="aRegistrationPanel">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#aRegistraion">
                                Зарегистрируйтесь
                            </a>
                        </h4>
                    </div>
                    <div id="aRegistraion" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3"> <?php
                    $w = new w_callback();
                    $w->tpl = 'constructor';
                    // echo $w->getHtml();
                    ?>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>-->
                    <!--<div class="panel panel-default">
                        <div id="aDesign" class="panel-collapse">
                            <div class="panel-body">
                                <div class="products grid">
                                    <div class="col-sm-6 col-md-6 product product-mini" data-type="design" data-id="0">
                                        <img src="/_c/placeholder/show/s/?w=600&h=300" alt="" title="">
                                        <div class="product-description">
                                            <div class="price"><small>Создать дизайн самостоятельно</small></div>
                                        </div>
                                    </div>
                                    <div class="col-sm-6 col-md-6 product product-mini " data-type="design" data-id="1">
                                        <img src="/_c/placeholder/show/s/?w=600&h=300" alt="" title="" width="600" height="300">
                                        <div class="product-description">
                                            <div class="price"><small>Использовать шаблон</small></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>-->
                    <div id="aTemplatePanel">
                        <div class="row bottom-padding-mini">
                            <?php
                            $templates = Yii::app()->params['photobook']['templates'];
                            $photobook = new m_photobook();
                            $i = 0 ;
                            foreach ($templates as $id => $template) { ?>
                                <div class="col-sm-4 col-md-4 product" data-type="template" data-id="<?=$id?>" data-formats='<?=json_encode($template['formats'])?>'>
                                    <div class="constructor-book__title"><?=$template['title']?></div>
                                    <div class="frame frame-padding frame-shadow">
                                        <img src="/_site/i/templates/<?=$id?>.jpg" alt="" title="">
                                    </div>
                                    <div class="constructor-book__description">
                                        <div class="constructor-book__format" data-editor-format="408">Формат <?=current($template['formats'])?></div>
                                        <div class="constructor-book__price">Цена <span class="text-big"><?=$photobook->getEditorPrice(m_photobook::TYPE_PHOTOBOOK_PREMIUM, '20x20')?></span> р.</div>
                                    </div>
                                </div>
                            <?php
                                $i++;
                                if ($i % 3  == 0) {
                                    echo '</div><div class="row">';
                                }

                            } ?>
                        </div>

                    </div>
                    <div class="panel panel-default hide" id="aFormatPanel">
                        <div class="panel-heading">
                            <h4 class="panel-title">
                                <a href="#">Формат фотокниги</a>
                            </h4>
                        </div>
                        <div id="aFormat" class="panel-collapse">
                            <div class="panel-body">
                                <div class="products grid">
                                    <?php
                                    $formats = Yii::app()->params['photobook']['formats'];
                                    foreach ($formats as $id => $format) { ?>
                                        <div class="col-sm-2 col-md-2 product product-mini" data-type="format" data-editor-format="<?=$id?>" data-id-format="<?=$format['id_format']?>">
                                            <img src="/_site/i/<?=$format['id_format']?>.jpg" alt="" title="" width="270" height="270">
                                            <div class="product-description">
                                                <div class="price"><?=$format['title']?></div>
                                            </div>
                                        </div>
                                    <?php } ?>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <style>
        .constructor-footer {
            position: fixed;
            bottom:0;
            background: #fdfdfd;
            width:100%;
            z-index:9999;
            border-top: 1px solid #e1e1e1;
            transition: margin-top 0.2s linear;
            box-shadow: 0 0 5px rgba(0, 0, 0, 0.15);
        }

        .constructor-price {
            padding-top:23px;
        }

        .constructor-footer button {
            margin-top:23px;
        }

        .constructor-footer .title {
            font-weight: normal;
        }
    </style>
    <div class="constructor-footer">
        <div class="container">
            <div id="constructorOrderInfo" class="hide">
                <div class="row">
                    <div class="col-xs-12 col-sm-4  text-center"><h3 class="title">Стоимость фотокниги<br/><small>5 разворотов / 10 страниц</small></h3></div>
                    <div class="col-xs-6 col-sm-2 constructor-price text-center"><span class="h3" id="aTotal"></span> руб.</div>
                    <div class="col-xs-6 col-sm-2"><button class="btn btn-warning" id="constructorOrder">Создать фотокнигу</button></div>
                    <div class="col-xs-12 col-sm-4"><br/><h5 class="title text-center hide">Фотокнига будет готова <span id="aBookReady">12.10.2015</span></h5></div>
                </div>
            </div>
        </div>
    </div>
    <div id="constructorModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Закрыть">
                        <span aria-hidden="true">X</span>
                    </button>
                    <form class="register-form contact-form" method="post" action="/_j/order/saveOrder/s/" id="saveOrderForm">
                        <input type="hidden" value="<?=m_order_type::DEFAULT_ORDER_TYPE?>" name="m_order[id_order_type]" />

                        <input type="hidden" value="1" name="m_order_item[0][id_catalog_item]" />
                        <input type="hidden" value="1" name="m_order_item[0][num]" />
                        <input type="hidden" value="Новый клиент" name="m_client_retail[firstname]" />
                        <input type="hidden" value='' name="m_order[param]" />

                        <input type="hidden" value="1" name="type_id" />
                        <input type="hidden" value="141" name="format_id" />
                        <input type="hidden" value="0" name="group_id" />
                        <input type="hidden" value="<?="ID ".time()?>" name="name" />

                        <br/><p class="text-center">Перед редактированием фотокниги укажите e-mail.
                            Используя e-mail, Вы сможете вернуться к редактированию фотокниги после закрытия браузера</p>
                        <div class="row">
                            <div class="col-xs-12 col-sm-12"><label>E-mail</label>
                                <input type="text" class="form-control" name="m_client_retail[email]" /></div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="buttons-box clearfix">
                            <div class="alert hide" id="formMsg_saveOrderForm"></div>
                            <button type="button" class="btn btn-warning form-submit" data-form="saveOrderForm">
                                Перейти в онлайн-редактор
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        /*$("#aDesign .product").click(function() {
            var tpl = $("#aTemplatePanel");

            if ($(this).data('id') == 1) {
               tpl.removeClass('hide');
                scrollToElement(tpl, 500);
                $("#aFormatPanel").addClass('hide');

            } else {
                tpl.addClass('hide');
                showFormatPanel();
            }
            resetCalculator()

            return false;
        });*/

        $("#aTemplatePanel .product").click(function() {
            //showFormatPanel($(this).data('formats'));
            calcConstructor($(this));
        });

        function showFormatPanel(showFormats)
        {
            var format = $("#aFormatPanel");
            var products = format.find(".product")

            if (showFormats != undefined) {
                products.each(function() {
                    $(this).addClass('hide');
                });
                for (var i=0; i< showFormats.length; i++) {
                    products.each(function() {
                        if ($(this).data('id-format') == showFormats[i]) {
                            $(this).removeClass('hide');
                        }
                    });
                }
            } else {
                products.each(function() {
                    $(this).removeClass('hide');
                });
            }
            format.removeClass('hide');
            scrollToElement(format, 500);
            resetCalculator();
        }

        function resetCalculator()
        {
            $("#aFormat .product").removeClass('active');
            $("#aTemplate .product").removeClass('active');

            $("#constructorOrderInfo").addClass('hide');
        }

        function calcConstructor(aTemplate)
        {
            var params = <?= (new m_photobook())->toJson(m_photobook::TYPE_PHOTOBOOK_PREMIUM); ?>;

            var duration = {print: 3, design: 4, delivery:4};

           // var aFormat = $("#aFormat .product.active");
            //var aTemplate = $("#aTemplatePanel .product.active");
            var id_group = aTemplate.length == 1 ? aTemplate.data('id') : 0;

            var id_format = aTemplate.find(".constructor-book__format").data('editor-format');
            var price_page = params['params']["20x20"]['price_page'];
            var price_cover = params['covers']["20x20"]['cover_hard']['price'];

            var aDesignVal = 1;
            var aBookNum = 1; //$("#aBooksNum");
            var numPages = 10; //aPages.val();

            var price = (price_cover + price_page * numPages) * aBookNum;

            $("#aTotal").html(price);
            var durationTotal = duration.print - (-1) * duration.delivery;

            $("#aBookReady").text(durationTotal);
            //$("#constructorOrderInfo").removeClass('hide');

            var params = {type_id:<?=Yii::app()->params['photobook']['editor_types']?>, format_id: id_format, group_id: id_group};
            $("[name='m_order[param]']").val(JSON.stringify(params));

            $("#constructorModal").modal('show');
        }

        $(".product").click(function() {
            var type = $(this).data("type");
            $(".product[data-type="+type+"]").removeClass('active');
            $(this).addClass('active');
        });

        $("#aFormat .product").click(function() {
            calcConstructor();
        });

        //подписываемся на событие успешного сабмита
        $(document).on('form_submit_success', function(e, data) {
            // $(document).on('start_submit_form', function(e, data) {

            //перезагружаем список
            var id_form = $(data.form).attr('id');

            if (id_form == "saveCallbackForm") {
                $("#aRegistrationPanel").hide();
                $("#aRegistraion").data("registered", 1);
                $("#aRegistrationPanel").hide();
                $("#aFormat").collapse('show');
            }
        });

        $("#constructorOrder").click(function() {
            $("#constructorModal").modal('show');

        });
    </script>
</div>