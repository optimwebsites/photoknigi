<?php

namespace dfs\docdoc\components;

use Yii;
use CHttpCookie;

/**
 *  Класс Partner
 *  реализует компонент для партнера-реферала.
 *
 * Если посетитель пришел по ссылке с сайта партнера, этот компонент инициализирует партнера и сохраняет его в сессии
 * и реализует доступ к информации о партнере через Yii::app()->referral
 *
 *
 * Свойста, получаемые через магические методы при помощи геттеров
 *
 * @property PartnerModel $partner
 * @property integer      $id
 */
class k_partner extends \CApplicationComponent
{
	/**
	 * @var string ключ массива в сессии, в котором будет храниться $_partner
	 */
	public $sessParam = 'ReferralObj';

	/**
	 * @var string имя $_GET параметра, в котором передается id партнера
	 */
	public $getParam = 'pid';

	/**
	 * Время жизни куки в секундах
	 * По умолчанию 30 дней
	 *
	 * @var int
	 */
	public $cookieLifeTime = 2592000;

	/**
	 * @var PartnerModel экземпляр модели партнера
	 */
	private $_partner;

	/**
	 * @var int идентификатор партнера
	 */
	private $_id;

	/**
	 * Инициализация компонента
	 *
	 */
	public function init()
	{
		parent::init();

		$app = Yii::app();

		if ($app->request->getQuery('widget', null) !== null) {
			$this->_isWidget = true;
		}

		$this->initPartner();

		if (!$ab && isset($app->abTest)) {
			$ab = $app->abTest;
		}

		if ($ab) {
			$ab->initAbTest($this);
		}

		if (!$this->_id) {
			$this->setId(PartnerModel::getDefaultPartnerId(), false);
		}
	}

	/**
	 * инициализация партера
	 */
	private function initPartner()
	{
		$app = Yii::app();
		$request = $app->request;

		//проверяем передан ли id партнера
		$id = $this->getPartnerIdByHost();

		if (!$id) {
			if (!is_null($request->getQuery($this->getParam))) {
				$id = $request->getQuery($this->getParam);
			} elseif ($request->cookies->contains($this->getParam)) {
				$id = (int)(string)$request->cookies[$this->getParam];
			} elseif (isset($app->session[$this->sessParam])) {
				// проверяем партнера, существует ли он в сессии
				$id = $app->session[$this->sessParam];
			} else {
				$id = $this->getPartnerIdBySource();
			}
		}

		if ($id) {
			$this->setId($id);
		}

		if ($this->_partner !== null && $this->_partner->param_client_uid_name) {
			$this->checkPartnerClientUid($this->_partner->param_client_uid_name);
		}
	}

	/**
	 * Опеределение партнера для определенного хоста
	 *
	 * @return int|null
	 */
	private function getPartnerIdByHost()
	{
		$host = Yii::app()->request->getHostInfo();
		$hosts =  Yii::app()->params['hosts'];
		foreach ($this->_hostPartners as $h => $partnerId) {
			if (strpos($host, $hosts[$h]) !== false) {
				return $partnerId;
			}
		}

		return null;
	}

	/**
	 * Является ли партнер спец. партнером для конкретного хоста
	 *
	 * @param string $host
	 *
	 * @return bool
	 */
	public function isPartnerForHost($host)
	{
		return $this->_hostPartners[$host] == $this->id;
	}

	/**
	 *  Геттер для модели партнера
	 *
	 * @return PartnerModel
	 */
	public function getPartner()
	{
		if ($this->_partner === null && $this->_id !== null) {
			$this->setPartner();

			if ($this->_partner === null) {
				$this->_id = null;
			}
		}

		if ($this->_id === null || $this->_id == PartnerModel::getDefaultPartnerId()) {
			if (Yii::app()->hasComponent('adWords')) {
				if (Yii::app()->adWords->isShow()) {
					$this->_id = Yii::app()->adWords->getPartnerId();
					$this->setPartner();
				}
			}
		}

		return $this->_partner;
	}

	/**
	 * Установка партнера
	 */
	private function setPartner()
	{
		$this->_partner = PartnerModel::model()
			->withoutSecureInfo()
			// ->with('phones')
			->cache(3600)
			->findByPk($this->_id);
	}

	/**
	 * Ставит партнерскую куку
	 */
	private function setPartnerCookie()
	{
		if (php_sapi_name() === 'cli') {
			return;
		}

		if ($this->_isWidget) {
			return;
		}
		$cookieHost = Yii::app()->site->isTopDoc() ? 'td' : 'main';

		$params = [
			'expire' => time() + $this->cookieLifeTime,
			'domain' => Yii::app()->params['hosts'][$cookieHost],
		];

		$cookie = new CHttpCookie($this->getParam, $this->_id, $params);
		Yii::app()->request->cookies[$this->getParam] = $cookie;
	}

	/**
	 * Установка в куки идентификатора клиента переданого от партнера
	 *
	 * @param string $paramName
	 */
	private function checkPartnerClientUid($paramName)
	{
		$value = \Yii::app()->request->getQuery($paramName);
		if ($value) {
			$cookieHost = Yii::app()->site->isTopDoc() ? 'td' : 'front';
			\Yii::app()->request->cookies['partner_client_uid'] =
				new \CHttpCookie(
					'partner_client_uid',
					$value,
					[
						'expire' => time() + $this->cookieLifeTime,
						'domain' => "." . Yii::app()->params['hosts'][$cookieHost],
					]
				);
		}
	}

	/**
	 * Возвращает id партнера
	 *
	 * @return int идентификатор патнера
	 */
	public function getId()
	{
		return $this->_id;
	}

	/**
	 * Установка партнера
	 *
	 * @param int  $id
	 * @param bool $setCookie
	 */
	public function setId($id, $setCookie = true)
	{
		$app = Yii::app();
		$request = $app->request;

		$this->_id      = (int)$id;
		$this->_partner = null;


		if ($this->getPartner() !== null && !$this->_isWidget) {
			$app->session[$this->sessParam] = $this->_id;
			if ($setCookie) {
				$this->setPartnerCookie();
			}
			// Принудительно обновим куи. Если в них сохраняется другой партнёр
			elseif($request->cookies->contains($this->getParam) && $request->cookies[$this->getParam]->value!=$id) {
				$this->setPartnerCookie();
			}
		}
	}

	/**
	 * Логин партнера
	 *
	 * @return null|string
	 */
	public function getLogin()
	{
		$partner = $this->getPartner();
		return $partner ? $partner->login : null;
	}

	/**
	 * Получаем идентификатор партнера в зависимости от источника трафика
	 *
	 * @return int|null
	 */
	protected function getPartnerIdBySource()
	{

		$request = Yii::app()->request;
		$trafficSource = Yii::app()->trafficSource->getSource();

		$id = null;
		if (strpos($request->getUrl(), '/service') === 0) {
			if ($trafficSource == TrafficSourceComponent::SOURCE_SEO) {
				$id = PartnerModel::DEFAULT_SEO_SERVICE_ID;
			}
		}
		if (!$id && $trafficSource == TrafficSourceComponent::SOURCE_TYPEIN) {
			$cityId = Yii::app()->city->getCityId();
			$id = PartnerModel::getDefaultTypeInPartnerId($cityId);
		}

		return $id;
	}
}
