<?php

return [
    'default' => [
        "id_page" => [
            "value" => 71,
            "title" => "Использовать расположение блоков, как на странице",
            "editable" => true
        ],
        "id_page_template_block_lib" => [
            "value" => "content",
            "title" => "Выводить сообщение об ошибке в блоке с идентификатором",
            "editable" => true
        ],
    ]
];
